# VSDK

[![coverage report](https://gitlab.com/HackerSir/VSDK/badges/master/coverage.svg)](https://gitlab.com/HackerSir/VSDK/commits/master)

逢甲大學學生自治選舉公開資訊站  
VSDK(Voting Standard Digital Kiosk)  
FCU Student Government Election Observation Post System

## 網址
- [https://voting.hackersir.org/](https://voting.hackersir.org/)

## 基礎架構
- PHP
- MySQL

## 主要框架
- Laravel 5.2
- Bootstrap 3

## 文件格式
- Use [PSR-2](http://www.php-fig.org/psr/psr-2/) as Coding Style
- Use [EditorConfig](http://editorconfig.org/) for other coding style settings

## 推薦IDE
- PhpStorm

## 授權條款
VSDK專案是一個開源專案，專案以 [MIT License](http://opensource.org/licenses/MIT) 授權釋出。
