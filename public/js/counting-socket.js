var socket;
var $connect_status = $('#connect_status');
var $updated_at = $('#updated_at');

function initSocket() {
    socket = io(socketUrl);
    // 連線成功提示
    socket.on("connect", function () {
        $connect_status.removeClass();
        $connect_status.addClass('text-success');
        $connect_status.html('<i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;連線成功');

        socket.emit('join_room', 'activity_' + activityId);
        for (var t in shownChartId) {
            socket.emit('join_room', shownChartId[t]);
        }
    });

    // 連線失敗提示
    socket.on("connect_error", function () {
        $connect_status.removeClass();
        $connect_status.addClass('text-danger');
        $connect_status.html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;連線失敗');
    });

    // 連線中提示
    socket.on("reconnecting", function () {
        $connect_status.removeClass();
        $connect_status.addClass('text-danger');
        $connect_status.html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>&nbsp;連線中');
    });

    socket.on('updated_at', function (message) {
        lastUpdateAt = new Date(message.updated_at * 1000);
    });

    socket.on('ballot_update', function (message) {
        updateChart(message.rid, message.data);
    });

    socket.on('activity_status', function (message) {
        $status_label.text(message);
    });

    socket.on('online_count_update', function (onlineCount) {
        $onlineCount.text(onlineCount);
    });
}

setInterval(function () {
    if (lastUpdateAt !== undefined) {
        $('#updated_at').text(Math.round((new Date() - lastUpdateAt) / 1000));
    }
}, 500);
