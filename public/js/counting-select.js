var $video_list = $('#video_list');

function initSelect() {
    initVideoSelect();
}

function initVideoSelect() {
    $video_list.change(function () {
        var $option = $video_list.find("option:selected");
        var embedUrl = $option.val();
        Cookies.set(activityId + 'videoSelected', $option.attr('id'));
        if (embedUrl == "null") {
            $('#video_div').hide();
        }
        else {
            $('#video_div').show();
            $('#video_iframe').attr('src', embedUrl);
        }
    });

    var videoSelected = Cookies.get(activityId + 'videoSelected');
    var $option = $video_list.find('#' + videoSelected);
    if ($option.length === 0) {
        // 沒有設定檔，代表是第一次進來，嘗試讓使用者選擇第一項(以0開始)
        $option = $video_list.find('option:eq(1)');
    }
    if ($option.length !== 0) {
        $option.attr('selected', true);
        $('#video_list').change();
    }
    else {
        $('#video_div').hide();
    }
}

