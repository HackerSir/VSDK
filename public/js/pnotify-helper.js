//設定風格
PNotify.prototype.options.styling = "bootstrap3";

var stack_topright = {"dir1": "down", "dir2": "left", "push": "down", "firstpos1": 70, "firstpos2": 25};
function show_message(type, message) {
    var opts = {
        title: "提示訊息",
        text: message,
        addclass: "stack-topright",
        stack: stack_topright,
        buttons: {
            closer_hover: false
        }
    };
    switch (type) {
        case 'error':
            opts.type = "error";
            break;
        case 'info':
            opts.type = "info";
            break;
        case 'success':
            opts.type = "success";
            break;
    }
    new PNotify(opts);
}
