var charts = {};
var options = {
    width: '100%',
    height: 250 - 20 - 5,
    bar: {groupWidth: "75%"},
    legend: {position: "none"},
    chartArea: {
        left: '20%',
        width: '60%'
    },
    hAxis: {
        title: '票數',
        viewWindow: {
            min: 0
        },
        textStyle: { fontSize: 12 },
        titleTextStyle: { fontSize: 14 },
        format: 'decimal'
    },
    vAxis: {
        textStyle: { fontSize: 14 }
    },
    titleTextStyle: {
        fontSize: 16
    },
    tooltip: {
        textStyle: { fontSize: 12 }
    }
};

function createAllChart() {
    for (var t in data) {
        var col_div = createChart(t);
        if (shownChartId.indexOf(t) < 0) col_div.hide();
    }

    if (shownChartId.length == 0) {
        $no_chart_tip.show();
    }
}

function createChart(rid) {
    var datum = google.visualization.arrayToDataTable(data[rid].data);
    options.title = data[rid].title;

    var col_div = $('<div>').addClass('col-xs-12 col-md-4 chartWithOverlay');
    col_div.css({'height': '250px', 'padding-top': '5px'});

    var chart_div = $('<div id="chart_' + rid + '"></div>');
    col_div.append($closeIcon.clone(true));
    col_div.append(clear_div);
    col_div.append(chart_div);
    col_div.data('id', rid);
    col_div.animateCss('fadeIn');

    var $invalidCount_span = $('<span style="color: gray">' + data[rid].invalid + '</span>');
    col_div.append($('<div class="overlay"></div>').append('廢票：', $invalidCount_span ,'票'));

    // 必須在 chart.draw 前加入，不然寬度會計算錯誤
    $chart_area.append(col_div);

    var chart = new google.visualization.BarChart(chart_div[0]);
    chart.draw(datum, options);

    var chartInfo = {
        data: datum,
        chart: chart,
        gids: data[rid].gid,
        invalidCount_span: $invalidCount_span
    };

    charts[rid] = chartInfo;

    return col_div;
}

function updateChart(rid, newRowData) {
    var chartInfo = charts[rid];
    if (chartInfo === undefined) return;

    options.title = data[rid].title;

    // update count
    for (var gid in newRowData.data) {
        var ballotData = newRowData.data[gid];
        var rowIndex = chartInfo.gids.indexOf(parseInt(gid));

        if (typeof ballotData === 'object') {
            chartInfo.data.setValue(rowIndex, 1, ballotData['agree']);
            chartInfo.data.setValue(rowIndex, 2, ballotData['agree']);
            chartInfo.data.setValue(rowIndex, 3, ballotData['disagree']);
            chartInfo.data.setValue(rowIndex, 4, ballotData['disagree']);
        }
        else {
            chartInfo.data.setValue(rowIndex, 1, ballotData);
            chartInfo.data.setValue(rowIndex, 2, ballotData);
        }
    }

    //update invalid count
    chartInfo.invalidCount_span.text(newRowData.invalid);

    chartInfo.chart.draw(chartInfo.data, options);
}
