<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Hackersir\Activity::class, function (Faker\Generator $faker) {
    return [
        'process'             => $faker->realText(),
        'reporting_link'      => $faker->url,
        'results_report_link' => $faker->url,
    ];
});

$factory->define(Hackersir\Election::class, function (Faker\Generator $faker) {
    return [
        'name'         => $faker->name,
        'introduction' => $faker->realText(),
    ];
});

$factory->define(Hackersir\Candidate::class, function (Faker\Generator $faker) {
    return [
        'name'       => $faker->randomElement(['釋輝長', '富惠掌', '莊孝維', '甄祆授']),
        'gender'     => $faker->randomElement(['男', '女']),
        'age'        => rand(18, 25),
        'grade'      => rand(1, 4),
        'class_no'   => rand(0, 3),
        'experience' => $faker->realText(),
    ];
});
