<?php

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->defineAs(Hackersir\Ballot::class, 'VALID', function (Faker\Generator $faker) use ($factory) {
    return [
        'type' => Hackersir\Ballot::VALID,
    ];
});

$factory->defineAs(Hackersir\Ballot::class, 'VALID_OBJECTION', function (Faker\Generator $faker) use ($factory) {
    return [
        'type' => Hackersir\Ballot::VALID_OBJECTION,
    ];
});

$factory->defineAs(Hackersir\Ballot::class, 'INVALID', function (Faker\Generator $faker) use ($factory) {
    return [
        'type' => Hackersir\Ballot::INVALID,
    ];
});
