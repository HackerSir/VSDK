<?php

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Hackersir\Group::class, function (Faker\Generator $faker) {
    $politicsList = [
        '買下逢甲夜市',
        '蓋一棟新的社團大樓',
        '選總統！',
        '呵呵！',
        '我要當會長',
        '每間實驗室應該要有PS4',
        '一直玩！一直玩！一直玩！一直玩！一直玩！一直玩！',
        '禁止沒水準觀光客來逢甲',
        '逢甲應設立學生工會以保障學生在學勞動權',
        '在摩斯門和大門中間的圍牆開一個門',
    ];

    return [
        'politics' => implode("\n", $faker->randomElements($politicsList, rand(1, count($politicsList)))),
    ];
});
