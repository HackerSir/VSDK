<?php

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Hackersir\User::class, function (Faker\Generator $faker) {
    return [
        'name'       => $faker->name,
        'email'      => $faker->email,
        'confirm_at' => $faker->dateTime,
    ];
});

$factory->defineAs(Hackersir\User::class, 'noConfirm', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(Hackersir\User::class);

    return array_merge($user, [
        'confirm_code' => str_random(60),
        'confirm_at'   => null,
    ]);
});
