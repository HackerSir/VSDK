<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ModifyBallotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 關閉外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // 為 ballots 加入 category_election_id
        Schema::table('ballots', function (Blueprint $table) {
            $table->integer('category_election_id')->unsigned();
            $table->foreign('category_election_id')
                ->references('id')->on('category_election')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 開啟外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        /** @var \Illuminate\Database\Eloquent\Collection $ballots */
        $ballots = DB::table('ballots')->get();
        foreach ($ballots as $ballot) {
            $categoryElectionRelation = DB::table('category_election')
                ->where('category_id', '=', $ballot->category_id)
                ->where('election_id', '=', $ballot->election_id)->first();

            if ($categoryElectionRelation != null) {
                $id = $categoryElectionRelation->id;
            } else {
                $id = DB::table('category_election')->insertGetId([
                    'category_id' => $ballot->category_id,
                    'election_id' => $ballot->election_id,
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now(),
                ]);
            }

            DB::table('ballots')->where('id', '=', $ballot->id)->update([
                'category_election_id' => $id,
                'updated_at'           => Carbon::now(),
            ]);
        }

        // 移除 ballots 的 category_id、election_id
        Schema::table('ballots', function (Blueprint $table) {
            $table->dropForeign('ballots_category_id_foreign');
            $table->dropForeign('ballots_election_id_foreign');

            $table->dropColumn('category_id');
            $table->dropColumn('election_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 關閉外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // 加入 groups 的 category_id、election_id
        Schema::table('ballots', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('election_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('election_id')->references('id')->on('elections')
                ->onUpdate('cascade')->onDelete('cascade');
        });
        // 開啟外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        //複製資料
        /** @var \Hackersir\CategoryElectionRelation[] $categoryElectionRelations */
        $categoryElectionRelations = DB::table('category_election')->get();
        foreach ($categoryElectionRelations as $categoryElectionRelation) {
            DB::table('ballots')->where('category_election_id', '=', $categoryElectionRelation->id)->update([
                'category_id' => $categoryElectionRelation->category_id,
                'election_id' => $categoryElectionRelation->election_id,
                'updated_at'  => Carbon::now(),
            ]);
        }

        // 移除 ballots 的 category_election 關係
        Schema::table('ballots', function (Blueprint $table) {
            $table->dropForeign('ballots_category_election_id_foreign');
            $table->dropColumn('category_election_id');
        });
    }
}
