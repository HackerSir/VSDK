<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateElectionCountPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permElectionCountStart = new Permission();
        $permElectionCountStart->name = 'election.count.start';
        $permElectionCountStart->display_name = '開始開票';
        $permElectionCountStart->description = '開始指定選舉的開票';
        $permElectionCountStart->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permElectionCountStart);

        $permElectionVotingStationOver = new Permission();
        $permElectionVotingStationOver->name = 'election.voting-station.over';
        $permElectionVotingStationOver->display_name = '完成開票';
        $permElectionVotingStationOver->description = '結束選舉某投票站的開票';
        $permElectionVotingStationOver->save();

        $role = Role::where('name', 'counting_staff')->first();
        $role->attachPermission($permElectionVotingStationOver);

        $permCountBallot = new Permission();
        $permCountBallot->name = 'election.count.ballot';
        $permCountBallot->display_name = '開票';
        $permCountBallot->description = '寫入新的選票資料';
        $permCountBallot->save();

        $role = Role::where('name', 'counting_staff')->first();
        $role->attachPermission($permCountBallot);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'election.count.start')->delete();
        Permission::where('name', 'election.voting-station.over')->delete();
        Permission::where('name', 'election.count.ballot')->delete();
    }
}
