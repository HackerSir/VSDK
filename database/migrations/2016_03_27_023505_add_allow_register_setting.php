<?php

use Hackersir\Setting;
use Illuminate\Database\Migrations\Migration;

class AddAllowRegisterSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Setting::create([
            'name' => 'AllowRegister',
            'type' => 'boolean',
            'desc' => '是否允許註冊',
            'data' => 'true',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Setting::where('name', 'AllowRegister')->delete();
    }
}
