<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateEditCountingStaffPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permEditCountingStaff = new Permission();
        $permEditCountingStaff->name = 'election.edit-counting-staff';
        $permEditCountingStaff->display_name = '編輯開票員';
        $permEditCountingStaff->description = '編輯自己所管理選舉之投票所開票員';
        $permEditCountingStaff->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permEditCountingStaff);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'election.edit-counting-staff')->delete();
    }
}
