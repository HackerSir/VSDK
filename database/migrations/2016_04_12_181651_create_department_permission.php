<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permCreateDepartment = new Permission();
        $permCreateDepartment->name = 'department.create';
        $permCreateDepartment->display_name = '建立科系';
        $permCreateDepartment->description = '新增新的科系';
        $permCreateDepartment->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permCreateDepartment);

        $permEditDepartment = new Permission();
        $permEditDepartment->name = 'department.edit';
        $permEditDepartment->display_name = '編輯科系';
        $permEditDepartment->description = '編輯科系資料';
        $permEditDepartment->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permEditDepartment);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'department.create')->delete();
        Permission::where('name', 'department.edit')->delete();
    }
}
