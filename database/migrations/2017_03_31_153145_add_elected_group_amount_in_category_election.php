<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddElectedGroupAmountInCategoryElection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_election', function (Blueprint $table) {
            $table->integer('elected_group_amount')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_election', function (Blueprint $table) {
            $table->dropColumn('elected_group_amount');
        });
    }
}
