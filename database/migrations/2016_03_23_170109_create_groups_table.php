<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //候選人組
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->unsigned();  //號次
            $table->text('politics');               //政見
            $table->integer('election_id')->unsigned();

            $table->foreign('election_id')->references('id')->on('elections')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groups');
    }
}
