<?php

use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //新增 選務人員 角色
        $role = new Role();
        $role->name = 'staff';
        $role->display_name = '選務人員';
        $role->description = '管理選舉資料，負責開票流程。';
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        //移除 選務人員 角色
        Role::where('name', 'staff')->delete();
    }
}
