<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateUserManagePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permUserManage = new Permission();
        $permUserManage->name = 'user.manage';
        $permUserManage->display_name = '管理會員';
        $permUserManage->description = '修改會員資料、調整會員權限、刪除會員等';
        $permUserManage->save();

        $permUserView = new Permission();
        $permUserView->name = 'user.view';
        $permUserView->display_name = '查看會員資料';
        $permUserView->description = '查看會員清單、資料、權限等';
        $permUserView->save();

        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($permUserManage);
        $admin->attachPermission($permUserView);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'user.manage')->delete();
        Permission::where('name', 'user.view')->delete();
    }
}
