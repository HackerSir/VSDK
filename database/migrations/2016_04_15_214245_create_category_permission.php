<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permCreateCategory = new Permission();
        $permCreateCategory->name = 'category.create';
        $permCreateCategory->display_name = '建立選舉分組';
        $permCreateCategory->description = '為網站建立新的選舉分組';
        $permCreateCategory->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permCreateCategory);

        $permEditCategory = new Permission();
        $permEditCategory->name = 'category.edit';
        $permEditCategory->display_name = '編輯選舉分組';
        $permEditCategory->description = '編輯現有的選舉分組';
        $permEditCategory->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permEditCategory);

        $permCreateRelationCategory = new Permission();
        $permCreateRelationCategory->name = 'category.relation.create';
        $permCreateRelationCategory->display_name = '為選舉新增分組';
        $permCreateRelationCategory->description = '新增分組到指定的選舉（只能在活動未公開的時候）';
        $permCreateRelationCategory->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permCreateRelationCategory);

        $permEditRelationCategory = new Permission();
        $permEditRelationCategory->name = 'category.relation.edit';
        $permEditRelationCategory->display_name = '編輯選舉底下分組的資料';
        $permEditRelationCategory->description = '編輯選舉底下分組的資料（只能在活動未公開的時候）';
        $permEditRelationCategory->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permEditRelationCategory);

        $permDeleteRelationCategory = new Permission();
        $permDeleteRelationCategory->name = 'category.relation.delete';
        $permDeleteRelationCategory->display_name = '刪除選舉底下的分組';
        $permDeleteRelationCategory->description = '刪除選舉底下的分組（只能在活動未公開的時候）';
        $permDeleteRelationCategory->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permDeleteRelationCategory);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'category.create')->delete();
        Permission::where('name', 'category.edit')->delete();
        Permission::where('name', 'category.relation.create')->delete();
        Permission::where('name', 'category.relation.edit')->delete();
        Permission::where('name', 'category.relation.delete')->delete();
    }
}
