<?php

use Hackersir\Permission;
use Illuminate\Database\Migrations\Migration;

class CreateLogViewerPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new Permission();
        $permission->name = 'log-viewer.access';
        $permission->display_name = '進入 Log Viewer 面板';
        $permission->description = '進入 Log Viewer 面板，對網站記錄進行查詢與管理';
        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'log-viewer.access')->delete();
    }
}
