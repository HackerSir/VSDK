<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateEditStaffPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permEditStaff = new Permission();
        $permEditStaff->name = 'election.edit-staff';
        $permEditStaff->display_name = '編輯選務人員';
        $permEditStaff->description = '修改選舉之選務人員名單';
        $permEditStaff->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permEditStaff);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'election.edit-staff')->delete();
    }
}
