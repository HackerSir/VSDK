<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class AddActivityViewPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //建立新增活動的權限
        $viewPermission = new Permission();
        $viewPermission->name = 'activity.view';
        $viewPermission->display_name = '檢視未公開的活動';
        $viewPermission->description = '檢視未公開的活動';
        $viewPermission->save();

        Role::where('name', 'admin')->first()->attachPermission($viewPermission);
        Role::where('name', 'staff')->first()->attachPermission($viewPermission);
        Role::where('name', 'counting_staff')->first()->attachPermission($viewPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        //移除編輯活動的權限
        Permission::where('name', 'activity.view')->delete();
    }
}
