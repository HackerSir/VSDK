<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateGroupPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permCreateGroup = new Permission();
        $permCreateGroup->name = 'group.create';
        $permCreateGroup->display_name = '建立候選人組';
        $permCreateGroup->description = '建立新的候選人組（只能在活動未公開的時候）';
        $permCreateGroup->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permCreateGroup);

        $permEditGroup = new Permission();
        $permEditGroup->name = 'group.edit';
        $permEditGroup->display_name = '編輯候選人組';
        $permEditGroup->description = '編輯候選人組（只能在活動未公開的時候）';
        $permEditGroup->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permEditGroup);

        $permDeleteGroup = new Permission();
        $permDeleteGroup->name = 'group.delete';
        $permDeleteGroup->display_name = '刪除候選人組';
        $permDeleteGroup->description = '刪除候選人組（只能在活動未公開的時候）';
        $permDeleteGroup->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permDeleteGroup);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'group.create')->delete();
        Permission::where('name', 'group.edit')->delete();
        Permission::where('name', 'group.delete')->delete();
    }
}
