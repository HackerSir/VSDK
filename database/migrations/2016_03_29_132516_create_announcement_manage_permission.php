<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementManagePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permAnnouncementManage = new Permission();
        $permAnnouncementManage->name = 'announcement.manage';
        $permAnnouncementManage->display_name = '管理公告';
        $permAnnouncementManage->description = '新增、編輯、刪除公告';
        $permAnnouncementManage->save();


        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($permAnnouncementManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'announcement.manage')->delete();
    }
}
