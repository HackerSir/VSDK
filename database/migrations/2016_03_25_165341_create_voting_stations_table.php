<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVotingStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //投票所
        Schema::create('voting_stations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');     //名稱
            $table->string('location'); //位置
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('voting_stations');
    }
}
