<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ModifyGroupsRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 關閉外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // 先將 groups 連到 category_election
        Schema::table('groups', function (Blueprint $table) {
            $table->integer('category_election_id')->unsigned();
            $table->foreign('category_election_id')
                ->references('id')->on('category_election')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        // 開啟外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // 複製資料到 category_election
        /** @var \Illuminate\Database\Eloquent\Collection $groups */
        $groups = DB::table('groups')->get();
        foreach ($groups as $group) {
            $categoryElectionRelation = DB::table('category_election')
                ->where('category_id', '=', $group->category_id)
                ->where('election_id', '=', $group->election_id)->first();

            if ($categoryElectionRelation != null) {
                $id = $categoryElectionRelation->id;
            } else {
                $id = DB::table('category_election')->insertGetId([
                    'category_id' => $group->category_id,
                    'election_id' => $group->election_id,
                    'created_at'  => Carbon::now(),
                    'updated_at'  => Carbon::now(),
                ]);
            }

            DB::table('groups')->where('id', '=', $group->id)->update([
                'category_election_id' => $id,
                'updated_at'           => Carbon::now(),
            ]);
        }

        // 移除 groups 的 category_id、election_id
        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign('groups_category_id_foreign');
            $table->dropForeign('groups_election_id_foreign');

            $table->dropColumn('category_id');
            $table->dropColumn('election_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 關閉外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // 加入 groups 的 category_id、election_id
        Schema::table('groups', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('election_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('election_id')->references('id')->on('elections')
                ->onUpdate('cascade')->onDelete('cascade');
        });
        // 開啟外鍵檢查
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        //複製資料
        /** @var \Hackersir\CategoryElectionRelation[] $categoryElectionRelations */
        $categoryElectionRelations = DB::table('category_election')->get();
        foreach ($categoryElectionRelations as $categoryElectionRelation) {
            DB::table('groups')->where('category_election_id', '=', $categoryElectionRelation->id)->update([
                'category_id' => $categoryElectionRelation->category_id,
                'election_id' => $categoryElectionRelation->election_id,
                'updated_at'  => Carbon::now(),
            ]);
        }

        // 移除 groups 的 category_election 關係
        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign('groups_category_election_id_foreign');
            $table->dropColumn('category_election_id');
        });
    }
}
