<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateImgurImageManagePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permImgurImageManage = new Permission();
        $permImgurImageManage->name = 'imgur-image.manage';
        $permImgurImageManage->display_name = '管理Imgur圖片';
        $permImgurImageManage->description = '檢視或刪除已上傳的Imgur圖片';
        $permImgurImageManage->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permImgurImageManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'imgur-image.manage')->delete();
    }
}
