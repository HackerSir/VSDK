<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionIndexAccessPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new Permission();
        $permission->name = 'permission.index.access';
        $permission->display_name = '進入權限面板';
        $permission->description = '進入權限面板，查看各角色權限清單';
        $permission->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'permission.index.access')->delete();
    }
}
