<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateElectionPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //建立新增選舉的權限
        $createPermission = new Permission();
        $createPermission->name = 'election.create';
        $createPermission->display_name = '建立選舉';
        $createPermission->description = '為活動新增選舉';
        $createPermission->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($createPermission);

        //建立編輯選舉的權限
        $editPermission = new Permission();
        $editPermission->name = 'election.edit';
        $editPermission->display_name = '編輯選舉';
        $editPermission->description = '編輯選舉的資料';
        $editPermission->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($editPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        //移除新增選舉的權限
        Permission::where('name', 'election.create')->delete();

        //移除編輯選舉的權限
        Permission::where('name', 'election.edit')->delete();
    }
}
