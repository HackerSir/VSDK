<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElectionVotingStationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('election_voting_station', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('election_id')->unsigned();
            $table->integer('voting_station_id')->unsigned();
            $table->boolean('counting_over')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('election_id')->references('id')->on('elections')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('voting_station_id')->references('id')->on('voting_stations')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('election_voting_station');
    }
}
