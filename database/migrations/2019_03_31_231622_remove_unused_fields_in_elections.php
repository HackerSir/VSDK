<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveUnusedFieldsInElections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elections', function (Blueprint $table) {
            $table->dropColumn('reporting_link');
            $table->dropColumn('results_report_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elections', function (Blueprint $table) {
            $table->text('reporting_link')->nullable()->after('introduction');      //公報連結
            $table->text('results_report_link')->nullable()->after('reporting_link'); //結果報告連結
        });
    }
}
