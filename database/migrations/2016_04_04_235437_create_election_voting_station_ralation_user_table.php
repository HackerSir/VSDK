<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElectionVotingStationRalationUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('election_voting_station_relation_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('election_voting_station_relation_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('election_voting_station_relation_id', 'relation_user_relation_fk')
                ->references('id')->on('election_voting_station')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id', 'relation_user_user_fk')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('election_voting_station_relation_user');
    }
}
