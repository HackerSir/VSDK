<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permCreateCandidate = new Permission();
        $permCreateCandidate->name = 'candidate.create';
        $permCreateCandidate->display_name = '建立候選人';
        $permCreateCandidate->description = '建立候選人（只能在活動未公開的時候）';
        $permCreateCandidate->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permCreateCandidate);

        $permEditCandidate = new Permission();
        $permEditCandidate->name = 'candidate.edit';
        $permEditCandidate->display_name = '編輯候選人';
        $permEditCandidate->description = '編輯候選人（只能在活動未公開的時候）';
        $permEditCandidate->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permEditCandidate);

        $permDeleteCandidate = new Permission();
        $permDeleteCandidate->name = 'candidate.delete';
        $permDeleteCandidate->display_name = '刪除候選人';
        $permDeleteCandidate->description = '刪除候選人（只能在活動未公開的時候）';
        $permDeleteCandidate->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permDeleteCandidate);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'candidate.create')->delete();
        Permission::where('name', 'candidate.edit')->delete();
        Permission::where('name', 'candidate.delete')->delete();
    }
}
