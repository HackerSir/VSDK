<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateTreeAccessPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permVotingStationManage = new Permission();
        $permVotingStationManage->name = 'tree.access';
        $permVotingStationManage->display_name = '查看資源樹狀圖';
        $permVotingStationManage->description = '進入資源樹狀圖頁面';
        $permVotingStationManage->save();

        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($permVotingStationManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'tree.access')->delete();
    }
}
