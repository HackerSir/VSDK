<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //候選人
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');

            $table->text('photo_url');   //照片(imgur_ID)
            $table->text('name');        //姓名
            $table->text('gender');      //性別(男(male)、女(female))
            $table->integer('age');      //年齡(age)
            $table->integer('grade');    //年級
            //系所用外鍵，會在別的 migration 加入
            $table->integer('class_no'); //班級(0 => 無, 1 => 甲)
            $table->text('experience');  //經歷
            $table->integer('group_id')->unsigned(); //所屬候選人組

            $table->foreign('group_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
