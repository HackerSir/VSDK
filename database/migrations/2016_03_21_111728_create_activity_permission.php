<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateActivityPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //建立新增活動的權限
        $createPermission = new Permission();
        $createPermission->name = 'activity.create';
        $createPermission->display_name = '建立活動';
        $createPermission->description = '為網站新增活動';
        $createPermission->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($createPermission);

        //建立編輯活動的權限
        $editPermission = new Permission();
        $editPermission->name = 'activity.edit';
        $editPermission->display_name = '編輯活動';
        $editPermission->description = '編輯活動的資料，以及包含哪些選舉';
        $editPermission->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($editPermission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        //移除新增活動的權限
        Permission::where('name', 'activity.create')->delete();

        //移除編輯活動的權限
        Permission::where('name', 'activity.edit')->delete();
    }
}
