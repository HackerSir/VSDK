<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MakeNonRequiredFieldNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->text('image')->nullable()->change();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->text('image')->nullable()->change();
        });
        //清除歷史傷痕
        DB::statement('ALTER TABLE activities
MODIFY COLUMN vote_start TIMESTAMP NULL,
MODIFY COLUMN vote_end TIMESTAMP NULL,
MODIFY COLUMN billing_start TIMESTAMP NULL');
        Schema::table('activities', function (Blueprint $table) {
            $table->text('process')->nullable()->change();
            $table->text('count_help_text')->nullable()->change();
            $table->text('reporting_link')->nullable()->change();
            $table->text('results_report_link')->nullable()->change();
        });
        Schema::table('election_voting_station', function (Blueprint $table) {
            $table->text('video')->nullable()->change();
        });
        Schema::table('candidates', function (Blueprint $table) {
            $table->text('photo_url')->nullable()->change();
            $table->text('experience')->nullable()->change();
            $table->string('title')->nullable()->change();
        });
        Schema::table('elections', function (Blueprint $table) {
            $table->text('image')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->text('image')->nullable(false)->change();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->text('image')->nullable(false)->change();
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->text('process')->nullable(false)->change();
            $table->text('count_help_text')->nullable(false)->change();
            $table->text('reporting_link')->nullable(false)->change();
            $table->text('results_report_link')->nullable(false)->change();
        });
        Schema::table('election_voting_station', function (Blueprint $table) {
            $table->text('video')->nullable(false)->change();
        });
        Schema::table('candidates', function (Blueprint $table) {
            $table->text('photo_url')->nullable(false)->change();
            $table->text('experience')->nullable(false)->change();
            $table->string('title')->nullable(false)->change();
        });
        Schema::table('elections', function (Blueprint $table) {
            $table->text('image')->nullable(false)->change();
        });
    }
}
