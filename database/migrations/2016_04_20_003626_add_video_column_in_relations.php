<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddVideoColumnInRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('election_voting_station', function (Blueprint $table) {
            $table->text('video')->after('counting_over');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('election_voting_station', function (Blueprint $table) {
            $table->dropColumn('video');
        });
    }
}
