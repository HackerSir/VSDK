<?php

use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class ModifyAdminDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //修改 Admin 的描述
        Role::where('name', 'admin')
            ->update(['description' => '管理活動、使用者，查看系統狀態']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //回復 Admin 的描述
        Role::where('name', 'admin')
            ->update(['description' => '擁有最高權限的網站管理者']);
    }
}
