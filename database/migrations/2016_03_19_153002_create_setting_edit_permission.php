<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateSettingEditPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new Permission();
        $permission->name = 'setting.edit';
        $permission->display_name = '修改網站設定';
        $permission->description = '進入設定面板，檢視、修改網站設定';
        $permission->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'setting.edit')->delete();
    }
}
