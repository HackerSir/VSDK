<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationManagePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permImgurImageManage = new Permission();
        $permImgurImageManage->name = 'notification.manage';
        $permImgurImageManage->display_name = '管理活動公告';
        $permImgurImageManage->description = '管理活動開票頁面上方的公告或警告訊息';
        $permImgurImageManage->save();

        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($permImgurImageManage);
        $staff = Role::where('name', 'staff')->first();
        $staff->attachPermission($permImgurImageManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'notification.manage')->delete();
    }
}
