<?php

use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateCountingStaffRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //新增 開票員 角色
        $role = new Role();
        $role->name = 'counting_staff';
        $role->display_name = '開票員';
        $role->description = '負責開票工作。';
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        //移除 開票員 角色
        Role::where('name', 'counting_staff')->delete();
    }
}
