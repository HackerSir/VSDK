<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MakeNonRequiredFieldInElectionsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elections', function (Blueprint $table) {
            $table->text('reporting_link')->nullable()->change();      //公報連結
            $table->text('results_report_link')->nullable()->change(); //結果報告連結
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elections', function (Blueprint $table) {
            $table->text('reporting_link')->change();      //公報連結
            $table->text('results_report_link')->change(); //結果報告連結
        });
    }
}
