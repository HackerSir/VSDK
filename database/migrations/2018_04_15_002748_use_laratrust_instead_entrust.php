<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UseLaratrustInsteadEntrust extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('role_user_tmp');
        // Remove original role_user
        $data = DB::table('role_user')->select('*')->get()->map(function ($i) {
            return ['user_id' => $i->user_id, 'role_id' => $i->role_id, 'user_type' => \Hackersir\User::class];
        })->all();
        Schema::dropIfExists('role_user');
        Schema::create('role_user_tmp', function (Blueprint $table) {
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('user_id');
            $table->string('user_type');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->primary(['user_id', 'role_id', 'user_type']);
        });
        // Move Data from role_user to role_user_tmp
        DB::table('role_user_tmp')->insert($data);
        // Rename
        Schema::rename('role_user_tmp', 'role_user');

        // Create table for associating permissions to users (Many To Many Polymorphic)
        Schema::create('permission_user', function (Blueprint $table) {
            $table->unsignedInteger('permission_id');
            $table->unsignedInteger('user_id');
            $table->string('user_type');
            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->primary(['user_id', 'permission_id', 'user_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_user_tmp');
        // Remove original role_user
        $data = DB::table('role_user')->select('*')->get()->map(function ($i) {
            return ['user_id' => $i->user_id, 'role_id' => $i->role_id];
        })->all();
        Schema::dropIfExists('role_user');
        Schema::create('role_user_tmp', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });
        DB::table('role_user_tmp')->insert($data);
        // Rename
        Schema::rename('role_user_tmp', 'role_user');

        Schema::dropIfExists('permission_user');

    }
}
