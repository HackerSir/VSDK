<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateActivityVideoManagePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permImgurImageManage = new Permission();
        $permImgurImageManage->name = 'activity.video.manage';
        $permImgurImageManage->display_name = '管理活動直播影片';
        $permImgurImageManage->description = '管理每個活動在每個投票所的直播影片網址';
        $permImgurImageManage->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permImgurImageManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'activity.video.manage')->delete();
    }
}
