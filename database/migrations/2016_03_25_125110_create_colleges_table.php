<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCollegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //學院
        Schema::create('colleges', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');         //名稱
            $table->string('abbreviation'); //簡稱
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('colleges');
    }
}
