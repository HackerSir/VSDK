<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->display_name = '管理員';
        $role->description = '擁有最高權限的網站管理者';
        $role->save();

        $permission = Permission::where('name', 'log-viewer.access')->first();
        $role->attachPermission($permission);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Role::where('name', 'admin')->delete();
    }
}
