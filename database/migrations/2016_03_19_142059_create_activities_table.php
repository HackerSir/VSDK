<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');              //活動名稱
            $table->timestamp('vote_start')->nullable();     //投票開始時間
            $table->timestamp('vote_end')->nullable();       //投票結束時間
            $table->timestamp('billing_start')->nullable();  //開票開始時間
            $table->text('process');             //活動流程(像是登記時間、抽籤日)
            $table->text('reporting_link');      //公報連結
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activities');
    }
}
