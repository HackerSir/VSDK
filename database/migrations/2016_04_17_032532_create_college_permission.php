<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateCollegePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permCreateCollege = new Permission();
        $permCreateCollege->name = 'college.create';
        $permCreateCollege->display_name = '建立學院';
        $permCreateCollege->description = '新增新的學院';
        $permCreateCollege->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permCreateCollege);

        $permEditCollege = new Permission();
        $permEditCollege->name = 'college.edit';
        $permEditCollege->display_name = '編輯學院';
        $permEditCollege->description = '編輯學院資料';
        $permEditCollege->save();

        $role = Role::where('name', 'admin')->first();
        $role->attachPermission($permEditCollege);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'college.create')->delete();
        Permission::where('name', 'college.edit')->delete();
    }
}
