<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCommentsInSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name')->nullable()->comment('組別名稱')->change();
        });
        Schema::table('colleges', function (Blueprint $table) {
            $table->string('name')->comment('名稱')->change();
            $table->string('abbreviation')->comment('簡稱')->change();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->string('name')->comment('名稱')->change();
            $table->string('abbreviation')->comment('簡稱')->change();
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->integer('number')->unsigned()->comment('號次')->change();
            $table->text('politics')->comment('政見')->change();
        });
        Schema::table('imgur_images', function (Blueprint $table) {
            $table->string('file_name')->comment('完整原始檔名')->change();
            $table->string('extension')->comment('副檔名')->change();
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->string('type')->default('info')->comment('類型：success、info、warning、danger')->change();
            $table->text('text')->comment('內文')->change();
            $table->integer('user_id')->unsigned()->nullable()->comment('發布者ID')->change();
            $table->integer('activity_id')->unsigned()->comment('所屬活動ID')->change();
        });
        Schema::table('voting_stations', function (Blueprint $table) {
            $table->string('name')->comment('名稱')->change();
            $table->string('location')->comment('地點')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('name')->nullable()->comment(null)->change();
        });
        Schema::table('colleges', function (Blueprint $table) {
            $table->string('name')->comment(null)->change();
            $table->string('abbreviation')->comment(null)->change();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->string('name')->comment(null)->change();
            $table->string('abbreviation')->comment(null)->change();
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->integer('number')->unsigned()->comment(null)->change();
            $table->text('politics')->comment(null)->change();
        });
        Schema::table('imgur_images', function (Blueprint $table) {
            $table->string('file_name')->comment(null)->change();
            $table->string('extension')->comment(null)->change();
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->string('type')->default('info')->comment(null)->change();
            $table->text('text')->comment(null)->change();
            $table->integer('user_id')->unsigned()->nullable()->comment(null)->change();
            $table->integer('activity_id')->unsigned()->comment(null)->change();
        });
        Schema::table('voting_stations', function (Blueprint $table) {
            $table->string('name')->comment(null)->change();
            $table->string('location')->comment(null)->change();
        });
    }
}
