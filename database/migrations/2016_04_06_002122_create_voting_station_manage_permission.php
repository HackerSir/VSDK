<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateVotingStationManagePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permVotingStationManage = new Permission();
        $permVotingStationManage->name = 'voting-station.manage';
        $permVotingStationManage->display_name = '管理投票所';
        $permVotingStationManage->description = '新增、編輯投票所';
        $permVotingStationManage->save();

        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($permVotingStationManage);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'voting-station.manage')->delete();
    }
}
