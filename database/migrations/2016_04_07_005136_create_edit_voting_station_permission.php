<?php

use Hackersir\Permission;
use Hackersir\Role;
use Illuminate\Database\Migrations\Migration;

class CreateEditVotingStationPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permEditStaff = new Permission();
        $permEditStaff->name = 'election.edit-voting-station';
        $permEditStaff->display_name = '編輯選舉之投票所';
        $permEditStaff->description = '修改選舉所對應之投票所清單';
        $permEditStaff->save();

        $role = Role::where('name', 'staff')->first();
        $role->attachPermission($permEditStaff);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Permission::where('name', 'election.edit-voting-station')->delete();
    }
}
