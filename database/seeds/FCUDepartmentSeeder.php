<?php

use Hackersir\College;
use Hackersir\Department;
use Illuminate\Database\Seeder;

/**
 * 產生逢甲大學部學系
 */
class FCUDepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colleges = [
            ['商學院', '商學院'],
            ['工學院', '工學院'],
            ['建設學院', '建設學院'],
            ['金融學院', '金融學院'],
            ['資訊電機學院', '資電學院'],
            ['人文社會學院', '人文社會學院'],
            ['理學院', '理學院'],
        ];

        $departments = [
            '商學院'    => [
                ['會計學系', '會計'],
                ['國際貿易學系', '國貿'],
                ['財稅學系', '財稅'],
                ['合作經濟學系', '合經'],
                ['統計學系', '統計'],
                ['經濟學系', '經濟'],
                ['企業管理學系', '企管'],
                ['行銷學系', '行銷'],
            ],
            '工學院'    => [
                ['機械與電腦輔助工程學系', '機電'],
                ['纖維與複合材料學系', '纖複'],
                ['工業工程與系統管理學系', '工工'],
                ['化學工程學系', '化工'],
                ['航太與系統工程學系', '航太'],
                ['精密系統設計學士學位學程', '精密'],
            ],
            '建設學院'   => [
                ['土木工程學系', '土木'],
                ['水利工程與資源保育學系', '水利'],
                ['建築學系', '建築'],
                ['都市計畫與空間資訊學系', '都資'],
                ['運輸科技與管理學系', '運管'],
                ['土地管理學系', '土管'],
            ],
            '金融學院'   => [
                ['風險管理與保險學系', '風保'],
                ['財務金融學系', '財金'],
                ['財務工程與精算學士學位學程', '財精'],
            ],
            '資電學院'   => [
                ['資訊工程學系', '資訊'],
                ['電機工程學系', '電機'],
                ['電子工程學系', '電子'],
                ['自動控制工程學系', '自控'],
                ['通訊工程學系', '通訊'],
            ],
            '人文社會學院' => [
                ['中國文學系', '中文'],
                ['外國語文學系', '外文'],
            ],
            '理學院'    => [
                ['應用數學系', '應數'],
                ['環境工程與科學學系', '環工'],
                ['材料科學與工程學系', '材料'],
                ['光電學系', '光電'],
            ],
        ];

        foreach ($colleges as $key => $value) {
            if (!(College::where('name', '=', $value[0])->exists())) {
                $college = new College;
                $college->name = $value[0];
                $college->abbreviation = $value[1];
                $college->save();
            }
        }

        foreach ($departments as $key => $value) {
            $college = College::where('abbreviation', '=', $key)->first();

            foreach ($value as $item) {
                if (!(Department::where('name', '=', $item[0])->exists())) {
                    $department = new Department;
                    $department->name = $item[0];
                    $department->abbreviation = $item[1];
                    $department->college_id = $college->id;
                    $department->save();
                }
            }
        }
    }
}
