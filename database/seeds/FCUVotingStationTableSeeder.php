<?php

use Hackersir\VotingStation;
use Illuminate\Database\Seeder;

class FCUVotingStationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $votingStations = [
            ['忠勤站(第一投票所)', '忠勤樓前榕榕大道'],
            ['行政館站(第二投票所)', '行政一館前榕榕大道'],
            ['圖書館站(第三投票所)', '丘逢甲紀念館前'],
            ['育樂館站(第四投票所)', '課外活動組外榕樹下'],
            ['資電站(第五投票所)', '資訊電機館川堂'],
            ['商學站(第六投票所)', '商學館一樓大廳'],
            ['學思站(第七投票所)', '摩斯門旁'],
            ['體育館站(第八投票所)', '體育館與校園北門之廣場'],
        ];

        foreach ($votingStations as $info) {
            if (!(VotingStation::where('name', '=', $info[0])->exists())) {
                $votingStation = new VotingStation;
                $votingStation->name = $info[0];
                $votingStation->location = $info[1];
                $votingStation->save();
            }
        }
    }
}
