<?php

use Carbon\Carbon;
use Hackersir\Activity;
use Hackersir\Candidate;
use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Group;
use Hackersir\Utils\ActivityStatus;
use Hackersir\VotingStation;
use Illuminate\Database\Seeder;

class ActivitiesSeeder extends Seeder
{
    /**
     * 活動
     *     準備中(+1W+6H, +1W+12H, +1W+18H)、0個選舉
     *     準備中(+1W+6H, +1W+12H, +1W+18H)、3個選舉(無分組、1個分組、4個分組)
     *     公開中(+1W+6H, +1W+12H, +1W+18H)、3個選舉(1個分組、4個分組、4個分組)
     *     投票中(-1W+6H, +1W, +1W1D)、3個選舉(1個分組、4個分組、4個分組)
     *     等待開票(-1W+6H, -1D, +1W)、3個選舉(1個分組、4個分組、4個分組)
     *     開票中(-1W+6H, -5D, -3D)、3個選舉(1個分組、4個分組、4個分組)
     *     已結束(-1W+6H, -3D, -1D)、3個選舉(1個分組、4個分組、4個分組)
     *
     * 選舉
     *     都是九個投票所
     *     每個選舉都有一個選務人員
     *     每個投票所都有一位開票人員
     *
     * 分組
     *     1組1人、1組2人
     *     2組1人、2組2人
     *
     * 選票
     *     1組1人、2組1人
     *         100 贊成票、100 反對票、50 廢票、25 錯誤票
     *     1組2人、2組2人
     *         150 第一組票、150 第二組票、50 廢票、25 錯誤票
     */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::today();

        /** @var Activity $activity */
        // 準備中(+1W+6H, +1W+12H, +1W+18H)、0個選舉
        $activity = factory(Activity::class)->make();
        $activity->name = '[準備中][0個選舉] 活動';
        $activity->vote_start = $now->copy()->addWeek()->addHours(6);
        $activity->vote_end = $now->copy()->addWeek()->addHours(12);
        $activity->billing_start = $now->copy()->addWeek()->addHours(18);
        $activity->publicity = false;
        $activity->save();

        // 準備中(+1W+6H, +1W+12H, +1W+18H)、3個選舉(無分組、1個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[準備中][3個選舉] 活動';
        $activity->vote_start = $now->copy()->addWeek()->addHours(6);
        $activity->vote_end = $now->copy()->addWeek()->addHours(12);
        $activity->billing_start = $now->copy()->addWeek()->addHours(18);
        $activity->publicity = false;
        $activity->save();

        $this->createElections($activity, [0, 1, 4]);

        // 公開中(+1W+6H, +1W+12H, +1W+18H)、3個選舉(1個分組、4個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[公開中][3個選舉] 活動';
        $activity->vote_start = $now->copy()->addWeek()->addHours(6);
        $activity->vote_end = $now->copy()->addWeek()->addHours(12);
        $activity->billing_start = $now->copy()->addWeek()->addHours(18);
        $activity->publicity = true;
        $activity->save();

        $this->createElections($activity, [1, 1, 4]);

        // 投票中(-1W+6H, +1W, +1W1D)、3個選舉(1個分組、4個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[投票中][3個選舉] 活動';
        $activity->vote_start = $now->copy()->subWeek()->addHours(6);
        $activity->vote_end = $now->copy()->addWeek();
        $activity->billing_start = $now->copy()->addWeek()->addDay();
        $activity->publicity = true;
        $activity->save();

        $this->createElections($activity, [1, 1, 4]);

        // 等待開票(-1W+6H, -1D, +1W)、3個選舉(1個分組、4個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[等待開票][3個選舉] 活動';
        $activity->vote_start = $now->copy()->subWeek()->addHours(6);
        $activity->vote_end = $now->copy()->subDay();
        $activity->billing_start = $now->copy()->addWeek();
        $activity->publicity = true;
        $activity->save();

        $this->createElections($activity, [1, 1, 4]);

        // 開票中(-1W+6H, -5D, -3D)、3個選舉(1個分組、4個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[開票中][3個選舉] 活動';
        $activity->vote_start = $now->copy()->subWeek()->addHours(6);
        $activity->vote_end = $now->copy()->subDays(5);
        $activity->billing_start = $now->copy()->subHours(3);
        $activity->publicity = true;
        $activity->save();

        $this->createElections($activity, [1, 1, 4]);
        $this->addBallot($activity);

        // 已結束(-1W+6H, -5D, -3D)、3個選舉(1個分組、4個分組、4個分組)
        $activity = factory(Activity::class)->make();
        $activity->name = '[已結束][3個選舉] 活動';
        $activity->vote_start = $now->copy()->subWeek()->addHours(6);
        $activity->vote_end = $now->copy()->subDays(5);
        $activity->billing_start = $now->copy()->subDays(3);
        $activity->publicity = true;
        $activity->save();

        $this->createElections($activity, [1, 1, 4]);
        $this->addBallot($activity);
        $this->setAllElectionFinish($activity);
    }

    /**
     * @param Activity $activity
     * @param $categoryCounts
     */
    public function createElections(Activity $activity, $categoryCounts)
    {
        foreach ($categoryCounts as $index => $count) {
            $this->createElection($activity, $index, $count);
        }
    }

    /**
     * @param Activity $activity
     * @param int $order
     * @param int $categoryCount
     */
    public function createElection(Activity $activity, $order, $categoryCount)
    {
        /** @var Election $election */
        $election = factory(Election::class)->make([
            'order' => $order,
        ]);
        $activity->elections()->save($election);

        // TODO: 設定一位選務人員

        // 設定投票所
        $votingStationsId = VotingStation::pluck('id')->toArray();
        $election->votingStations()->sync($votingStationsId);

        // TODO: 為每一個開票所設定一位開票員

        // 設定分組
        if ($categoryCount > 0) {
            $categoriesIds = Category::pluck('id')->random($categoryCount)->toArray();
            $election->categories()->sync($categoriesIds);
        }

        // 為每一個分組加上候選組
        foreach ($election->categories as $index => $category) {
            $this->addGroup($election, $category, $index);
        }
    }

    /**
     * @param Election $election
     * @param Category $category
     * @param int $type
     */
    public function addGroup(Election $election, Category $category, $type)
    {
        /** @var CategoryElectionRelation $relation */
        $relation = CategoryElectionRelation::findRelation($category, $election)->first();
        /**
         * [0] 1組1人
         * [1] 1組2人
         * [2] 2組1人
         * [3] 2組2人
         */
        $info = [
            [1, 1],
            [1, 2],
            [2, 1],
            [2, 2],
        ];
        $titles = ['學生會會長', '第一副會長'];
        $groupsCount = $info[$type % 4][0];
        $candidatesCount = $info[$type % 4][1];
        for ($i = 0; $i < $groupsCount; $i++) {
            /** @var Group $group */
            $group = factory(Group::class)->make([
                'number' => $i + 1,
            ]);
            $relation->groups()->save($group);
            for ($j = 0; $j < $candidatesCount; $j++) {
                $candidate = factory(Candidate::class)->make([
                    'order' => $j,
                ]);
                if ($category->department_id == null) {
                    $candidate->department_id = \Hackersir\Department::all()->random()->id;
                } else {
                    $candidate->department_id = $category->department_id;
                }
                if ($candidatesCount > 1) {
                    $candidate->title = $titles[$j];
                }

                $group->candidates()->save($candidate);
            }
        }
    }

    /**
     * @param Activity $activity
     */
    public function addBallot(Activity $activity)
    {
        $service = App::make('Hackersir\Services\ActivityService');
        $status = $service->getStatus($activity);
        if (!ActivityStatus::isVoteBegin($status)) {
            return;
        }

        foreach ($activity->elections as $election) {
            foreach ($election->categoryElectionRelations as $relation) {
                $count = count($relation->groups);
                if ($count == 1) {
                    // 100 贊成票、100 反對票、50 廢票
                    factory(Hackersir\Ballot::class, 'VALID', 100)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                        'group_id'             => $relation->groups[0]->id,
                    ]);
                    factory(Hackersir\Ballot::class, 'VALID_OBJECTION', 100)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                        'group_id'             => $relation->groups[0]->id,
                    ]);
                    factory(Hackersir\Ballot::class, 'INVALID', 50)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                    ]);
                    // TODO: 產生 25 張錯誤票(錯誤格式有很多種啊)？
                } elseif ($count == 2) {
                    // 150 第一組票、150 第二組票、50 廢票
                    factory(Hackersir\Ballot::class, 'VALID', 100)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                        'group_id'             => $relation->groups[0]->id,
                    ]);
                    factory(Hackersir\Ballot::class, 'VALID', 100)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                        'group_id'             => $relation->groups[1]->id,
                    ]);
                    factory(Hackersir\Ballot::class, 'INVALID', 50)->create([
                        'category_election_id' => $relation->id,
                        'voting_station_id'    => $election->votingStations->random()->id,
                    ]);
                    // TODO: 產生 25 張錯誤票(錯誤格式有很多種啊)？
                }
            }
        }
    }

    /**
     * @param Activity $activity
     */
    public function setAllElectionFinish(Activity $activity)
    {
        foreach ($activity->elections as $election) {
            foreach ($election->votingStations as $votingStation) {
                $votingStation->pivot->counting_over = true;
                $votingStation->pivot->save();
            }
        }
    }
}
