<?php

use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Group;

class BigActivitiesWithBallotSeeder extends BigActivitiesSeeder
{
    protected $debugInfo = "BigActivitiesWithBallotSeeder: \n";

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
        // 建立選票
        // 如果選舉只有1個分組，那每一個候選人組會有1000 ~ 2000票
        // 如果選舉有大於1個分組，那每一個候選人組會有300 ~ 500票
        // 如果可以有反對票，則會有300 ~ 500票
        // 每個分組的廢票會有100 ~ 200票

        foreach ($this->activity->elections as $election) {
            $this->debugInfo .= "[選舉]" . $election->name . "\n";
            if ($election->categories->count() == 1) {
                $this->createBallot($election, 1000, 2000);
            } elseif ($election->categories->count() > 1) {
                $this->createBallot($election, 300, 500);
            }
        }

        // 儲存產生結果，供未來 Debug
        Log::debug($this->debugInfo);
    }

    /**
     * @param Election $election
     * @param int $minCount
     * @param int $maxCount
     */
    protected function createBallot(Election $election, $minCount, $maxCount)
    {
        foreach ($election->categoryElectionRelations as $relation) {
            $this->debugInfo .= ($relation->category->name ?: $relation->category->department->name) . ": \n";
            foreach ($relation->groups as $group) {
                $count = rand($minCount, $maxCount);
                $this->createValidBallot($relation, $group, $count);
                $this->debugInfo .= $group->candidates->first()->name . ": " . $count . "\n";

                if ($relation->can_vote_objection) {
                    $count = rand($minCount, $maxCount);
                    $this->createValidObjectionBallot($relation, $group, $count);
                    $this->debugInfo .= $group->candidates->first()->name . "反對: " . $count . "\n";
                }
            }
            $count = rand(100, 200);
            $this->createInvalidBallot($relation, $count);
            $this->debugInfo .= "廢票: " . $count . "\n\n";
        }
    }

    /**
     * @param CategoryElectionRelation $relation
     * @param Group $group
     * @param int $count
     */
    protected function createValidBallot(CategoryElectionRelation $relation, Group $group, $count)
    {
        $ballots = factory(Hackersir\Ballot::class, 'VALID', $count)->make([
            'category_election_id' => $relation->id,
            'group_id'             => $group->id,
        ]);
        $votingStations = $relation->election->votingStations;
        foreach ($ballots as $ballot) {
            $ballot->voting_station_id = $votingStations->random()->id;
        }
        DB::table('ballots')->insert($ballots->toArray());
    }

    /**
     * @param CategoryElectionRelation $relation
     * @param Group $group
     * @param int $count
     */
    protected function createValidObjectionBallot(CategoryElectionRelation $relation, Group $group, $count)
    {
        $ballots = factory(Hackersir\Ballot::class, 'VALID_OBJECTION', $count)->make([
            'category_election_id' => $relation->id,
            'voting_station_id'    => $relation->election->votingStations->random()->id,
            'group_id'             => $group->id,
        ]);
        $votingStations = $relation->election->votingStations;
        foreach ($ballots as $ballot) {
            $ballot->voting_station_id = $votingStations->random()->id;
        }
        DB::table('ballots')->insert($ballots->toArray());
    }

    /**
     * @param CategoryElectionRelation $relation
     * @param int $count
     */
    protected function createInvalidBallot(CategoryElectionRelation $relation, $count)
    {
        $ballots = factory(Hackersir\Ballot::class, 'INVALID', $count)->make([
            'category_election_id' => $relation->id,
        ]);
        $votingStations = $relation->election->votingStations;
        foreach ($ballots as $ballot) {
            $ballot->voting_station_id = $votingStations->random()->id;
        }
        DB::table('ballots')->insert($ballots->toArray());
    }
}
