<?php

use Carbon\Carbon;
use Hackersir\Activity;
use Hackersir\Candidate;
use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Group;
use Hackersir\VotingStation;
use Illuminate\Database\Seeder;

/**
 * 產生接近實際上用的測試活動資料
 *
 * 1個活動：
 *    1個選舉(學生會會長)：
 *        1個分組(學生會)：
 *           2組(各3人)
 *    1個選舉(學生議員)：
 *        19個分組：
 *           1~4組(各1人)
 *           應選組數
 *    1個選舉(系學會會長)：
 *        29個分組：
 *           1~4組(各1人)
 */
class BigActivitiesSeeder extends Seeder
{
    /** @var Activity */
    protected $activity;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::today();

        /** @var Activity $activity */
        $activity = factory(Activity::class)->make([
            'process' => '這是網站夢到的 \("▔□▔)/',
        ]);
        $activity->name = '測試用活動';
        $activity->vote_start = $now->copy()->subDay()->addHours(10);
        $activity->vote_end = $now->copy()->subDay()->addHours(18);
        $activity->billing_start = $now->copy()->subHours(1);
        $activity->publicity = true;
        $activity->save();

        $names = ['學生會會長', '學生議員', '系學會會長'];
        for ($i = 0; $i < 3; $i++) {
            $this->createElection($activity, $names[$i]);
        }

        $categoriesCount = [1, 16, 28];
        foreach ($activity->elections as $index => $election) {
            $this->createCategories($election, $categoriesCount[$index]);
        }
        foreach ($activity->elections[0]->categoryElectionRelations as $relation) {
            $this->createGroupsAndCandidate($relation, 2, 2, 3);
        }

        foreach ($activity->elections[1]->categoryElectionRelations as $relation) {
            $this->createGroupsAndCandidate($relation, 1, 4, 1);
            $relation->update([
                'elected_group_amount' => rand(1, $relation->groups->count())
            ]);
        }

        foreach ($activity->elections[2]->categoryElectionRelations as $relation) {
            $this->createGroupsAndCandidate($relation, 1, 4, 1);
        }

        $this->activity = $activity;
    }

    /**
     * @param Activity $activity
     * @param $name
     */
    public function createElection(Activity $activity, $name)
    {
        /** @var Election $election */
        $election = factory(Election::class)->make([
            'name'         => $name,
            'introduction' => '\("▔□▔)/',
        ]);
        $activity->elections()->save($election);

        // 設定投票所
        $votingStationsId = VotingStation::pluck('id')->toArray();
        $election->votingStations()->sync($votingStationsId);
    }

    /**
     * @param Election $election
     * @param int $count
     */
    public function createCategories(Election $election, $count)
    {
        if ($count == 1) {
            $election->categories()->save(Category::where('name', '=', '學生會會長')->first());
        } else {
            $categoriesId = Category::where('name', '<>', '學生會會長')->pluck('id')->random($count)->toArray();
            $election->categories()->sync($categoriesId);
        }
    }

    /**
     * @param CategoryElectionRelation $relation
     * @param int $minGroupCount
     * @param int $maxGroupCount
     * @param int $candidateCount
     */
    public function createGroupsAndCandidate(CategoryElectionRelation $relation, $minGroupCount, $maxGroupCount, $candidateCount)
    {
        $titles = ['學生會會長', '第一副會長', '第二副會長'];

        $groupCount = rand($minGroupCount, $maxGroupCount);
        for ($i = 0; $i < $groupCount; $i++) {
            $group = factory(Group::class)->make([
                'number' => $i + 1,
            ]);
            $relation->groups()->save($group);

            for ($j = 0; $j < $candidateCount; $j++) {
                $candidate = factory(Candidate::class)->make([
                    'order' => $j,
                ]);
                $category = $relation->category;
                if ($category->department_id == null) {
                    $candidate->department_id = \Hackersir\Department::all()->random()->id;
                } else {
                    $candidate->department_id = $category->department_id;
                }
                if ($candidateCount > 1) {
                    $candidate->title = $titles[$j];
                }

                $group->candidates()->save($candidate);
            }
        }
    }
}
