<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FCUDepartmentSeeder::class);
        $this->call(FCUCategorySeeder::class);
        $this->call(FCUVotingStationTableSeeder::class);
        $this->call(ActivitiesSeeder::class);
    }
}
