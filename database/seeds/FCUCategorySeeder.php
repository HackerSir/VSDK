<?php

use Hackersir\Category;
use Hackersir\Department;
use Illuminate\Database\Seeder;

/**
 * 產生逢甲自治選舉的候選人分組
 */
class FCUCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!(Category::where('name', '=', '學生會會長')->exists())) {
            $sa = new Category;
            $sa->name = '學生會會長';
            $sa->save();
        }

        $departments = Department::all();
        foreach ($departments as $department) {
            if (!(Category::where('department_id', '=', $department->id)->exists())) {
                $category = new Category;
                $category->department_id = $department->id;
                $category->save();
            }
        }
    }
}
