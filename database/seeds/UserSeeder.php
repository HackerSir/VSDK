<?php

use Hackersir\Role;
use Illuminate\Database\Seeder;

/**
 * 產生使用者
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //無角色之帳號
        factory(Hackersir\User::class)->create();
        //無角色之帳號(未驗證)
        factory(Hackersir\User::class, 'noConfirm')->create();
        //角色
        $roles = Role::all();
        //擁有單一角色
        foreach ($roles as $role) {
            $user = factory(Hackersir\User::class)->create();
            $user->attachRole($role);

            $noConfirmUser = factory(Hackersir\User::class, 'noConfirm')->create();
            $noConfirmUser->attachRole($role);
        }
    }
}
