# Contributing to VSDK
We'd love for you to contributing to this project and make it even better.  
Here are some guidelines we'd like you to follow:  

- [Code of Conduct](#conduct)
- [Got a problem?](#problem)
- [Having an issue?](#issue)
- [Translation](#translation)
- [Want a Feature?](#feature)

## <a name="conduct"></a>Code of Conduct
Help us keep this project open and inclusive.  
We will add CoC later.  

## <a name="problem"></a>Got a problem?
If you have any problem about this project, you can contact us on Facebook.  
Here is our [Group](https://www.facebook.com/groups/HackerSir/) and [Fan Page](https://www.facebook.com/HackerSir.tw/).  

## <a name="issue"></a>Having an issue?
If you find a bug or mistake in the project, you can tell us using [Issue](https://gitlab.com/HackerSir/VSDK/issues).  
Even better you can submit a [Merge Request](https://gitlab.com/HackerSir/VSDK/merge_requests) with a fix.  

## <a name="translate"></a>Translation
This project use "Traditional Chinese" as default language, and "English (US)" as fallback language.  
We might not add language into this project, sorry.  

## <a name="feature"></a>Want a Feature or a Doc Fix?
Same as [Having an issue?](#issue).
Please tell us using [Issue](https://gitlab.com/HackerSir/VSDK/issues) or submit a [Merge Request](https://gitlab.com/HackerSir/VSDK/merge_requests).

## <a name="submit"></a> Submission Guidelines

### Submitting an Issue
Before you summit your issue, make sure that you have searched the archive, maybe the question was already solved.  
If your issue appears to be a bug, and hasn't been reported, open a new issue.
If you have the screenshot about the bug, upload it with that issue.

### Submitting a Merge Request
Remember, you must rebase all your commits into single commit before you summit a merge request!
