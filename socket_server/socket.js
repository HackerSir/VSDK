var config = require('config');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis({
    host: config.redis.host,
    password: config.redis.passwd
});

server.listen(6001, config.express.ip, function () {
    console.log('Server is running!');
});

app.get('/', handler);

function handler(req, res) {
    res.send('success');
}

var onlineCount = 0;
var historyBallot = {};

io.on('connection', function (socket) {
    // console.log('使用者連入');
    socket.on('join_room', function (room) {
        socket.join(room);
        //console.log('joining room', room);
        // 送上一次的資料
        // check 邊界，只有rid才送
        if (parseInt(room) > 0 && historyBallot[room] !== undefined) {
            var message = {rid: room, data: historyBallot[room]};
            socket.emit('ballot_update', message);
            // console.log('Send History', room, message);
        }
    });
    // 離開房間
    socket.on('leave_room', function (room) {
        socket.leave(room);
        // console.log('leaving room', room);
    });

    socket.on('disconnect', function () {
        onlineCount -= 1;
        io.sockets.emit('online_count_update', onlineCount);
    });

    onlineCount += 1;
    io.sockets.emit('online_count_update', onlineCount);
});

redis.psubscribe('*', function (err, count) {
    //
});

// 這裡的 channel 是對 laravel 而言
redis.on('pmessage', function (subscribed, channel, message) {
    message = JSON.parse(message);

    if (channel.startsWith('activity_')) {
        for (var rid in message.data.data) {
            io.to(rid).emit('ballot_update', {rid: rid, data: message.data.data[rid]});
            historyBallot[rid] = message.data.data[rid];
        }
        io.to(channel).emit('updated_at', {updated_at: message.data.updated_at});
        // console.log(subscribed, channel, message.data.updated_at);
    }
    else if (channel.startsWith('status_activity_')) {
        io.to('activity_' + message.data.id).emit('activity_status', message.data.status);
        // console.log(subscribed, channel, message);
    }
    else if (channel == 'test-broadcasting') {
        io.to(channel).emit('test-broadcasting', message.data);
        // console.log(subscribed, channel, message);
    }

});
