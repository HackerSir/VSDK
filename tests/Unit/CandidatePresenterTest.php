<?php

use Hackersir\Department;
use Hackersir\Presenters\CandidatePresenter;
use Tests\TestCase;

class CandidatePresenterTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testGetDepartmentGradeClass()
    {
        $presenter = new CandidatePresenter();

        $department = new Department();
        $department->abbreviation = '資訊';

        // 故意丟 $grade = 0
        try {
            $presenter->getDepartmentGradeClass($department, 0, 0);
            $this->fail('Here must have exception.');
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'grade must be in 1 ~ 7, grade = 0.');
        }

        // 故意丟 $grade = 8
        try {
            $presenter->getDepartmentGradeClass($department, 8, 0);
            $this->fail('Here must have exception.');
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'grade must be in 1 ~ 7, grade = 8.');
        }

        // 故意丟 $class_no = -1
        try {
            $presenter->getDepartmentGradeClass($department, 2, -1);
            $this->fail('Here must have exception.');
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'class_no must be in 0 ~ 4, class_no = -1.');
        }

        // 故意丟 $class_no = 5
        try {
            $presenter->getDepartmentGradeClass($department, 2, 5);
            $this->fail('Here must have exception.');
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), 'class_no must be in 0 ~ 4, class_no = 5.');
        }

        // 測試只有一個班級的狀況 ($class_no = 0)
        $ans = '資訊二';
        $result = $presenter->getDepartmentGradeClass($department, 2, 0);
        $this->assertEquals($ans, $result);

        // 測試有多個班級的狀況 ($class_no = 2)
        $ans = '資訊二乙';
        $result = $presenter->getDepartmentGradeClass($department, 2, 2);
        $this->assertEquals($ans, $result);
    }
}
