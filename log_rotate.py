#!env /usr/bin/python3
import os
from pathlib import Path

# Keep 3 days
days_keep = 3
# Path
project_path = Path(__file__).parent
log_path = project_path.joinpath('storage').joinpath('logs')
# Log list(.log)
log_list = sorted(list(log_path.glob('*.log')))
if len(log_list) > days_keep:
  for i in range(len(log_list)-days_keep):
    os.system('xz -z ' + str(log_list[i]))
