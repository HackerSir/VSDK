# Change Log
All notable changes to this project will be documented in this file.  
This file should follow the rules in [Keep a CHANGELOG](http://keepachangelog.com/zh-TW/)

## [Unreleased]
### Added
- Initialize project with Laravel 5.2
- Add `.editorconfig` for [EditorConfig](http://editorconfig.org/) supported.
- Install packages
  - [Carbon](http://carbon.nesbot.com/), a simple PHP API extension for DateTime.
  - [Throttle](https://github.com/GrahamCampbell/Laravel-Throttle), a rate limiter for Laravel 5.
  - [Laravel Gravatar](https://github.com/thomaswelton/laravel-gravatar) for showing [Gravatar](http://zh-tw.gravatar.com/) images more easily.
  - [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar) for showing more debug information under page footer.
  - [LogViewer](https://github.com/ARCANEDEV/LogViewer), a dashboard to manage and keep track of each one of log files.
  - [IDE Helper](https://github.com/barryvdh/laravel-ide-helper) to provide accurate auto-completion in IDE.
  - [Laravel Authentication](https://laravel.tw/docs/5.2/authentication) for user authentication.
  - [Laravel Menu](https://github.com/lavary/laravel-menu), a quick way to create menus.
  - [Forms & HTML](https://laravelcollective.com/docs/5.0/html), `Form` and `Html` facades.
  - [Guzzle](https://github.com/guzzle/guzzle), PHP HTTP client.
  - [Parsedown Extra](https://github.com/AlfredoRamos/parsedown-extra-laravel), Parsedown Extra for Laravel.
  - [Bootforms](https://github.com/adamwathan/bootforms), Rapid form generation with Bootstrap 3 and Laravel.
  - [Predis](https://github.com/nrk/predis/tree/master), Flexible and feature-complete PHP client library for Redis
  - [dump_r](https://github.com/leeoniya/dump_r.php), a cleaner, leaner mix of `print_r()` and `var_dump()`
- Install CSS toolkit
  - [Font Awesome](http://fortawesome.github.io/Font-Awesome/)
- Install javascript plugin
  - [PNotify](https://sciactive.com/pnotify/) (Desktop, Buttons, Mobile, Animate Modules) for desktop notifications.
  - [X-editable](http://vitalets.github.io/x-editable/), In-place editing with Twitter Bootstrap
  - [Express](http://expressjs.com/), Fast, unopinionated, minimalist web framework for Node.js
  - [Socket.io](http://socket.io/), FEATURING THE FASTEST AND MOST RELIABLE REAL-TIME ENGINE
  - [ioredis](https://github.com/luin/ioredis), A robust, performance-focused and full-featured Redis client for Node and io.js.
- Add `zh-TW` language files from [Laravel-lang](https://github.com/caouecs/Laravel-lang)
- Finish UserController
