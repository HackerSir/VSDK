#!/usr/bin/env bash

# 切換到腳本所在路徑，防止腳本在其他路徑被執行
cd "$(dirname "$0")"

# 清除之前sudo留下的有效時間，強制重新輸入密碼
sudo -k

# 顯示執行指令
set -x

sudo php artisan down

# 更新專案
sudo git pull
sudo composer install --no-dev --no-suggest
sudo php artisan migrate
sudo yarn install
sudo yarn run prod

sudo php artisan cache:clear
sudo systemctl restart supervisord
sudo php artisan queue:restart
sudo su - http -s /bin/bash -c "PM2_HOME=/srv/http/.pm2 /usr/bin/pm2 start /srv/http/VSDK/socket_server/socket.yml"

# http 為 Arch linux
sudo chown http:http -R .

sudo php artisan up
