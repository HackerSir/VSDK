@extends('app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>廣播測試</h1>
        </div>
        <h3>Output: </h3>
        <pre id="output">----- Output -----<br/></pre>
    </div>
@endsection

@section('js')
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
        var $output;
        var jobs = [
            testRedis,
            testBroadcasting,
            testSocketIO,
            testSocketMessage
        ];
        $(function () {
            $output = $('#output');
            doNextJob();
        });

        function doNextJob() {
            if (jobs.length > 0) {
                var job = jobs[0];
                jobs.shift();
                job();
            }
        }

        function testRedis() {
            sendTest('{{ URL::route('broadcasting-test.redis') }}', '測試 Server <--> Redis: ');
        }

        function testBroadcasting() {
            sendTest('{{ URL::route('broadcasting-test.server') }}', '測試 Server <--> Broadcasting Server: ');
        }

        var socket;
        function testSocketIO() {
            $output.append('測試 Client <--> Broadcasting Server(Socket): ' + '<br/>');
            socket = io('{{env('SOCKET_SERVER_URL', 'http://localhost:6002')}}');
            // 連線成功提示
            socket.on("connect", function () {
                $output.append('socket connect success' + '<br/>');
                socket.emit('join_room', 'test-broadcasting');
                doNextJob();
            });
        }

        function testSocketMessage() {
            socket.on('test-broadcasting', function (data) {
                $output.append(data.message + '<br/>');
            });
            sendTest('{{ URL::route('broadcasting-test.fire') }}', '測試 Broadcasting: ');
        }

        function sendTest(url, title) {
            $output.append(title + '<br/>');
            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}"
                },
                type: "POST",
                dataType: "text",
                success: function (data) {
                    $output.append(data + '<br/>');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $output.append('error', xhr.status + ': ' + thrownError + '<br/>');
                },
                statusCode: {
                    502: function () {
                        $output.append('502: Bad Gateway' + '<br/>');
                    }
                },
                complete: doNextJob
            });
        }
    </script>
@endsection
