@extends('app')

@section('css')
    <style>
        h2 {
            font-size: 2.1em;
        }
        h3 {
            font-size: 1.8em;
        }
        p.text, li.text {
            font-size: 1.3em;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>{{ Config::get('site.name') }} - 網站政策</h1>
        </div>
        <ul class="nav nav-tabs">
            <li role="presentation" @if($tab == 'about') class="active" @endif>
                {!! Html::linkRoute('about', '關於我們') !!}
            </li>
            <li role="presentation" @if($tab == 'term') class="active" @endif>
                {!! Html::linkRoute('term', '服務條款') !!}
            </li>
            <li role="presentation" @if($tab == 'disclaimer') class="active" @endif>
                {!! Html::linkRoute('disclaimer', '免責聲明') !!}
            </li>
            <li role="presentation" @if($tab == 'privacy') class="active" @endif>
                {!! Html::linkRoute('privacy', '隱私權政策') !!}
            </li>
            <li role="presentation" @if($tab == 'faq') class="active" @endif>
                {!! Html::linkRoute('faq', '常見問題') !!}
            </li>
        </ul>
        <div class="panel panel-default">
            <div class="panel-body">
                @if($tab == 'about')
                    @include('misc.about')
                @elseif($tab == 'term')
                    @include('misc.term')
                @elseif($tab == 'disclaimer')
                    @include('misc.disclaimer')
                @elseif($tab == 'privacy')
                    @include('misc.privacy')
                @elseif($tab == 'faq')
                    @include('misc.faq')
                @endif
            </div>
        </div>
    </div>
@endsection
