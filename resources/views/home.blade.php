@extends('app')

@section('css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/components/button.min.css">
    <style type="text/css">
        .navbar {
            margin-bottom: 0;
        }

        .jumbotron {
            min-height: calc(100vh - 50px);
            background: #ffd87a url("{{ asset('img/background.jpg') }}") no-repeat center center;
            background-size: cover;
        }

        .jumbotron-content {
            text-align: center;
            background: rgba(100, 100, 100, .6);
            margin-top: 20vh;
            margin-bottom: 20vh;
            padding-top: 40px;
            padding-bottom: 40px;
            border-radius: 20px;
            color: white;
        }

        a.button {
            font-size: 1.2em !important;
            margin-left: 20px !important;
            margin-right: 20px !important;
            margin-bottom: 5px !important;
        }

        a.button > i.fa {
            margin-bottom: 10px !important;
        }
    </style>
@endsection

@section('content')
    <div class="jumbotron">
        <div class="jumbotron-content container text-center">
            <h1>{{ Config::get('site.name') }}</h1>
            <p>{{ Config::get('site.desc') }}</p>
            <div class="text-center">
                <a href=" @if(isset($activity)) {{ route('activity.show', $activity) }} @else javascript:void(0) @endif " class="ui icon orange inverted massive button">
                    <i class="fa fa-commenting-o fa-2x" aria-hidden="true"></i><br/>選舉政見</a>
                <a href="{{ route('activity.index') }}" class="ui icon teal inverted massive button">
                    <i class="fa fa-ticket fa-2x" aria-hidden="true"></i><br/>選舉活動</a>
                <a href=" @if(isset($activity)) {{ route('counting.show', $activity) }} @else javascript:void(0) @endif "
                   class="ui icon red inverted massive button">
                    <i class="fa fa-podcast fa-2x" aria-hidden="true"></i><br/>開票直播</a>
                <a href="{{ route('announcement.index') }}" class="ui icon olive inverted massive button">
                    <i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i><br/>網站公告</a>
            </div>
        </div>
    </div>
@endsection
