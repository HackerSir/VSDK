{{--
    $group: 候選人組
--}}
@if(!$election->activity->publicity && $election->staffs->pluck('id')->contains(auth()->id()))
    <div class="col-sm-12" style="margin-top: 10px;">
        @permission('group.edit')
        <a href="{{ route('group.edit', $group->id) }}" class="btn btn-primary" role="button">
            <i class="fa fa-pencil"></i> 編輯候選人組
        </a>
        @endpermission
        @permission('group.delete')
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['group.destroy', $group],
            'style' => 'display: inline',
            'onSubmit' => "return confirm('確定要刪除此候選人「組」嗎？');"
        ]) !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-times"></i> 刪除候選人組
        </button>
        {!! Form::close() !!}
        @endpermission
        @permission('candidate.create')
        <a href="{{ route('candidate.create', ['gid' => $group->id]) }}" class="btn btn-primary" role="button">
            <i class="fa fa-plus-square"></i> 新增候選人
        </a>
        @endpermission
    </div>
@endif
<div class="col-sm-1" id="{{ $group->number }}">
    <h3 class="text-center"><a href="#{{ $group->number }}">{{ $group->number }}號</a></h3>
</div>
<div class="col-sm-11">
    @foreach($group->candidates as $candidate)
        @include('candidate.block', $candidate)
    @endforeach
    <div style="margin-top: 10px;">
        <h3>政見：</h3>
        {!! Hackersir\Helper\MarkdownHelper::translate($group->politics) !!}
    </div>
</div>
<hr>
