@forelse($groups as $group)
    @include('group.block', compact('group'))
@empty
    <div class="col-sm-12">
        <div class="well text-danger">
            沒有候選組，按「新增候選組」
        </div>
    </div>
@endforelse
@if(!$relation->election->activity->publicity && $relation->election->staffs->pluck('id')->contains(auth()->id()))
    @permission('group.create')
    <div class="col-sm-12 text-danger">
        <a href="{{ route('group.create', ['eid' => $relation->election->id, 'cid' => $relation->category->id]) }}" class="btn btn-primary" role="button">
            <i class="fa fa-plus-square"></i> 新增候選組
        </a>
    </div>
    @endpermission
@endif
