@extends('app')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')

@php($isEditMode = isset($group))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '候選人組')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('group.update', $group))->put() !!}
                        {!! BootForm::bind($group) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('group.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}候選人組</legend>
                        {{-- 選舉名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">選舉</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $relation->election->name }}</p>
                                {{ Form::hidden('election_id', $relation->election->id) }}
                            </div>
                        </div>
                        {{-- 分組名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">分組</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $categoryPresenter->getFullName($relation->category) }}</p>
                                {{ Form::hidden('category_id', $relation->category->id) }}
                            </div>
                        </div>
                        @if($isEditMode)
                            {{-- 候選人組編號 --}}
                            <div class="form-group">
                                <label class="col-sm-5 col-lg-2 control-label">號次</label>
                                <div class="col-sm-7 col-lg-10">
                                    <p class="form-control-static">{{ $group->number }}</p>
                                </div>
                            </div>
                        @endif
                        {{-- 政見 --}}
                        @include('common.markdown', [
                            'name' => 'politics',
                            'labelName' => '政見',
                            'text' => $isEditMode ? $group->politics : null,
                        ])
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($relation->election->categories->count() == 1)
                                    <a href="{{ route('election.show', $relation->election) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('category.show-category', $relation) }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- 政見 --}}
    @include('common.markdown-js', ['name' => 'politics'])
@endsection
