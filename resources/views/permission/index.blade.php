@extends('app')

@section('title', '權限清單')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>權限清單</h1>
        </div>
        <div class="visible-xs-block">
            <p class="text-warning">表格可能會太寬，請對表格左右滑動</p>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th class="text-center">權限節點</th>
                    @foreach($roles as $role)
                        <th class="text-center">
                            {{ $role->display_name }}
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>
                            {{ $permission->display_name }}<span class="text-muted">（{{ $permission->name }}）</span><br/>
                            <small><i class="fa fa-angle-double-right"></i> {{ $permission->description }}</small>
                        </td>
                        @foreach($roles as $role)
                            <td class="text-center">
                                @if($permission->hasRole($role->name))
                                    <span class="fa fa-check text-success fa-2x"></span>
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="page-header">
            <h1>角色說明</h1>
        </div>
        <dl class="dl-horizontal">
            @foreach($roles as $role)
                <dt>{{ $role->display_name }}<br/>({{ $role->name }})</dt>
                <dd>{{ $role->description }}</dd>
            @endforeach
        </dl>
    </div>
@endsection
