<!DOCTYPE html>
<html lang="zh-Hant-TW">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    @inject('basePresenter', 'Hackersir\Presenters\BasePresenter')
    <title>{{ $pageTitle = $basePresenter->getPageTitle($__env->yieldContent('title')) }}</title>

    <!-- Metatag for Opengraph -->
    <meta property="og:url" content="{{ URL::full() }}" />
    <meta property="og:type" content="website" /> <!-- set profile in ot -->
    <meta property="og:title" content="{{ $pageTitle }}" />
    <meta property="og:description" content="{{ Config::get('site.desc') }}" />
    <meta property="og:image" content="{{ asset('img/hacker.png') }}" />
    <meta property="og:locale" content="zh_TW" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta name="description" content="{{ Config::get('site.desc') }}" />

    <link rel="icon" href="{{ asset('favicon.png') }}">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset(mix('build-css/bootswatch.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('build-css/sticky-footer.css')) }}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {!! Html::style('css/pnotify.custom.min.css') !!}
    {!! Minify::stylesheet('/css/app.css')->withFullUrl() !!}

    @yield('css')

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ Config::get('site.name') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @include(config('laravel-menu.views.bootstrap-items'), ['items' => Menu::get('navBar')->roots()])
            </ul>
        </div>
    </div>
</nav>

@yield('content')

{{-- Footer --}}
<footer class="footer">
    <div class="container">
        <div class="text-center social-list">
            <a href="https://hackersir.org" title="黑客社社網" target="_blank"><span class="fa-stack fa-lg fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-link fa-stack-1x fa-inverse"></i></span></a>
            <a href="https://www.facebook.com/HackerSir.tw" title="黑客社粉絲專頁" target="_blank"><span class="fa-stack fa-lg fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a>
            <a href="https://www.youtube.com/channel/UCeDnuTpnq_4As-ceZiWsi4A" title="黑客社Youtube" target="_blank"><span class="fa-stack fa-lg fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span></a>
            <a href="https://gitlab.com/HackerSir/VSDK" title="專案GitLab" target="_blank"><span class="fa-stack fa-lg fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-git fa-stack-1x fa-inverse"></i></span></a>
            <a href="mailto:fcuhackersir@gmail.com" title="寄信給我們" target="_blank"><span class="fa-stack fa-lg fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a>
        </div>
        <div class="text-center">
            Copyright &copy;	2016-{{ Carbon\Carbon::now()->year }} HackerSir 逢甲大學黑客社
            <ol class="footer-list">
                <li>{!! link_to_route('about', '關於我們') !!}</li>
                <li>{!! link_to_route('term', '服務條款 ') !!}</li>
                <li>{!! link_to_route('disclaimer', '免責聲明') !!}</li>
                <li>{!! link_to_route('privacy', '隱私權政策') !!}</li>
                <li>{!! link_to_route('faq', '常見問題') !!}</li>
                @if(Auth::guest())
                    <li>{!! link_to('login', '工作人員登入') !!}</li>
                @endif
            </ol>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

@if(env('Google_Analytics_Code'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('Google_Analytics_Code') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', '{{ env('Google_Analytics_Code') }}');
    </script>
@endif

@yield('js')

{{ Html::script("js/pnotify.custom.min.js") }}
{{ Html::script("js/pnotify-helper.js") }}

<script>
    @if(Session::has('global'))
        show_message('success', '{!! Session::get('global') !!}');
    @endif
    @if(Session::has('warning'))
        show_message('error', '{!! Session::get('warning') !!}');
    @endif
    @if(Session::has('info'))
        show_message('info', '{!! Session::get('info') !!}');
    @endif
</script>

</body>
</html>
