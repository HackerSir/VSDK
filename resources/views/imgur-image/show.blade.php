@extends('app')

@section('title', $imgurImage->file_name)

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('imgurImage.index', 'Imgur圖片管理') }}</li>
            <li class="active">{{ $imgurImage->file_name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('imgurImage.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回Imgur圖片管理</a>
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['imgurImage.destroy', $imgurImage],
                'style' => 'display: inline',
                'onSubmit' => "return confirm('確定要從Imgur刪除此圖片嗎？');"
            ]) !!}
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash"></i> 刪除圖片
            </button>
            {!! Form::close() !!}
        </div>
        <div class="page-header">
            <h1>{{ $imgurImage->file_name }}</h1>
        </div>
        <blockquote>
            圖片網址：<a href="{{ $imgurImage->url }}" target="_blank">{{ $imgurImage->url }}</a><br/>
            原始檔名：{{ $imgurImage->file_name }}<br/>
            上傳時間：{{ $imgurImage->created_at }}
        </blockquote>
        <div class="vertical-container" style="height: 320px">
            <img src="{{ $imgurImage->url}}" class="img-responsive" style="padding: 0 5% 0 5%; max-height: 300px; max-width: 100%; margin-top: 20px">
        </div>
    </div>
@endsection
