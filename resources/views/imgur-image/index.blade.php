@extends('app')

@section('title', 'Imgur圖片管理')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">Imgur圖片管理</li>
        </ol>
        <div class="page-header">
            <h1>Imgur圖片管理</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>圖片</th>
                <th>檔名</th>
            </tr>
            </thead>
            <tbody>
            @foreach($imgurImages as $imgurImage)
                <tr>
                    <td class="text-center" style="width: 200px">
                        <img src="{{ $imgurImage->url }}" style="max-width: 200px; max-height: 200px">
                    </td>
                    <td style="vertical-align: middle">
                        <a href="{{ route('imgurImage.show', $imgurImage) }}">{{ $imgurImage->file_name }}</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $imgurImages->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection
