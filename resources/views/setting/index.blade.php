@extends('app')

@section('title', '網站設定')

@section('css')
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap-editable/css/bootstrap-editable.css') !!}
    <style>
        {{-- Make inline editables take the full width of their parents
         http://patabugen.co.uk/2014/05/29/full-width-inline-x-editable-elements/ --}}
        .editable-container.editable-inline,
        .editable-container.editable-inline .control-group.form-group,
        .editable-container.editable-inline .control-group.form-group .editable-input,
        .editable-container.editable-inline .control-group.form-group .editable-input textarea,
        .editable-container.editable-inline .control-group.form-group .editable-input select,
        .editable-container.editable-inline .control-group.form-group .editable-input input:not([type=radio]):not([type=checkbox]):not([type=submit]) {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>網站設定</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th class="text-center col-md-4">設定項目</th>
                <th class="text-center col-md-8">資料</th>
            </tr>
            </thead>
            <tbody>
            @foreach($settings as $setting)
                <tr>
                    <td class="col-md-4">
                        {{ $setting->name }}<span class="text-muted">（{{ $setting->getTypeDesc() }}）</span><br/>
                        <small><i class="fa fa-angle-double-right"></i> {{ $setting->desc }}</small>
                    </td>
                    <td class="col-md-8">
                        @if($setting->getType() == 'boolean')
                            {{-- FIXME: 編輯時，無法自動選擇原設定值 --}}
                            <div class="editableField" data-pk="{{ $setting->id }}" data-type="select" data-url="{{ route('setting.update', $setting->id) }}" data-source="[{'value':1, 'text':'✔ True'},{'value':0, 'text':'✘ False'}]" data-value="{{ ($setting->getData()) ? 1 : 0 }}">{{ ($setting->getData()) ? '✔ True' : '✘ False' }}</div>
                        @else
                            <div class="editableField" data-pk="{{ $setting->id }}" data-type="{{ $setting->getHtmlFieldType() }}" data-url="{{ route('setting.update', $setting->id) }}">{{ $setting->data }}</div>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="page-header">
            <h1>Mail測試</h1>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Mail測試</div>
            <div class="panel-body">
                {!! Form::open(['class' => 'form-inline', 'id' => 'sendTestMail']) !!}
                <div class="form-group">
                    {!! Form::email('email', null, ['id' => 'testMailTo', 'placeholder' => 'Email', 'class' => 'form-control', 'required']) !!}
                </div>
                {!! Form::button('寄送測試信(使用Queue)', ['class' => 'btn btn-success', 'id' => 'btnSend_queue', 'style' => 'margin-bottom: 0;']) !!}
                {!! Form::button('寄送測試信', ['class' => 'btn btn-success', 'id' => 'btnSend', 'style' => 'margin-bottom: 0;']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js') !!}
    <script>
        $.fn.editable.defaults.mode = 'inline';
        $(document).ready(function () {
            $('.editableField').editable({
                ajaxOptions: {
                    headers: {
                        "X-CSRF-Token": "{{ Session::token() }}",
                        "Accept": "application/json"
                    },
                    type: 'put',
                    dataType: 'json',
                    success: function (response, newValue) {
                        if (!response.success) return response.msg;
                    },
                    error: function (response, newValue) {
                        if (response.status === 500) {
                            return 'Service unavailable. Please try later.';
                        } else {
                            return response.responseText;
                        }
                    }
                },
                emptytext: '尚未設定',
                rows: '10',
                showbuttons: 'bottom'
            });
        });
    </script>

    <script type="text/javascript">
        $btnSend_queue = $('#btnSend_queue');
        $btnSend = $('#btnSend');

        $btnSend_queue.click(function () {
            sendTestMailRequest('queue');
        });

        $btnSend.click(function () {
            sendTestMailRequest('normal');
        });

        function validateEmail(sEmail) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(sEmail)) {
                return true;
            } else {
                return false;
            }
        }

        function sendTestMailRequest(type) {
            $btnSend_queue.prop('disabled', true);
            $btnSend.prop('disabled', true);

            var URLs = "{{ URL::route('send-test-mail') }}";
            var val = $('#testMailTo').val();
            val = $.trim(val);

            //輸入檢查
            if (val.length == 0) {
                show_message('error', '請輸入信箱');
                $btnSend_queue.prop('disabled', false);
                $btnSend.prop('disabled', false);
                return;
            }
            if (!validateEmail(val)) {
                show_message('error', '信箱格式錯誤');
                $btnSend_queue.prop('disabled', false);
                $btnSend.prop('disabled', false);
                return;
            }
            $.ajax({
                url: URLs,
                data: {email: val, type: type},
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}",
                    "Accept": "application/json"
                },
                type: "POST",
                dataType: "text",

                success: function (data) {
                    if (data == "success") {
                        show_message('success', '成功寄出測試信');
                    }
                    else {
                        show_message('error', '發生未知的錯誤');
                    }

                    $btnSend_queue.prop('disabled', false);
                    $btnSend.prop('disabled', false);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    show_message('error', xhr.status + ': ' + thrownError);

                    $btnSend_queue.prop('disabled', false);
                    $btnSend.prop('disabled', false);
                }
            });
        }
    </script>
@endsection
