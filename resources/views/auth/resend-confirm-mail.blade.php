@extends('app')

@section('title', '重新發送驗證信件')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {!! Form::model($user, ['route' => ['confirm-mail.resend'], 'class' => 'form-horizontal']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">重新發送驗證信件</div>
                    {{-- Panel body --}}
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="name">Email</label>
                            <div class="col-md-9">
                                <span class="form-control" readonly>{{ $user->email }}</span>
                                <span class="label label-primary">若信箱錯誤，請重新註冊帳號</span>
                            </div>
                        </div>
                        <ul>
                            <li>發送前，請先確認信箱是否屬於自己</li>
                            <li>發送信件可能耗費幾分鐘，請耐心等待</li>
                            <li>僅最後一次發送之信件有效</li>
                        </ul>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-envelope"></i> 發送驗證信件
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
