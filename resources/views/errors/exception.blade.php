<!DOCTYPE html>
<html>
<head>
    <title>Oops!</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Oops!</div>
        <h1>{{ "看起來你遇到網站的Bug了" }}</h1>
        <h1>{{ "你可以回報給我們，讓我們改進它" }}</h1>
        {{ link_to_route('home', '返回首頁') }}<br />
        <a href="javascript:history.back();">返回上一頁</a>
    </div>
</div>
</body>
</html>
