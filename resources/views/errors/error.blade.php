<!DOCTYPE html>
<html>
    <head>
        <title>{{ $code }} {{ $message }} - {{ Config::get('site.name') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }

            a {
                color: #337ab7;
                text-decoration: none;
            }

            a:hover, a:focus {
                color: #23527c;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{{ $code }}</div>
                <h1>{{ $message }}</h1>
                {{ link_to_route('home', '返回首頁') }}<br />
                <a href="javascript:history.back();">返回上一頁</a>
            </div>
        </div>
    </body>
</html>
