@extends('app')

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@section('title', '活動公告')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            @if(isset($activity))
                <li>{{ link_to_route('notification.index', '活動公告') }}</li>
                <li class="active">{{ $activity->name }}</li>
            @else
                <li class="active">活動公告</li>
            @endif
        </ol>
        @if(isset($activity))
            <a href="{{ route('activity.show', $activity) }}" class="btn btn-default">
                <i class="fa fa-arrow-left"></i> 返回活動
            </a>
        @endif
        @permission('notification.manage')
        @if(isset($activity))
            <a href="{{ route('notification.create', ['activity' => $activity->id]) }}" class="btn btn-primary">
                <i class="fa fa-plus-square"></i> 新增活動公告
            </a>
        @else
            <a href="{{ route('notification.create') }}" class="btn btn-primary">
                <i class="fa fa-plus-square"></i> 新增活動公告
            </a>
        @endif
        {!! BootForm::open([])->get()->action(route('notification.store'))->attribute('style', 'display:inline-block')->id('activity_filter') !!}
        {!! BootForm::select('活動','activity')->hideLabel()->options($activityPresenter->getSelectList())->select(Request::get('activity')) !!}
        {!! BootForm::close() !!}
        @if(isset($activity))
            <a href="{{ route('notification.index') }}" class="btn btn-default">
                <i class="fa fa-times"></i>
            </a>
        @endif

        @endpermission
        <div class="page-header">
            <h1>活動公告</h1>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-center col-md-1">類型</th>
                    <th class="text-center">內文</th>
                    <th class="text-center">發布者</th>
                    <th class="text-center">所屬活動</th>
                </tr>
                </thead>
                <tbody>
                @forelse($notifications as $notification)
                    <tr>
                        <td class="{{ $notification->type }} text-center">
                            <i class="fa fa-2x {{ $notification->fa_class }}" aria-hidden="true"></i>
                        </td>
                        <td>{{ link_to_route('notification.show', $notification->name, $notification) }}</td>
                        <td>
                            @if($notification->user)
                                {{ $notification->user->name }}
                            @endif
                        </td>
                        <td>{{ link_to_route('activity.show', $notification->activity->name, $notification->activity) }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">暫無公告</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="text-center">
            {!! $notifications->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#activity').change(function () {
            if ($(this).val()) {
                $('#activity_filter').submit();
            }
        });
    </script>
@endsection
