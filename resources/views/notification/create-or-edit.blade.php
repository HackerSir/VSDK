@extends('app')

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@php($isEditMode = isset($notification))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '活動公告')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('notification.index', '活動公告') }}</li>
            @if($isEditMode)
                <li>{{ link_to_route('notification.show', $notification->name, $notification) }}</li>
            @endif
            <li class="active">{{ $methodText }}</li>
        </ol>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('notification.update', $notification))->put() !!}
                        {!! BootForm::bind($notification) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('notification.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}活動公告</legend>

                        {{-- 公告內容 --}}
                        @if(!$isEditMode && isset($activity))
                            {!! BootForm::select('所屬活動', 'activity_id')->options($activityPresenter->getSelectList())->select($activity->id) !!}
                        @else
                            {!! BootForm::select('所屬活動', 'activity_id')->options($activityPresenter->getSelectList())->required() !!}
                        @endif
                        {{-- 公告類型 --}}
                        {!! BootForm::select('類型', 'type')->options($activityPresenter->getTypeSelectList())->required() !!}
                        {{-- 公告內容 --}}
                        {!! BootForm::textarea('內容', 'text') !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('notification.show', $notification) }}"
                                       class="btn btn-default">返回</a>
                                @elseif(isset($activity))
                                    <a href="{{ route('notification.index', ['activity' => $activity->id]) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('notification.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
