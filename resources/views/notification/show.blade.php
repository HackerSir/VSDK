@extends('app')

@section('title', $notification->name . ' - 活動公告')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('notification.index', '活動公告') }}</li>
            <li class="active">{{ $notification->name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('notification.index') }}" class="btn btn-default" role="button">
                <i class="fa fa-arrow-left"></i> 返回活動公告清單
            </a>
            @permission('notification.manage')
            <a href="{{ route('notification.edit', $notification) }}" class="btn btn-primary" role="button">
                <i class="fa fa-cogs"></i> 編輯活動公告
            </a>
            {!! Form::open(['route' => ['notification.destroy', $notification], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此活動公告嗎？');"]) !!}
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash"></i> 刪除活動公告
            </button>
            {!! Form::close() !!}
            @endpermission
        </div>
        <div class="page-header">
            <h1>{{ $notification->name }}</h1>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td class="text-center col-xs-4 col-md-2">建立時間</td>
                    <td class="col-xs-8 col-md-10">{{$notification->created_at }}</td>
                </tr>
                <tr>
                    <td class="text-center col-xs-4 col-md-2">最後修改時間</td>
                    <td class="col-xs-8 col-md-10">{{ $notification->updated_at }}</td>
                </tr>
                <tr>
                    <td class="text-center col-xs-4 col-md-2">發布者</td>
                    <td class="col-xs-8 col-md-10">
                        @if($notification->user)
                            {{ $notification->user->name }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-center col-xs-4 col-md-2">所屬活動</td>
                    <td class="col-xs-8 col-md-10">
                        {{ link_to_route('activity.show', $notification->activity->name, $notification->activity) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="alert alert-{{ $notification->type }}" role="alert">
                            <i class="fa {{ $notification->fa_class }}" aria-hidden="true"></i> {{ $notification->text }}
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
