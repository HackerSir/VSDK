@extends('app')

@section('title', $election->name)

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $election->activity->name, $election->activity) }}</li>
            <li>{{ link_to_route('election.show', $election->name, $election) }}</li>
            <li class="active">編輯投票所</li>
        </ol>
        <a href="{{ route('election.show', $election) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉</a>
        <div class="page-header">
            <h1>投票所</h1>
        </div>
        <blockquote>
            @if(count($election->votingStations))
                投票所＆開票員：<br/>
                @foreach($election->electionVotingStationRelations as $relation)
                    <ul class="list-inline">
                        <li>{{ $relation->votingStation->name }}</li>
                        <li><a href="{{ route('election.edit-counting-staff', $relation) }}" title="編輯開票員"><i class="fa fa-users" aria-hidden="true"></i></a></li>
                        @foreach($relation->users as $staff)
                            @if(Entrust::can('user.view'))
                                <li>{!! link_to_route('user.show', $staff->name, $staff, ['target' => '_blank']) !!}</li>
                            @else
                                {{ $staff->name }}
                            @endif
                        @endforeach
                    </ul>
                @endforeach
                {{--<ul class="list-inline">--}}
                    {{--@foreach($relation->users as $staff)--}}
                        {{-- TODO: 顯示使用者小卡片的Presenter或view（決定樣式，並根據有無權限決定有無超連結） --}}
                        {{--@if(Entrust::can('user.view'))--}}
                            {{--<li>{!! link_to_route('user.show', $staff->name, $staff, ['target' => '_blank']) !!}</li>--}}
                        {{--@else--}}
                            {{--{{ $staff->name }}--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            @else
                暫無投票所
            @endif
        </blockquote>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 well">
                    請透過此選舉對應之投票所：
                    {!! Form::open(['route' => ['election.edit-voting-station', $election], 'class' => 'form-horizontal']) !!}
                    @foreach($votingStations as $votingStation)
                        <label>
                            {!! Form::checkbox('station[]', $votingStation->id, ($election->votingStations->contains($votingStation)))  !!} {{ $votingStation->name }}
                        </label>
                        <br/>
                    @endforeach
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> 確認
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
