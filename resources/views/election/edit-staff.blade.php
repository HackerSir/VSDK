@extends('app')

@section('title', $election->name)

@section('css')
    @include('common.select2.css')
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $election->activity->name, $election->activity) }}</li>
            <li>{{ link_to_route('election.show', $election->name, $election) }}</li>
            <li class="active">編輯選務人員</li>
        </ol>        <a href="{{ route('election.show', $election) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉</a>
        <div class="page-header">
            <h1>選務人員</h1>
        </div>
        <div class="container">
            <div class="row">
                <blockquote>
                    @if(count($election->staffs))
                        選務人員：
                        <ul class="list-inline">
                            @foreach($election->staffs as $staff)
                                {{-- TODO: 顯示使用者小卡片的Presenter或view（決定樣式，並根據有無權限決定有無超連結） --}}
                                @if(Entrust::can('user.view'))
                                    <li>{!! link_to_route('user.show', $staff->name, $staff, ['target' => '_blank']) !!}</li>
                                @else
                                    {{ $staff->name }}
                                @endif
                            @endforeach
                        </ul>
                    @else
                        暫無選務人員
                    @endif
                </blockquote>
                <div class="col-md-8 col-md-offset-2 well">
                    請透過以下欄位進行設定，可選擇之使用者條件如下：
                    <ul>
                        <li>必須完成<b>信箱驗證</b></li>
                        <li>必須擁有<b>選務人員</b>之角色</li>
                    </ul>
                    {!! Form::open(['route' => ['election.edit-staff', $election], 'class' => 'form-horizontal']) !!}
                    <div class="row">
                        @include('common.user-picker.form-input',[
                            'name' => 'staff',
                            'userCollection' => $election->staffs
                        ])
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> 確認
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('common.select2.js')
    @include('common.user-picker.js', ['name' => 'staff', 'role' => 'staff'])
@endsection
