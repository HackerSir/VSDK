@extends('app')

@php($isEditMode = isset($election))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '選舉')

@section('css')
    @include('common.fileinput.css')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('election.update', $election))->put() !!}
                        {!! BootForm::bind($election) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('election.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}選舉</legend>
                        {{-- 活動名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">活動</label>
                            <div class="col-sm-7 col-lg-10">
                                @if($isEditMode)
                                    <p class="form-control-static">{{ $election->activity->name }}</p>
                                @else
                                    <p class="form-control-static">{{ $activity->name }}</p>
                                    {{ Form::hidden('activity_id', $activity->id) }}
                                @endif
                            </div>
                        </div>
                        {{-- 選舉名稱 --}}
                        @if($isEditMode && $election->activity->publicity)
                            {!! BootForm::text('選舉名稱', 'name')->helpBlock('活動公開後不可修改')->attribute('readonly', '') !!}
                        @else
                            {!! BootForm::text('選舉名稱', 'name')->helpBlock('活動公開後不可修改') !!}
                        @endif
                        {{-- 圖片 --}}
                        {!! BootForm::text('圖片網址', 'image')->placeholder('填入Imgur圖片網址，或使用下方上傳功能') !!}
                        {{-- FIXME: 右上角清除按鍵看不見 --}}
                        {!! BootForm::file('圖片上傳', 'image_upload') !!}
                        {{-- 選舉說明文字 --}}
                        @if($isEditMode && $election->activity->publicity)
                            {!! BootForm::textarea('選舉說明文字', 'introduction')->helpBlock('活動公開後不可修改')->attribute('readonly', '')->attribute('style', 'resize: vertical') !!}
                        @else
                            @include('common.markdown', [
                                'name' => 'introduction',
                                'labelName' => '選舉說明文字',
                                'text' => $isEditMode ? $election->introduction : null,
                                'helpText' => '活動公開後不可修改',
                            ])
                        @endif
                        {{-- TODO:選舉公報連結(投票開始後不可變更，如果活動有設定選舉公報連，此設定會被忽略) --}}

                        {{-- TODO:選舉結果連結(開票結束才能填寫，如果活動有設定選舉結果連，此設定會被忽略) --}}

                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('election.show', $election) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('activity.show', $activity) }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @if($isEditMode && $election->image)
        @include('common.fileinput.js', ['name' => 'image', 'image' => $election->image])
    @else
        @include('common.fileinput.js', ['name' => 'image'])
    @endif
    {{-- 選舉說明文字 --}}
    @unless($isEditMode && $election->activity->publicity)
        @include('common.markdown-js', ['name' => 'introduction'])
    @endunless
@endsection
