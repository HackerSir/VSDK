@extends('app')

@section('title', $relation->election->name)

@section('css')
    @include('common.select2.css')
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $relation->election->activity->name, $relation->election->activity) }}</li>
            <li>{{ link_to_route('election.show', $relation->election->name, $relation->election) }}</li>
            <li>{{ link_to_route('votingStation.show', $relation->votingStation->name, $relation->votingStation) }}</li>
            <li class="active">編輯開票員</li>
        </ol>
        <a href="{{ route('election.edit-voting-station', $relation->election) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回投票所編輯</a>
        <div class="page-header">
            <h1>開票員</h1>
        </div>
        <div class="container">
            <div class="row">
                <blockquote>
                    選舉：{{ link_to_route('election.show', $relation->election->name, $relation->election) }}
                    <br/>
                    開票所：{{ link_to_route('votingStation.show', $relation->votingStation->name, $relation->votingStation) }}
                </blockquote>
                <blockquote>
                    @if(count($relation->users))
                        開票員：
                        <ul class="list-inline">
                            @foreach($relation->users as $staff)
                                {{-- TODO: 顯示使用者小卡片的Presenter或view（決定樣式，並根據有無權限決定有無超連結） --}}
                                @if(Entrust::can('user.view'))
                                    <li>{!! link_to_route('user.show', $staff->name, $staff, ['target' => '_blank']) !!}</li>
                                @else
                                    {{ $staff->name }}
                                @endif
                            @endforeach
                        </ul>
                    @else
                        暫無開票員
                    @endif
                </blockquote>
                <div class="col-md-8 col-md-offset-2 well">
                    請透過以下欄位進行設定，可選擇之使用者條件如下：
                    <ul>
                        <li>必須完成<b>信箱驗證</b></li>
                        <li>必須擁有<b>開票員</b>之角色</li>
                    </ul>
                    {!! Form::open(['route' => ['election.edit-counting-staff', $relation], 'class' => 'form-horizontal']) !!}
                    <div class="row">
                        @include('common.user-picker.form-input',[
                            'name' => 'counting_staff',
                            'userCollection' => $relation->users
                        ])
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> 確認
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('common.select2.js')
    @include('common.user-picker.js', ['name' => 'counting_staff', 'role' => 'counting_staff'])
@endsection
