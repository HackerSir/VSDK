{{-- 用來顯示選舉的方塊 --}}
{{-- $election: 選舉 model--}}
{{-- $imgurImagePresenter --}}
<div class="col-sm-4" style="margin-top: 20px">
    <a href="{{ route('election.show', $election) }}">
        <div style="height: 300px; position: relative;">
            <img src="{{ $imgurImagePresenter->getSafeURL($election->image) }}"
                 class="img-responsive" style="max-height: 300px; left: 50%; position: absolute; top: 50%; transform: translate(-50%, -50%);"
            >
        </div>
        <div>
            <h3>{{ $election->name }}</h3>
        </div>
    </a>
</div>
