@extends('app')

@section('title', $election->name)

@inject('electionPresenter', 'Hackersir\Presenters\ElectionPresenter')
@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')
@inject('candidatePresenter', 'Hackersir\Presenters\CandidatePresenter')
@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')
@inject('basePresenter', 'Hackersir\Presenters\BasePresenter')

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $election->activity->name, $election->activity) }}</li>
            <li class="active">{{ $election->name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('activity.show', $election->activity) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回活動</a>
            @permission('election.edit')
            <a href="{{ route('election.edit', $election) }}" class="btn btn-primary"><i class="fa fa-cogs"></i> 編輯選舉</a>
            @endpermission
            @permission('election.edit-staff')
            <a href="{{ route('election.edit-staff', $election) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯選務人員</a>
            @endpermission
            @permission('election.edit-voting-station')
            <a href="{{ route('election.edit-voting-station', $election) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯投票所</a>
            @endpermission
            @permission('category.relation.create')
            @if(!$election->activity->publicity)
                <a href="{{ route('category-relation.create', ['eid' => $election->id]) }}" class="btn btn-primary" role="button">
                    <i class="fa fa-plus-square"></i> 新增分組
                </a>
            @else
                <a href="javascript:void(0)" class="btn btn-primary" role="button" disabled="disabled">
                    <i class="fa fa-plus-square"></i> 新增分組
                </a>
            @endif
            @endpermission
            @if($categoriesCount == 1)
                @permission('category.relation.delete')
                @if(!$election->activity->publicity)
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['category-relation.delete', $categories[0], 'eid' => $election->id],
                        'style' => 'display: inline',
                        'onSubmit' => "return confirm('確定要刪除此分組嗎？');"
                    ]) !!}
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i> 刪除分組
                    </button>
                    {!! Form::close() !!}
                @else
                    <a href="javascript:void(0)" class="btn btn-danger" role="button" disabled="disabled">
                        <i class="fa fa-trash"></i> 刪除分組
                    </a>
                @endif
                @endpermission
            @endif
            @permission('tree.access')
            <a href="{{ route('tree.index', ['election' => $election->id]) }}" class="btn btn-success"><i class="fa fa-tree" aria-hidden="true"></i></a>
            @endpermission
            {{-- 社群分享按鈕 --}}
            @include('common.share-button-bar', ['title' => $basePresenter->getPageTitle($__env->yieldContent('title')), 'url' => URL::current()])
        </div>
        <div class="page-header">
            <h1>{{ $election->name }}</h1>
        </div>
        {{-- FIXME: 如果有選舉公報連結、結果報告的話，可能會顯示不出來 --}}
        @if($election->introduction)
            <blockquote>
                {!! Hackersir\Helper\MarkdownHelper::translate($election->introduction) !!}
                {{-- TODO: 選舉公報連結、結果報告(如果活動有的話，顯示活動的) --}}
            </blockquote>
        @endif
        @if(count($election->votingStations))
            <blockquote>
                投票所：
                <ul>
                    @foreach($election->votingStations as $votingStation)
                        <li>{{ $votingStation->name }}</li>
                    @endforeach
                </ul>
            </blockquote>
        @else
            <blockquote>
                <p class="text-danger">
                    沒有投票所可以投票？對，我也不知道為什麼。
                    @permission('election.edit-voting-station')
                    <br/>請使用「編輯投票所」進行編輯
                    @endpermission
                </p>
            </blockquote>
        @endif
        <div class="row">
            @if ($categoriesCount == 0)
                <div class="col-sm-12">
                    <div class="well text-danger">
                        目前沒有分組
                        @permission('category.relation.create')
                        ，請使用「新增分組」進行編輯
                        @endpermission
                    </div>
                </div>
            @elseif ($categoriesCount == 1)
                @include('group.collection', ['relation' => $relations[0]])
            @else
                @foreach($relations as $relation)
                    @include('category.block', compact('categoryPresenter', 'relation'))
                @endforeach
            @endif
        </div>
        @if(Entrust::can('election.edit') || Entrust::can('election.edit-staff'))
            <div class="panel panel-danger" style="margin-top: 20px;">
                <div class="panel-heading">
                    <h3 class="panel-title">選務人員 <i class="fa fa-eye-slash" aria-hidden="true" title="僅管理員可見"></i></h3>
                </div>
                <div class="panel-body">
                    @if(count($election->staffs))
                        <ul class="list-inline">
                            @foreach($election->staffs as $staff)
                                <li>{!! link_to_route('user.show', $staff->name, $staff, ['target' => '_blank']) !!}</li>
                            @endforeach
                        </ul>
                    @else
                        暫無選務人員
                    @endif
                </div>
            </div>
        @endif
    </div>
@endsection
