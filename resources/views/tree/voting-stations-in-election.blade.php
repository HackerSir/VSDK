{{--
    $election: 選舉
--}}
<ul>
    @foreach($election->electionVotingStationRelations as $relation)
        <li>
            <span class="label label-default">投票所</span>
            {{ $relation->votingStation->name }}
            @if(count($relation->users))
                <span class="label label-info">開票員</span>
                @include('tree.user-list', ['users' => $relation->users])
            @endif
        </li>
    @endforeach
</ul>
