{{--
    $election: 選舉
--}}
<ul>
    @foreach($election->categoryElectionRelations as $relation)
        <li>
            <span class="label label-primary">分組</span>
            {{ $relation->category->name }}
            @if($relation->category->department)
                <span class="label label-info">科系</span>
                [{{ $relation->category->department->abbreviation }}]
                {{ $relation->category->department->name }}
            @endif
        </li>
        @include('tree.groups-in-category-election-relation', ['categoryElectionRelations' => $relation])
    @endforeach
</ul>
