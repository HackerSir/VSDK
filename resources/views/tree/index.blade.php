@extends('app')

@section('title', '資源樹狀圖')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            @if(isset($activity))
                <li>{{ link_to_route('tree.index', '資源樹狀圖') }}</li>
                <li class="active">{{ $activity->name }}</li>
            @elseif(isset($election))
                <li>{{ link_to_route('tree.index', '資源樹狀圖') }}</li>
                <li>{{ link_to_route('tree.index', $election->activity->name, ['activity' => $election->activity->id]) }}</li>
                <li class="active">{{ $election->name }}</li>
            @else
                <li class="active">資源樹狀圖</li>
            @endif
        </ol>
        <div class="page-header">
            <h1>資源樹狀圖</h1>
        </div>
        @if(isset($activity))
            @include('tree.elections-in-activity', ['activity' => $activity])
        @elseif(isset($election))
            @include('tree.categories-in-election', ['election' => $election])
            @include('tree.voting-stations-in-election', ['election' => $election])
        @else
            @include('tree.activities', ['activities' => $activities])
        @endif
    </div>
@endsection
