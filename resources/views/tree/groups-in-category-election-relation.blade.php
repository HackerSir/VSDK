{{--
    $categoryElectionRelations: 分組 與 選舉 的關聯
--}}
<ul>
    @foreach($categoryElectionRelations->groups as $group)
        <li>
            <span class="label label-primary">候選組</span>
            {{ $group->number }}
        </li>
        @include('tree.candidates-in-group', ['group' => $group])
    @endforeach
</ul>
