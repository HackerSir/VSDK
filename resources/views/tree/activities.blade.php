{{--
    $activities: 活動清單
--}}
<ul>
    @foreach($activities as $activity)
        <li>
            <span class="label label-primary">活動</span>
            {{ link_to_route('activity.show', $activity->name, $activity, ['target' => '_blank']) }}
            <a href="{{ route('tree.index', ['activity' => $activity->id]) }}"><i class="fa fa-tree" aria-hidden="true"></i></a>
        </li>
        @include('tree.elections-in-activity', ['activity' => $activity])
    @endforeach
</ul>
