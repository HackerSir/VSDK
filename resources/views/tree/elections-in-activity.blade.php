{{--
    $activity: 活動
--}}
<ul>
    @foreach($activity->elections as $election)
        <li>
            <span class="label label-primary">選舉</span>
            {{ link_to_route('election.show', $election->name, $election, ['target' => '_blank']) }}
            <a href="{{ route('tree.index', ['election' => $election->id]) }}"><i class="fa fa-tree" aria-hidden="true"></i></a>
            @if(count($election->staffs))
                <span class="label label-info">選務人員</span>
                @include('tree.user-list', ['users' => $election->staffs])
            @endif
        </li>
        @include('tree.categories-in-election', ['election' => $election])
        @include('tree.voting-stations-in-election', ['election' => $election])
    @endforeach
</ul>
