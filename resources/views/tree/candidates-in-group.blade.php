{{--
    $group: 候選組
--}}
<ul>
    @foreach($group->candidates as $candidate)
        <li>
            <span class="label label-primary">候選人</span>
            {{ $candidate->name }}
        </li>
    @endforeach
</ul>
