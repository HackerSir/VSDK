@extends('app')

@section('title', '個人資料')

@section('css')
    <style>
        #gravatar {
            border: 3px solid white;
        }
        #gravatar:hover {
            border: 3px dotted black;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">個人資料</div>
                    {{-- Panel body --}}
                    <div class="panel-body">
                        <div class="row">
                            <div class="text-center">
                                {{-- Gravatar大頭貼 --}}
                                <a href="http://zh-tw.gravatar.com/" target="_blank" title="透過Gravatar更換照片">
                                    <img src="{{ Gravatar::src($user->email, 200) }}" class="img-circle" id="gravatar" /></a><br />
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="text-center col-md-10 col-md-offset-1">
                                <table class="table table-hover">
                                    <tr>
                                        <td>名稱：</td>
                                        <td>{{ $user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email：</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>角色：</td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                {{ $role->display_name }}<br />
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="{{ route('profile.edit') }}" class="btn btn-primary" title=""><i class="fa fa-pencil"></i> 編輯資料</a>
                    <a href="{{ route('profile.change-password') }}" class="btn btn-primary" title=""><i class="fa fa-unlock-alt"></i> 修改密碼</a>
                </div>
            </div>
        </div>
    </div>
@endsection
