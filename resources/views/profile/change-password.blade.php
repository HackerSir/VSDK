@extends('app')

@section('title', '修改密碼')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {!! Form::model($user, ['route' => ['profile.update-password'], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">修改密碼</div>
                    {{-- Panel body --}}
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group has-feedback{{ ($errors->has('password'))?' has-error':'' }}">
                                <label class="control-label col-md-3" for="password">原密碼</label>
                                <div class="col-md-8">
                                    {!! Form::password('password', ['placeholder' => '請輸入原密碼', 'class' => 'form-control', 'required']) !!}
                                    @if($errors->has('password'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('password') }}</span><br />@endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group has-feedback{{ ($errors->has('new_password'))?' has-error':'' }}">
                                <label class="control-label col-md-3" for="new_password">新密碼</label>
                                <div class="col-md-8">
                                    {!! Form::password('new_password', ['placeholder' => '請輸入新密碼', 'class' => 'form-control', 'required']) !!}
                                    @if($errors->has('new_password'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('new_password') }}</span><br />@endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group has-feedback{{ ($errors->has('new_password_confirmation'))?' has-error':'' }}">
                                <label class="control-label col-md-3" for="new_password_confirmation">確認新密碼</label>
                                <div class="col-md-8">
                                    {!! Form::password('new_password_confirmation', ['placeholder' => '請再次輸入新密碼', 'class' => 'form-control', 'required']) !!}
                                    @if($errors->has('new_password_confirmation'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('new_password_confirmation') }}</span><br />@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="{{ route('profile') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> 返回個人資料</a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i> 確認
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
