@extends('app')

@section('title', '選舉活動')

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">選舉活動</li>
        </ol>
        @permission('activity.create')
        <a href="{{ route('activity.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增活動</a>
        @endpermission
        <div class="page-header">
            <h1>選舉活動</h1>
        </div>
        <div class="list-group">
            {{-- 照投票開始時間排序，最新在前面 --}}
            @foreach($activities as $index => $activity)
                {!! $activityPresenter->openItemGroup($activity) !!}
                <span class="badge">{{ trans('activity_status'.'.'.$status[$index]) }}</span>
                <h3 class="list-group-item-heading">{{ $activity->name }}</h3>
                <p class="list-group-item-text">
                    <span style="display: inline-block">投票日：{{ $activityPresenter->getVoteDay($activity) }}</span><br/>
                    <span style="display: inline-block">開票時間：{{ $activityPresenter->getBillingTime($activity) }}</span>
                </p>
                {!! $activityPresenter->closeItemGroup() !!}
            @endforeach
        </div>
        <div class="text-center">
            {!! $activities->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection
