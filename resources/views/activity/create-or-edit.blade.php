@extends('app')

@php($isEditMode = isset($activity))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '活動')

@section('css')
    {!! Html::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('activity.update', $activity))->put() !!}
                        {!! BootForm::bind($activity) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('activity.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}活動</legend>

                        {{-- 活動名稱(活動建立後不可修改) --}}
                        @if($isEditMode)
                            {!! BootForm::text('活動名稱', 'name')->helpBlock('活動建立後不可修改')->attribute('readonly', '') !!}
                        @else
                            {!! BootForm::text('活動名稱', 'name')->helpBlock('活動建立後不可修改') !!}
                        @endif

                        {{-- 投票開始時間(活動建立後不可修改) --}}
                        <div class="form-group has-feedback{{ ($errors->has('vote_start'))?' has-error':'' }}">
                            {!! Form::label('vote_start', '投票開始時間', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                <div class='input-group date datetimepicker'>
                                    {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                    @if($isEditMode)
                                        {!! Form::text('vote_start', $activity->vote_start, ['id' => 'vote_start', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control', 'readonly' => '']) !!}
                                    @else
                                        {!! Form::text('vote_start', null, ['id' => 'vote_start', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control']) !!}
                                    @endif
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                @if($errors->has('vote_start'))
                                    <span class="glyphicon glyphicon-remove form-control-feedback"
                                          aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('vote_start') }}</span>
                                @endif
                                <p class="help-block">活動建立後不可修改</p>
                            </div>
                        </div>

                        {{-- 投票結束時間(活動建立後不可修改) --}}
                        <div class="form-group has-feedback{{ ($errors->has('vote_end'))?' has-error':'' }}">
                            {!! Form::label('vote_end', '投票結束時間', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                <div class='input-group date datetimepicker'>
                                    {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                    @if($isEditMode)
                                        {!! Form::text('vote_end', $activity->vote_end, ['id' => 'vote_end', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control', 'readonly' => '']) !!}
                                    @else
                                        {!! Form::text('vote_end', null, ['id' => 'vote_end', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control']) !!}
                                    @endif
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                @if($errors->has('vote_end'))<span
                                    class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first('vote_end') }}</span>@endif
                                <p class="help-block">活動建立後不可修改</p>
                            </div>
                        </div>

                        {{-- 開票開始時間(活動建立後不可修改) --}}
                        <div class="form-group has-feedback{{ ($errors->has('billing_start'))?' has-error':'' }}">
                            {!! Form::label('billing_start', '開票開始時間', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                <div class='input-group date datetimepicker'>
                                    {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                    @if($isEditMode)
                                        {!! Form::text('billing_start', $activity->billing_start, ['id' => 'billing_start', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control', 'readonly' => '']) !!}
                                    @else
                                        {!! Form::text('billing_start', null, ['id' => 'billing_start', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control']) !!}
                                    @endif
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                @if($errors->has('billing_start'))<span
                                    class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first('billing_start') }}</span>@endif
                                <p class="help-block">活動建立後不可修改</p>
                            </div>
                        </div>

                        {{-- 選舉流程說明(Markdown)(非必填)(投票開始後不可變更) --}}
                        @if($isEditMode && \Hackersir\Utils\ActivityStatus::isVoteBegin($status))
                            {!! BootForm::textarea('選舉流程說明', 'process')->helpBlock('投票開始後不可變更')->attribute('readonly', '')->attribute('style', 'resize: vertical') !!}
                        @else
                            @include('common.markdown', [
                                'name' => 'process',
                                'labelName' => '選舉流程說明',
                                'text' => $isEditMode ? $activity->process : null,
                                'helpText' => '投票開始後不可變更'
                            ])
                        @endif

                        {{-- 開票說明 --}}
                        @include('common.markdown', [
                            'name' => 'count_help_text',
                            'labelName' => '開票說明',
                            'text' => $isEditMode ? $activity->count_help_text : null,
                        ])

                        {{-- 選舉公報連結(URL)(非必填)(投票開始後不可變更) --}}
                        @if ($isEditMode && \Hackersir\Utils\ActivityStatus::isVoteBegin($status))
                            {!! BootForm::text('選舉公報連結(URL)', 'reporting_link')->helpBlock('投票開始後不可變更')->attribute('readonly', '') !!}
                        @else
                            {!! BootForm::text('選舉公報連結(URL)', 'reporting_link')->helpBlock('投票開始後不可變更') !!}
                        @endif

                        {{-- 選舉結果連結(URL)(開票結束才能填寫) --}}
                        @if ($isEditMode && \Hackersir\Utils\ActivityStatus::isFinish($status))
                            {!! BootForm::text('選舉結果連結(URL)', 'results_report_link')->helpBlock('開票完才能填寫') !!}
                        @else
                            {!! BootForm::text('選舉結果連結(URL)', 'results_report_link')->helpBlock('開票完才能填寫')->attribute('readonly', '') !!}
                        @endif

                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('activity.show', $activity) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('activity.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js') !!}
    {!! Html::script('js/moment_zh-tw.js') !!}
    {!! Html::script('js/bootstrap-datetimepicker.min.js') !!}
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').each(function () {
                $(this).datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                });
            });
        });
    </script>
    @unless($isEditMode && \Hackersir\Utils\ActivityStatus::isVoteBegin($status))
        @include('common.markdown-js', ['name' => 'process'])
    @endunless
    @include('common.markdown-js', ['name' => 'count_help_text'])
@endsection
