@extends('app')

@section('title', $activity->name)

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')
@inject('basePresenter', 'Hackersir\Presenters\BasePresenter')
@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li class="active">{{ $activity->name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('activity.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉活動</a>
            <div class="btn-group" role="group">
                <a href="{{ route('counting.show', $activity) }}" class="btn btn-danger" role="button"><i class="fa fa-video-camera" aria-hidden="true"></i> 開票直播</a>
                @permission('activity.video.manage')
                    <a href="{{ route('activity.video.show', $activity) }}" class="btn btn-danger" role="button" title="直播影片清單">
                        <i class="fa fa-gear" aria-hidden="true"></i>
                    </a>
                @endpermission
            </div>
            @permission('activity.edit')
            <a href="{{ route('activity.edit', $activity) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯活動</a>
            @if (!$activity->publicity)
                {!!  Form::open(['route' => ['activity.publish', $activity], 'style' => 'display: inline', 'onSubmit' => "return confirm('確定要公開活動嗎？候選人資料會設定成唯讀');"]) !!}
                <button type="submit" class="btn btn-danger" title="公開活動">
                    <i class="fa fa-eye"></i> 公開活動
                </button>
                {!! Form::close() !!}
            @endif
            @endpermission

            {{-- 活動公開後不可新增選舉，disable按鈕 --}}
            @permission('election.create')
            @if($activity->publicity)
                <a href="javascript:void(0)" class="btn btn-primary" role="button" disabled="disabled">
                    <i class="fa fa-plus-square"></i> 新增選舉
                </a>
            @else
                <a href="{{ route('election.create', ['id' => $activity->id]) }}" class="btn btn-primary" role="button">
                    <i class="fa fa-plus-square"></i> 新增選舉
                </a>
            @endif
            @endpermission
            @permission('tree.access')
                <a href="{{ route('tree.index', ['activity' => $activity->id]) }}" class="btn btn-success" role="button" title="資源樹狀圖">
                    <i class="fa fa-tree" aria-hidden="true"></i>
                </a>
            @endpermission
            <a href="{{ route('notification.index', ['activity' => $activity->id]) }}" class="btn btn-info" role="button" title="投票活動公告">
                <i class="fa fa-list-ol" aria-hidden="true"></i>
                @if($activity->notifications->count())
                    <span class="badge">{{ $activity->notifications->count() }}</span>
                @endif
            </a>
            {{-- 社群分享按鈕 --}}
            @include('common.share-button-bar', ['title' => $basePresenter->getPageTitle($__env->yieldContent('title')), 'url' => URL::current()])
        </div>
        <div class="page-header">
            <h1>{{ $activity->name }}</h1>
        </div>
        <blockquote>
            <p>
                投票日：{{ $activityPresenter->getVoteDay($activity) }}<br/>
                開票時間：{{ $activityPresenter->getBillingTime($activity) }}
            </p>
            {!! Hackersir\Helper\MarkdownHelper::translate($activity->process) !!}
            <p>
                @if($activity->reporting_link != '')
                    選舉公報： {{ link_to($activity->reporting_link, null, ['style' => 'word-break:break-all;', 'target' => '_blank']) }}<br/>
                @endif
                @if($activity->results_report_link != '')
                    投票結果公告： {{ link_to($activity->results_report_link, null, ['style' => 'word-break:break-all;', 'target' => '_blank']) }}<br/>
                @endif
            </p>
        </blockquote>

        <div class="row text-center">
            @forelse($activity->elections as $election)
                @include('election.block', compact('election'))
            @empty
                <div class="col-sm-12">
                    <div class="well">
                        目前沒有選舉
                        @permission('election.create')
                        @if(!$activity->publicity)
                            ，可以按上方的「新增選舉」
                        @endif
                        @endpermission
                    </div>
                </div>
            @endforelse
        </div>
    </div>
@endsection
