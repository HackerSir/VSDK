@extends('app')

@section('title', '編輯影片清單 - ' . $activity->name)

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $activity->name, $activity) }}</li>
            <li>{{ link_to_route('activity.video.show', '影片清單', $activity) }}</li>
            <li class="active">編輯</li>
        </ol>
        <div class="page-header">
            <h1>編輯直播影片清單</h1>
        </div>
        {{ Form::open(['route' => ['activity.video.update', $activity]]) }}
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>投票所</th>
                <th>直播影片網址（目前限填Youtube影片網址）</th>
            </tr>
            </thead>
            <tbody>
                @foreach($votingStations as $votingStation)
                    <tr>
                        <td>{{ $votingStation->name }}</td>
                        <td>
                            <div class="form-group has-feedback{{ ($errors->first("video.$votingStation->id"))?' has-error':'' }}" style="margin: 0">
                                {{ Form::text("video[$votingStation->id]", $votingStation->pivot->video, ['class' => 'form-control']) }}
                                @if($errors->first("video.$votingStation->id"))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first("video.$votingStation->id") }}</span>@endif
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        <div class="form-group">
            <div class="col-md-12 text-center">
                {!! Form::submit('確認', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('activity.video.show', $activity) }}" class="btn btn-default">返回</a>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
