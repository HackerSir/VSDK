@extends('app')

@section('title', '影片清單 - ' . $activity->name)

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $activity->name, $activity) }}</li>
            <li class="active">影片清單</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('activity.show', $activity) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉活動</a>
            <a href="{{ route('activity.video.edit', $activity) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯影片清單</a>
        </div>
        <div class="page-header">
            <h1>直播影片清單</h1>
        </div>
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>投票所</th>
                <th>直播影片網址</th>
            </tr>
            </thead>
            <tbody>
                @forelse($votingStations as $votingStation)
                    <tr>
                        <td>{{ $votingStation->name }}</td>
                        <td>
                            @if($votingStation->pivot->video)
                                {{ link_to($votingStation->pivot->video, $votingStation->pivot->video, ['target' => '_blank']) }}
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2">
                            此活動無任何投票所
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
    </div>
@endsection
