{{--
    $name: 控制項名稱
    $image: 原有圖片(可選)
--}}
{!! Html::script('build-js/fileinput.min.js') !!}
{!! Html::script('build-js/fileinput_locale_zh-TW.js') !!}
<script>
    var $url_input_field = $('input[name={{ $name }}]');
    var $upload_field = $("#{{ $name }}_upload");

    $upload_field.fileinput({
        language: 'zh-TW',
        uploadExtraData: {
            '_token': '{{ Session::token() }}',
            'input_name': $upload_field.attr('name')
        },
        deleteExtraData: {
            '_token': '{{ Session::token() }}'
        },
        maxFileCount: 1,
        autoReplace: true,
        allowedFileTypes: ['image'],
        uploadUrl: '{{ route('image.upload') }}',
        dropZoneEnabled: false,
        @if(isset($image))
        initialPreview: [
            "<img src='{{ $image }}' class='file-preview-image' title='{{ substr($image, strrpos($image, '/') + 1) }}' style='max-width:200px;max-height:200px;width:auto;height:auto;'>"
        ],
        initialPreviewConfig: [
            {
                caption: '{{ substr($image, strrpos($image, '/') + 1) }}',
                url: '{{ route('image.delete') }}',
                key: '{{ $image }}'
            }
        ],
        @endif
        overwriteInitial: false
    });
    $upload_field.on('fileuploaded', function (event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('Uploaded: ' + response.url);
        $url_input_field.val(response.url);
    });
    $upload_field.on('filedeleted', function (event, key) {
        console.log('Deleted: ' + key);
        $url_input_field.val('');
    });
</script>
