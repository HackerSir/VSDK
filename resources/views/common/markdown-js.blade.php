{{--
    $name: 控制項名稱
    $labelName: label 名稱
--}}
<script type="text/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // e.target -> newly activated tab
        if (e.target.id == 'tab_{{ $name }}_preview') {
            $("#{{ $name }}_preview").html('<i class="fa fa-spinner fa-spin"></i> Loading...');
            $("#{{ $name }}_preview").css('min-height', $('#{{ $name }}').height());

            var URLs = "{{ URL::route('markdown.preview') }}";
            var val = $('#{{ $name }}').val();

            $.ajax({
                url: URLs,
                data: val,
                headers: {
                    'X-CSRF-Token': "{{ Session::token() }}",
                    "Accept": "application/json"
                },
                type: "POST",
                dataType: "text",

                success: function (data) {
                    if (data) {
                        $("#{{ $name }}_preview").html(data);
                    } else {
                        alert("error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    });
</script>
