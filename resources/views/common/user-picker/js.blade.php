{{--
    $name: 控制項名稱
    $role: 限定角色
--}}
<script type="text/javascript">
    function formatTemplate(user) {
        if (user.loading) return user.text;
        if (!user.name) return null;

        var markup = "<div class='container' style='width: 100%'><div class='row'>"
            + "<div class='col-md-1'><img src='" + user.gravatar + "' /></div>"
            + "<div>" + user.name + "<br/><small>" + user.email + "</small></div>"
            + "</div></div>";

        return markup;
    }

    function formatTemplateSelection(user) {
        return user.name || user.text;
    }

    $("#{{ $name }}").select2({
        language: "zh-TW",
        placeholder: '請設定選務人員',
        tags: true,
        tokenSeparators: [',', ' '],
        ajax: {
            url: "{{ route('api.user-list') }}",
            type: 'POST',
            headers: {
                'X-CSRF-Token': "{{ Session::token() }}",
                "Accept": "application/json"
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    confirm: true,  // 信箱驗證
                    role: '{{ $role }}'   // 角色
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                    more: false
                };
            },
            cache: true
        },
        minimumInputLength: 0,
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        templateResult: formatTemplate, // omitted for brevity, see the source of this page
        templateSelection: formatTemplateSelection // omitted for brevity, see the source of this page
    });
</script>
