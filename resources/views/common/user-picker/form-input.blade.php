{{--
    $name: 控制項名稱
    $userCollection: 原始用戶集合
--}}
<div class="form-group has-feedback{{ ($errors->has($name))?' has-error':'' }}">
    <div class="col-md-12">
        <select name="{{ $name }}[]" id="{{ $name }}" multiple="" aria-hidden="true" class="form-control">
            {{-- FIXME: 須防止選取不存在的項目（例如：自己打字） --}}
            {{-- FIXME: 預選取的方式需要改善 --}}
            @foreach($userCollection as $user)
                <option selected="selected" value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
        </select>

        @if($errors->has($name))
            <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
            <span class="label label-danger">{{ $errors->first($name) }}</span><br/>@endif
    </div>
</div>
