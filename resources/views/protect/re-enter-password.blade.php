@extends('app')

@section('title', '密碼驗證')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {!! Form::open(['route' => ['protect.re-enter-password'], 'class' => 'form-horizontal']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">密碼驗證</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group has-feedback{{ ($errors->has('password'))?' has-error':'' }}">
                                <label class="control-label col-md-3" for="password">密碼</label>
                                <div class="col-md-8">
                                    {!! Form::password('password', ['placeholder' => '請輸入密碼', 'class' => 'form-control', 'required']) !!}
                                    @if($errors->has('password'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span class="label label-danger">{{ $errors->first('password') }}</span><br />@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i> 確認
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
