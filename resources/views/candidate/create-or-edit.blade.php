@extends('app')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')
@inject('departmentPresenter', 'Hackersir\Presenters\DepartmentPresenter')
@inject('candidatePresenter', 'Hackersir\Presenters\CandidatePresenter')

@php($isEditMode = isset($candidate))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '候選人')

@section('css')
    @include('common.fileinput.css')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('candidate.update', $candidate))->put() !!}
                        {!! BootForm::bind($candidate) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('candidate.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}候選人</legend>
                        {{-- 選舉名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">選舉</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $relation->election->name }}</p>
                            </div>
                        </div>
                        {{-- 候選人分組名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">分組</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $categoryPresenter->getFullName($relation->category) }}</p>
                            </div>
                        </div>
                        {{-- 候選人組編號 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">號次</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $group->number }}</p>
                                {{ Form::hidden('group', Request::get('gid')) }}
                            </div>
                        </div>
                        {{-- 照片 --}}
                        {{-- FIXME: 把必須加上 --}}
                        {!! BootForm::text('圖片網址', 'photo_url')->placeholder('填入Imgur圖片網址，或使用下方上傳功能') !!}
                        {{-- FIXME: 右上角清除按鍵看不見 --}}
                        {!! BootForm::file('圖片上傳', 'photo_url_upload') !!}
                        {{-- 候選人別 --}}
                        {!! BootForm::text('候選人別', 'title') !!}
                        {{-- 姓名 --}}
                        {!! BootForm::text('姓名', 'name')->required() !!}
                        {{-- 性別 --}}
                        {!! BootForm::select('性別', 'gender')->options([null => '-- 下拉選擇性別 --', '男' => '男', '女' => '女'])->required() !!}
                        {{-- 科系 --}}
                        {!! BootForm::select('科系', 'department_id')->options($departmentPresenter->getSelectList($departments))->required() !!}
                        {{-- 年級 --}}
                        {!! BootForm::select('年級', 'grade')->options($candidatePresenter->getGradeSelectList())->required() !!}
                        {{-- 班級 --}}
                        {!! BootForm::select('班級', 'class_no')->options($candidatePresenter->getClassSelectList())->required() !!}
                        {{-- 年齡 --}}
                        <div class="form-group has-feedback{{ ($errors->has('age'))?' has-error':'' }}">
                            {!! Form::label('age', '年齡', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                @if($isEditMode)
                                    {!! Form::number('age', $candidate->age, ['id' => 'age', 'class' => 'form-control', 'min' => '0', 'max' => '150', 'required']) !!}
                                @else
                                    {!! Form::number('age', null, ['id' => 'age', 'class' => 'form-control', 'min' => '0', 'max' => '150', 'required']) !!}
                                @endif
                                @if($errors->has('age'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first('age') }}</span>@endif
                            </div>
                        </div>
                        {{-- 經歷 --}}
                        @include('common.markdown', [
                            'name' => 'experience',
                            'labelName' => '經歷',
                            'text' => $isEditMode ? $candidate->experience : null,
                        ])
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($relation->election->categories->count() == 1)
                                    <a href="{{ route('election.show', $relation->election) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('category.show-category', $relation) }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- 經歷  --}}
    @include('common.markdown-js', ['name' => 'experience'])
    {{-- 照片 --}}
    @if($isEditMode && $candidate->photo_url)
        @include('common.fileinput.js', ['name' => 'photo_url', 'image' => $candidate->photo_url])
    @else
        @include('common.fileinput.js', ['name' => 'photo_url'])
    @endif
@endsection
