{{--
    $candidate: 候選人
--}}
<div class="row" style="margin-top: 10px">
    <div class="col-sm-3" style="max-height: 300px">
        <img src="{{ $imgurImagePresenter->getSafeURL($candidate->photo_url, '350x450') }}"
             class="img-responsive" style="max-height: 300px; margin: auto"
        >
    </div>
    <div class="col-sm-9">
        <h3 style="margin-top: 10px">個人資料：
            <small>
                @if(!$election->activity->publicity && $election->staffs->pluck('id')->contains(auth()->id()))
                    @permission('candidate.edit')
                    <a href="{{ route('candidate.edit', $candidate) }}" class="btn btn-link"><i class="fa fa-pencil"></i> 編輯</a>
                    @endpermission
                    @permission('candidate.delete')
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['candidate.destroy', $candidate],
                        'style' => 'display: inline',
                        'onSubmit' => "return confirm('確定要刪除此候選人嗎？');"
                    ]) !!}
                    <button type="submit" class="btn btn-link">
                        <i class="fa fa-times"></i> 刪除
                    </button>
                    {!! Form::close() !!}
                    @endpermission
                @endif
            </small>
        </h3>
        @if($candidate->title)
            <p>候選人別：{{ $candidate->title }}</p>
        @endif
        <p>姓名：{{ $candidate->name }}</p>
        <p>性別：{{ $candidate->gender }}</p>
        <p>系級：{{ $candidatePresenter->getDepartmentGradeClass($candidate->department, $candidate->grade, $candidate->class_no) }}</p>
        <p>年齡：{{ $candidate->age }}</p>
        <p>經歷：{!! Hackersir\Helper\MarkdownHelper::translate($candidate->experience) !!}</p>
    </div>
</div>
