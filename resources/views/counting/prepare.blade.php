@extends('app')

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
@endsection

@section('content')
    <div class="container">
        @include('counting.common-head')
        <h3 class="text-center text-danger">選舉資料準備中</h3>
    </div>
@endsection
