@extends('app')

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
@endsection

@section('content')
    <div class="container">
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            @if($activity->publicity)
                <li>{{ link_to_route('activity.show', $activity->name, $activity) }}</li>
            @else
                <li>{{ $activity->name }}</li>
            @endif
            <li class="active">各站開票狀態</li>
        </ol>
        <div class="btn-margin-bottom">
            @if($activity->publicity)
                <a href="{{ route('activity.show', $activity) }}" class="btn btn-default" role="button"><i class="fa fa-info-circle" aria-hidden="true"></i> 選舉資訊</a>
                <a href="{{ route('counting.show', $activity) }}" class="btn btn-default" role="button"><i class="fa fa-video-camera" aria-hidden="true"></i> 開票直播</a>
            @endif
            {{-- 社群分享按鈕 --}}
            @inject('basePresenter', 'Hackersir\Presenters\BasePresenter')
            @include('common.share-button-bar', ['title' => $basePresenter->getPageTitle($__env->yieldContent('title')), 'url' => URL::current()])
        </div>
        <div class="page-header">
            <h1>{{ $activity->name }} - 各站開票狀態</h1>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        @foreach($activity->elections as $election)
                            <th class="text-center">{{ $election->name }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($votingStations as $votingStation)
                        <tr>
                            <td>{{ $votingStation->name }}</td>
                            @foreach($activity->elections as $election)
                                <td class="text-center" style="font-size: 1.3em">
                                    @if(isset($status[$votingStation->id][$election->id]))
                                        @if($activity->billing_start->gt(\Carbon\Carbon::now()))
                                            <span class="label label-warning">等待開票</span>
                                        @elseif($status[$votingStation->id][$election->id] == true)
                                            <span class="label label-success">開票結束</span>
                                        @else
                                            <span class="label label-danger">開票中</span>
                                        @endif
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
