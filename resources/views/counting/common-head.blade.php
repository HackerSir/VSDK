<ol class="breadcrumb">
    現在位置：
    <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
    @if($activity->publicity)
        <li>{{ link_to_route('activity.show', $activity->name, $activity) }}</li>
    @else
        <li>{{ $activity->name }}</li>
    @endif
    <li class="active">開票直播</li>
</ol>
<div class="btn-margin-bottom">
    @if($activity->publicity)
        <a href="{{ route('activity.show', $activity) }}" class="btn btn-default" role="button"><i class="fa fa-info-circle" aria-hidden="true"></i> 選舉資訊</a>
        <a href="{{ route('counting.status', $activity) }}" class="btn btn-default" role="button"><i class="fa fa-bar-chart" aria-hidden="true"></i> 各站開票狀態</a>
        <a href="{{ route('notification.index', ['activity' => $activity->id]) }}" class="btn btn-info" role="button" title="投票活動公告">
            <i class="fa fa-list-ol" aria-hidden="true"></i>
            @if($activity->notifications->count())
                <span class="badge">{{ $activity->notifications->count() }}</span>
            @endif
        </a>
    @endif
    {{-- 社群分享按鈕 --}}
    @inject('basePresenter', 'Hackersir\Presenters\BasePresenter')
    @include('common.share-button-bar', ['title' => $basePresenter->getPageTitle($__env->yieldContent('title')), 'url' => URL::current()])
</div>
<div class="page-header">
    <h1>{{ $activity->name }} - 開票直播 <span id="status_label" class="label label-primary">{{ trans('activity_status'.'.'.$status) }}</span></h1>
</div>
