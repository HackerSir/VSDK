@extends('app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>開票管理</h1>
        </div>
        @foreach($groupedRelation as $key => $relations)
            <h2>{{ $key }}</h2>
            <div class="row" style="margin: 0">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-5">選舉</th>
                        <th class="col-md-5">開票所</th>
                        <th class="col-md-2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($relations as $relation)
                        <tr>
                            <td style="vertical-align: middle">{{ $relation->election->name }}</td>
                            <td style="vertical-align: middle">{{ $relation->votingStation->name }}</td>
                            <td class="text-center">
                                @if(!$relation->counting_over)
                                    <a href="{{ route('counting.manage.show', $relation) }}" class="btn btn-primary" role="button">前往</a>
                                @else
                                    <span class="btn btn-default disabled">開票已結束</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
@endsection
