@extends('app')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')

@section('css')
    {!! Html::style('build-css/animate.min.css') !!}
@endsection

@section('content')
    <div class="container">
        <a href="{{ route('counting.manage.index') }}" class="btn btn-default" role="button"><i
                    class="fa fa-arrow-left"></i> 返回清單</a>
        {!!  Form::open(['route' => ['counting.manage.close', $evsRelation], 'style' => 'display: inline', 'onSubmit' => "return (prompt('確定要結束投票嗎？結束後將無法修改票數，請輸入「確定」', '請在此輸入「確定」以結束開票') == '確定');"]) !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-exclamation-triangle"></i> 結束開票
        </button>
        {!! Form::close() !!}
        <div class="page-header">
            <h1>開票控制頁</h1>
        </div>
        <h2 id="activity_title">{{ $evsRelation->election->activity->name }}
            <small>{{ $evsRelation->election->name }} - {{ $evsRelation->votingStation->name }}</small>
        </h2>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">選舉分組</h3>
                    </div>
                    <div class="list-group" id="categories">
                        @foreach($groupedRelations as $key => $relations)
                            @if ($key != "all")
                                <button type="button" class="list-group-item list-group-item-info"
                                        style="cursor: default">
                                    {{ $key }}
                                </button>
                            @endif
                            @foreach($relations as $relation)
                                <button id="category_{{ $relation->category->id }}"
                                        data-rid="{{ $relation->id }}"
                                        data-remarkfullname="{{ $categoryPresenter->getRemarkFullName($relation->category) }}"
                                        class="list-group-item" style="padding-left: 2em">
                                    {{ $categoryPresenter->getTitleName($relation->category) }}
                                </button>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">分組資訊</h3>
                    </div>
                    <div class="panel-body">
                        <h4 id="category_title"></h4>
                        <table class="table table-bordered table-hover table-striped" style="font-size: 1.5em">
                            <thead>
                            <tr>
                                <th style="width: 20%" class="text-center">號次</th>
                                <th style="width: 40%">候選人</th>
                                <th style="width: 20%" class="text-center">選票</th>
                                <th style="width: 20%"></th>
                            </tr>
                            </thead>
                            <tbody id="relation_info">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $.fn.extend({
            animateCss: function (animationName, callback) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                    $(this).removeClass('animated ' + animationName);
                    if (callback !== undefined) {
                        callback();
                    }
                });
            }
        });

        var votingStationId = '{{ $evsRelation->votingStation->id }}';

        var $category_title;
        var $activeCategoryButton;
        var $relation_info;

        var isProcessButtonClick = true;

        $(function () {
            $category_title = $('#category_title');
            $relation_info = $('#relation_info');
            var $categoriesButton = $('#categories').find('[id^=category_]');
            $categoriesButton.each(function () {
                $(this).click(clickCategoryButton);
            });

            $activeCategoryButton = $($categoriesButton[0]);
            switchToCategory($activeCategoryButton);

            isProcessButtonClick = false;
        });

        function clickCategoryButton() {
            if (isProcessButtonClick) return;
            isProcessButtonClick = true;
            switchToCategory($(this));
            $('html, body').animate({
                scrollTop: $('#activity_title').offset().top
            }, 200);
        }

        function switchToCategory($button) {
            $activeCategoryButton.removeClass('active');
            $button.addClass('active');

            var title = $button.text();
            var remarkTitle = $button.data('remarkfullname');
            if (remarkTitle != "") {
                title += '(' + remarkTitle + ')';
            }
            $category_title.text(title);

            $relation_info.empty();
            // Load table
            getTableData($button.data('rid'));

            $activeCategoryButton = $button;
        }

        function getTableData(ceid) {
            $.ajax({
                url: "{{ route('category-relation.api.data') }}",
                data: {
                    'rid': ceid,
                    'vsid': votingStationId
                },
                headers: {
                    "Accept": "application/json",
                    'X-CSRF-Token': "{{ Session::token() }}"
                },
                type: "GET",
                dataType: "json",
                success: function (msg) {
                    if (msg.success) {
                        loadTable(msg.data, ceid);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    show_message('error', xhr.status + ': ' + thrownError + '<br/>');
                },
                complete: function () {
                    isProcessButtonClick = false;
                }
            });
        }

        function loadTable(data, ceid) {
            ballotButtons = [];
            data.forEach(function (item) {
                if (typeof item.ballot === 'object') {
                    $relation_info.append(generateRows(item, ceid, 'agree'));
                    $relation_info.append(generateRows(item, ceid, 'disagree'));
                }
                else {
                    if (item.name === '廢票') {
                        $relation_info.append(generateRows(item, ceid, 'invalid'));
                    }
                    else {
                        $relation_info.append(generateRows(item, ceid, 'simple'));
                    }
                }
            })
        }

        function generateRows(item, ceid, type) {
            var $tr = $('<tr>');
            //=================================
            var numberText;
            if (type !== 'disagree') {
                numberText = item.number;
            }
            $tr.append($('<td>').text(numberText).css('vertical-align', 'middle').addClass('text-center'));
            //=================================
            var $td_name = $('<td>').text(item.name).css('vertical-align', 'middle');

            if (type === 'agree') {
                $td_name.append(' <span class="label label-primary">同意</span>');
            }
            else if (type === 'disagree') {
                $td_name.append(' <span class="label label-danger">反對</span>');
            }

            $tr.append($td_name);
            //=================================
            var ballotText;
            if (type === 'agree') {
                ballotText = item.ballot['agree'];
            }
            else if (type === 'disagree') {
                ballotText = item.ballot['disagree'];
            }
            else {
                ballotText = item.ballot;
            }
            $tr.append($('<td>').text(ballotText).css('vertical-align', 'middle').addClass('text-center'));
            //=================================
            $button = $('<button>').addClass("btn btn-warning").text("+1");
            $button.click(ballotButtonClickHandler);
            $button.data('minus', false);
            ballotButtons.push($button);

            $minusButton = $('<button>').addClass("btn btn-danger").text("-1");
            $minusButton.click(ballotButtonClickHandler);
            $minusButton.data('minus', true);
            ballotButtons.push($minusButton);

            $tr.append($('<td>').append(
                $button,
                ' ',
                $minusButton
            ));
            //=================================
            $tr.data('categoryElectionRelation', ceid);
            $tr.data('trType', type);

            if (item.hasOwnProperty('gid')) {
                $tr.data('gid', item.gid);
            }

            if (type === 'disagree') {
                $tr.data('type', '{{ Hackersir\Ballot::VALID_OBJECTION }}');
            }
            else if (type === 'invalid') {
                $tr.data('type', '{{ Hackersir\Ballot::INVALID }}');
            }
            else {
                // default
                $tr.data('type', '{{ Hackersir\Ballot::VALID }}');
            }
            //=================================
            return $tr;
        }

        function ballotButtonClickHandler() {
            if (isProcessButtonClick) return;
            isProcessButtonClick = true;
            disabledBallotButton();

            var $ballotButton = $(this);
            var $tr = $ballotButton.closest('tr');

            $ballotButton.parent().append(' <i class="fa fa-spinner fa-spin fa-lg" aria-hidden="true"></i>');

            var data = {
                'categoryElectionRelation': $tr.data('categoryElectionRelation'),
                'votingStation': votingStationId,
                'type': $tr.data('type')
            };

            if ($tr.data('gid') !== undefined) {
                data.group = $tr.data('gid');
            }
            var isMinus = $ballotButton.data('minus');
            var $url;
            if (isMinus == false) {
                $url = "{{ route('ballot.store') }}";
            } else {
                if (!confirm('確定刪除選票？')) {
                    enabledBallotButton();
                    return;
                }
                $url = "{{ route('ballot.store', ['method' => 'destroy']) }}";
            }
            $.ajax({
                url: $url,
                data: data,
                headers: {
                    "Accept": "application/json",
                    'X-CSRF-Token': "{{ Session::token() }}"
                },
                type: "POST",
                dataType: "json",
                success: function (msg) {
                    if (msg.success) {
                        $relation_info.find('tr').each(function () {
                            var $tr = $(this);
                            var trType = $tr.data('trType');
                            var $ballot_td = $tr.children('td:nth-child(3)');
                            var gid = $tr.data('gid');

                            var beforeText = $ballot_td.text();
                            var afterText;
                            if (trType === 'invalid') {
                                afterText = msg.data['invalid'];
                            }
                            else if (trType === 'agree') {
                                afterText = msg.data.data[gid]['agree'];
                            }
                            else if (trType === 'disagree') {
                                afterText = msg.data.data[gid]['disagree'];
                            }
                            else if (trType === 'simple') {
                                afterText = msg.data.data[gid];
                            }

                            if (beforeText != afterText) {
                                $ballot_td.animateCss('flipOutX', function () {
                                    $ballot_td.text(afterText);
                                    $ballot_td.animateCss('flipInX');
                                });
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var errorMessage = xhr.responseJSON.message + '<br/>';
                    errorMessage += '（' + xhr.status + ': ' + thrownError + '）';
                    show_message('error', errorMessage);
                },
                complete: enabledBallotButton
            });
        }

        var ballotButtons = [];
        function disabledBallotButton() {
            ballotButtons.forEach(function (item) {
                item.prop('disabled', true);
            });
        }

        function enabledBallotButton() {
            ballotButtons.forEach(function (item) {
                item.prop('disabled', false);
            });

            $relation_info.find('i').remove();

            isProcessButtonClick = false;
        }
    </script>
@endsection
