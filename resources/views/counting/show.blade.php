@extends('app')

@inject('activityPresenter', 'Hackersir\Presenters\ActivityPresenter')

@php($data = $activityPresenter->getDataTableInfo($activity, $ballots))

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
    {!! Html::style('build-css/animate.min.css') !!}
    <style>
        .chartWithOverlay {
            position: relative;
        }

        .overlay {
            position: absolute;
            left: 20px;
            bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        @include('counting.common-head')
        @if(!empty($activity->count_help_text))
            <blockquote>
                {!! Hackersir\Helper\MarkdownHelper::translate($activity->count_help_text) !!}
            </blockquote>
        @endif
        @foreach($activity->notifications as $notification)
            <div class="alert alert-{{ $notification->type }}" role="alert">
                <i class="fa {{ $notification->fa_class }}" aria-hidden="true"></i> {{ $notification->text }}
            </div>
        @endforeach
        @if($status == \Hackersir\Utils\ActivityStatus::FINISH)
            <div class="alert alert-info" role="alert">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
                開票已結束，選舉結果以選委會選舉結果報告為主
            </div>
        @endif
        <div style="margin-top: 5px; display: inline-block">
            <span>直播：</span>
            <div style="display: inline-block;">
                <select class="form-control" id="video_list">
                    <option id="option_null" value="null">不顯示</option>
                    @foreach($videoList as $id => $value)
                        <option id="option_{{ $id }}" value="{{ $value['embed'] }}">{{ $value['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div style="margin-top: 5px; display: inline-block">
            <span>圖表：</span>
            <div style="display: inline-block;">
                <select class="form-control" id="chart_list">
                    <option value="">-- 請選擇圖表 --</option>
                </select>
            </div>
        </div>
        <div class="pull-right text-right" style="margin-top: 5px; height: 40px; display: inline-block">
            <span id="connect_status"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>&nbsp;連線中</span>
            <span title="線上觀看人數"><i class="fa fa-users" aria-hidden="true"></i> <span id="online_count"></span></span><br/>
            <i class="fa fa-square" aria-hidden="true" style="color: rgb(51, 102, 204);"></i>同意(得票)
            <i class="fa fa-square" aria-hidden="true" style="color: rgb(220, 57, 18);"></i>反對
            於 <span id="updated_at"></span> 秒前更新
        </div>
        <div class="clearfix"></div>
        <div id="chart_area" class="row" style="padding-top: 10px">
            <div id="video_div" class="col-xs-12 col-md-8">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe id="video_iframe" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div id="no_chart_tip" class="col-xs-12 col-md-4 well text-center"
                 style="height: 250px; padding-top: 5px; display: none">
                <h3>沒有圖表！！</h3>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    {!! Html::script('build-js/js.cookie.js') !!}
    <script type="text/javascript">
        $.fn.extend({
            animateCss: function (animationName, callback) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                    $(this).removeClass('animated ' + animationName);
                    if (callback !== undefined) {
                        callback();
                    }
                });
            }
        });

        Cookies.defaults = {
            expires: 30
        };

        var socketUrl = '{{ env('SOCKET_SERVER_URL', 'http://localhost:6002') }}';

        var $chart_list = $('#chart_list');
        var activityId = '{{ $activity->id }}';
        var $chart_area = $('#chart_area');
        var $no_chart_tip = $('#no_chart_tip');
        var $status_label = $('#status_label');
        var $onlineCount = $('#online_count');

        $(function () {
            initSelect();
            loadShownChartId();
            initChart_list();
        });
    </script>
    {!! Minify::javascript([
        '/js/counting-select.js',
        '/js/counting-socket.js',
        '/js/counting-chart.js'
    ])->withFullUrl() !!}
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        var $closeIcon = $('<i>').addClass('pull-right fa fa-times-circle fa-lg').css('cursor', 'pointer').css('height', '20px');
        $closeIcon.click(clickCloseIcon);
        var clear_div = '<div class="clearfix"></div>';
        var data = {!! Hackersir\Helper\JsonHelper::encode($data) !!};

        lastUpdateAt = new Date({{ $ballots['updated_at'] }} * 1000);

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages': ['corechart', 'bar']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(function () {
            createAllChart();
            initSocket();
        });

        function initChart_list() {
            $chart_list.append(getChartSelectOptions());
            $chart_list.change(function () {
                if (shownChartId.length == 0) {
                    $no_chart_tip.hide();
                }

                var $option = $chart_list.find("option:selected");
                var rid = $option.val();

                var $col_div = $('#chart_' + rid).parent();
                $col_div.show();
                $col_div.animateCss('zoomIn');

                $option.remove();
                $chart_list.prop('selectedIndex', 0);

                shownChartId_add(rid);
            });
        }

        function getChartSelectOptions() {
            var selectOptions = [];
            for (var t in data) {
                if (shownChartId.indexOf(t) >= 0) continue;
                selectOptions.push($('<option>').text(data[t].title).attr('value', t));
            }
            return selectOptions;
        }

        function clickCloseIcon() {
            var $col_div = $(this).parent();
            var id = $col_div.data('id');
            $col_div.animateCss('zoomOut', function () {
                $col_div.hide();

                if (shownChartId_remove(id)) {
                    // 加回選單
                    $chart_list.append($('<option>').text(data[id].title).attr('value', id));

                    var options = $chart_list.find("option");
                    options.detach().sort(function (a, b) {
                        var at = parseInt($(a).val()) || -1;
                        var bt = parseInt($(b).val()) || -1;
                        return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
                    });
                    options.appendTo($chart_list);
                    $chart_list.prop('selectedIndex', 0);
                }

                if (shownChartId.length == 0) {
                    $no_chart_tip.show();
                }
            });
        }
    </script>
    <script>
        var shownChartId;
        function loadShownChartId() {
            shownChartId = Cookies.get(activityId + 'shownChartId');
            if (shownChartId !== undefined) {
                shownChartId = JSON.parse(shownChartId);
            }
            else {
                shownChartId = [];
            }
        }

        function shownChartId_add(rid) {
            shownChartId.push(rid);
            Cookies.set(activityId + 'shownChartId', shownChartId);
            socket.emit('join_room', rid);
        }

        function shownChartId_remove(rid) {
            var index = shownChartId.indexOf(rid);
            if (index != -1) {
                shownChartId.splice(index, 1);
                Cookies.set(activityId + 'shownChartId', shownChartId);

                // 離開房間
                socket.emit('leave_room', rid);
                return true;
            }
            return false;
        }
    </script>
@endsection
