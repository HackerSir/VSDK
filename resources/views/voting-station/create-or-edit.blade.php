@extends('app')

@section('title', $methodText. '投票所')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if(isset($votingStation))
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('votingStation.update', $votingStation))->put() !!}
                        {!! BootForm::bind($votingStation) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('votingStation.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}投票所</legend>
                        {!! BootForm::text('名稱', 'name')->required() !!}
                        {!! BootForm::text('地點', 'location')->required() !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if(isset($votingStation))
                                    <a href="{{ route('votingStation.show', $votingStation) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('votingStation.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
