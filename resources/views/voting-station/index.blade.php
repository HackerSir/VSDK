@extends('app')

@section('title', '投票所')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">投票所</li>
        </ol>
        <a href="{{ route('votingStation.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增投票所</a>
        <div class="page-header">
            <h1>投票所</h1>
        </div>
        <div class="list-group">
            @foreach($votingStations as $votingStation)
                <a href="{{route('votingStation.show', $votingStation)}}" class="list-group-item">
                    <h3 class="list-group-item-heading">{{ $votingStation->name }}</h3>
                    <p class="list-group-item-text">
                        <span style="display: inline-block">地點：{{ $votingStation->location }}</span>
                    </p>
                </a>
            @endforeach
        </div>
    </div>
@endsection
