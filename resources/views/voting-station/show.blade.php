@extends('app')

@section('title', $votingStation->name)

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('votingStation.index', '投票所') }}</li>
            <li class="active">{{ $votingStation->name }}</li>
        </ol>
        <a href="{{ route('votingStation.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回投票所</a>
        <a href="{{ route('votingStation.edit', $votingStation) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯投票所</a>
        <div class="page-header">
            <h1>{{ $votingStation->name }}</h1>
        </div>
        <blockquote>
            <p>
                地點：{{ $votingStation->location }}
            </p>
        </blockquote>
    </div>
@endsection
