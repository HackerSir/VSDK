@extends('app')

@section('title', '網站記錄')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('websiteLog.index', '網站記錄') }}</li>
            <li class="active">詳細資料</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('websiteLog.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回網站記錄</a>
        </div>
        <blockquote>
            <p>
                DateTime：{{ $websiteLog->created_at }}<br/>
                Tag：{{ $websiteLog->tag }}<br/>
                @if($websiteLog->user)
                    User：{!! link_to_route('user.show', $websiteLog->user->name, $websiteLog->user) !!}<br/>
                @endif
                Message：{{ $websiteLog->message }}
            </p>
        </blockquote>
        <div>
            {!! preg_replace('/>[^>]+\\(line \\d+\\)/i', '>Data', dump_r($websiteLog->json, true), 1) !!}
        </div>
    </div>
@endsection
