@extends('app')

@section('title', '網站記錄')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">網站記錄</li>
        </ol>
        <div class="page-header">
            <h1>網站記錄</h1>
        </div>
        <div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>時間</th>
                    <th>類別</th>
                    <th>使用者</th>
                    <th>訊息</th>
                    <th>資料</th>
                </tr>
                </thead>
                <tbody>
                @foreach($websiteLogs as $websiteLog)
                    <tr>
                        <td>{{ $websiteLog->created_at }}</td>
                        <td>{{ $websiteLog->tag }}</td>
                        <td>
                            @if($websiteLog->user)
                                {!! link_to_route('user.show', $websiteLog->user->name, $websiteLog->user) !!}
                            @endif
                        </td>
                        <td>{{ $websiteLog->message }}</td>
                        <td>
                            {{ str_limit($websiteLog->data, 100, '...') }}
                            <a href="{{ route('websiteLog.show', $websiteLog) }}" class="pull-right"><i class="fa fa-book"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="text-center">
            {!! $websiteLogs->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection
