@extends('app')

@section('title', $college->name)

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('college.index', '學院管理') }}</li>
            <li class="active">{{ $college->name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('college.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回學院管理</a>
            <a href="{{ route('college.edit', $college) }}" class="btn btn-primary"><i class="fa fa-cogs"></i> 編輯學院</a>
        </div>
        <div class="page-header">
            <h1>{{ $college->name }}</h1>
        </div>
        <blockquote>
            名稱：{{ $college->name }}<br/>
            簡稱：{{ $college->abbreviation }}<br/>
        </blockquote>
    </div>
@endsection
