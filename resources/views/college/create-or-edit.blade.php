@extends('app')

@php($isEditMode = isset($college))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '學院')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('college.index', '學院管理') }}</li>
            <li class="active">{{ $methodText }}學院</li>
        </ol>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('college.update', $college))->put() !!}
                        {!! BootForm::bind($college) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('college.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}學院</legend>
                        {{-- 學院名稱 --}}
                        @if($isEditMode)
                            {!! BootForm::text('學院名稱', 'name')->placeholder('如：資訊電機學院')->helpBlock('科系建立後不可修改')->readonly() !!}
                        @else
                            {!! BootForm::text('學院名稱', 'name')->placeholder('如：資訊電機學院')->helpBlock('科系建立後不可修改')->required() !!}
                        @endif
                        {{-- 學院簡稱 --}}
                        {!! BootForm::text('學院簡稱', 'abbreviation')->placeholder('如：資電學院')->required() !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('college.show', $college) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('college.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
