@extends('app')

@section('title', '學院管理')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">學院管理</li>
        </ol>
        @permission('college.create')
        <a href="{{ route('college.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增學院</a>
        @endpermission
        <div class="page-header">
            <h1>學院管理</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>名稱</th>
            </tr>
            </thead>
            <tbody>
            @foreach($colleges as $college)
                <tr>
                    <td style="vertical-align: middle">
                        <a href="{{ route('college.show', $college) }}">{{ $college->name }}</a>
                        @permission('college.edit')
                        <a href="{{ route('college.edit', $college) }}" class="pull-right"><i class="fa fa-pencil"></i></a>
                        @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $colleges->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection
