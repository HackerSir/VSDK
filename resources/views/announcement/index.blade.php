@extends('app')

@section('title', '網站公告')

@section('content')
    <div class="container">
        @permission('announcement.manage')
        <a href="{{ route('announcement.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增公告</a>
        @endpermission
        <div class="page-header">
            <h1>網站公告</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th class="text-center col-xs-2">公告日</th>
                <th class="text-center col-xs-9">標題</th>
            </tr>
            </thead>
            <tbody>
            @foreach($announcements as $announcement)
                <tr>
                    <td class="text-center">
                        {!! $announcement->updated_at->format('Y/m/d') !!}
                    </td>
                    <td>
                        {!! link_to_route('announcement.show', $announcement->title, $announcement) !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-center">
        {!! $announcements->appends(Request::except(['page']))->render() !!}
    </div>
@endsection
