@extends('app')

@section('title', $announcement->title)

@section('content')
    <div class="container">
        <div class="btn-margin-bottom">
            <a href="{{ route('announcement.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回網站公告</a>
            @permission('announcement.manage')
            <a href="{{ route('announcement.edit', $announcement) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯公告</a>
            {!! Form::open(['route' => ['announcement.destroy', $announcement], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此公告嗎？');"]) !!}
            <button type="submit" class="btn btn-danger">
                <i class="fa fa-trash"></i> 刪除公告
            </button>
            {!! Form::close() !!}
            @endpermission
        </div>
        <div class="page-header">
            <h1>{{ $announcement->title }}</h1>
        </div>
        <div class="row" style="margin: 0">
            <table class="table table-bordered">
                <tr>
                    <td class="text-center col-xs-4 col-md-2">公告時間</td>
                    <td class="col-xs-8 col-md-10">{!! $announcement->updated_at !!}</td>
                </tr>
                @if(Entrust::can('user.view'))
                    <tr class="danger">
                        <td class="text-center col-xs-4 col-md-2">發布者</td>
                        <td class="col-xs-8 col-md-10">
                            {!! link_to_route('user.show', $announcement->user->name, $announcement->user) !!}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td class="text-center hidden-xs col-md-2">內容</td>
                    <td class="col-xs-12 col-md-10" colspan="2">
                        <div style="min-height: 40vh;">
                            {!! Hackersir\Helper\MarkdownHelper::translate($announcement->message) !!}
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
