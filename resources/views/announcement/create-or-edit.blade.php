@extends('app')

@section('title', $methodText. '公告')

@section('css')
    {!! Html::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if(isset($announcement))
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('announcement.update', $announcement))->put() !!}
                        {!! BootForm::bind($announcement) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('announcement.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}公告</legend>
                        {!! BootForm::text('標題', 'title')->required() !!}
                        {{--公告內容(Markdown) --}}
                        @include('common.markdown', [
                            'name' => 'message',
                            'labelName' => '內容',
                            'text' => isset($announcement) ? $announcement->message : null
                        ])
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if(isset($announcement))
                                    <a href="{{ route('announcement.show', $announcement) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('announcement.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('common.markdown-js', ['name' => 'message'])
@endsection
