@extends('app')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')
@inject('candidatePresenter', 'Hackersir\Presenters\CandidatePresenter')
@inject('basePresenter', 'Hackersir\Presenters\BasePresenter')
@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('title', $categoryPresenter->getTitleName($category) . ' - ' . $election->name)

@section('css')
    {!! Html::style('css/bootstrap-social.css') !!}
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('activity.index', '選舉活動') }}</li>
            <li>{{ link_to_route('activity.show', $election->activity->name, $election->activity) }}</li>
            <li>{{ link_to_route('election.show', $election->name, $election) }}</li>
            <li class="active">{{ $categoryPresenter->getTitleName($category) }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('election.show', $election) }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉</a>
            @if(!$election->activity->publicity)
                @permission('category.relation.delete')
                {!! Form::open([
                    'method' => 'DELETE',
                    'route' => ['category-relation.delete', $category, 'eid' => $election->id],
                    'style' => 'display: inline',
                    'onSubmit' => "return confirm('確定要刪除此分組嗎？');"
                ]) !!}
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-trash"></i> 刪除分組
                </button>
                {!! Form::close() !!}
                @endpermission
            @else
                <a href="javascript:void(0)" class="btn btn-danger" role="button" disabled="disabled">
                    <i class="fa fa-trash"></i> 刪除分組
                </a>
            @endif
            {{-- 社群分享按鈕 --}}
            @include('common.share-button-bar', ['title' => $basePresenter->getPageTitle($__env->yieldContent('title')), 'url' => URL::current()])
        </div>
        <div class="page-header">
            <h1>{{ $election->name }} - {{ $categoryPresenter->getTitleName($category) }}
                @if($relation->elected_group_amount > 1)
                <small> (應選{{ $relation->elected_group_amount }}人)</small>
                @endif
            </h1>
        </div>
        @include('group.collection', compact('groups', 'candidatePresenter', 'relation'))
    </div>
@endsection
