@extends('app')

@section('title', $category->name)

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')
@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('category.index', '選舉分組管理') }}</li>
            <li class="active">{{ $categoryPresenter->getFullName($category) }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('category.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回選舉分組列表</a>
            <a href="{{ route('category.edit', $category) }}" class="btn btn-primary"><i class="fa fa-cogs"></i> 編輯選舉分組</a>
        </div>
        <div class="page-header">
            <h1>{{ $categoryPresenter->getFullName($category) }}</h1>
        </div>
        <div class="vertical-container" style="height: 320px">
            <img src="{{ $imgurImagePresenter->getSafeURL($category->image) }}"
                 class="img-responsive"
                 style="padding: 0 5% 0 5%; max-height: 300px; max-width: 100%; margin-top: 20px"
            >
        </div>
    </div>
@endsection
