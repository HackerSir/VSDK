@extends('app')

@section('title', '選舉分組管理')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')
@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">選舉分組管理</li>
        </ol>
        @permission('category.create')
        <a href="{{ route('category.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增選舉分組</a>
        @endpermission
        <div class="page-header">
            <h1>選舉分組管理</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>圖示</th>
                <th>名稱</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td class="text-center" style="width: 60px">
                        <img src="{{ $imgurImagePresenter->getSafeURL($category->image, '60x60') }}" style="max-width: 60px; max-height: 60px">
                    </td>
                    <td style="vertical-align: middle">
                        <a href="{{ route('category.show', $category) }}">{{ $categoryPresenter->getFullName($category) }}</a>
                        @permission('category.edit')
                        <a href="{{ route('category.edit', $category) }}" class="pull-right"><i class="fa fa-pencil"></i></a>
                        @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $categories->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection
