@extends('app')

@section('title', '新增組別')

@inject('categoryPresenter', 'Hackersir\Presenters\CategoryPresenter')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('category-relation.store')) !!}
                    <fieldset>
                        <legend>新增組別</legend>
                        {{-- 選舉名稱 --}}
                        <div class="form-group">
                            <label class="col-sm-5 col-lg-2 control-label">選舉</label>
                            <div class="col-sm-7 col-lg-10">
                                <p class="form-control-static">{{ $election->name }}</p>
                                {{ Form::hidden('election_id', $election->id) }}
                            </div>
                        </div>
                        {{-- 分組--}}
                        {!! BootForm::select('分組', 'category')->options($categoryPresenter->getSelectList($categories))->required() !!}
                        {{-- 應選組別數 --}}
                        {!! BootForm::text('應選組別數', 'elected_group_amount')->defaultValue(1)->attribute('min', 1)->required()->type('number') !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit('建立', ['class' => 'btn btn-primary']) !!}
                                <a href="{{ route('election.show', $election) }}" class="btn btn-default">返回</a>
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
