{{--
    顯示候選人組分組 Block

    $relation: 分組和選舉的關聯
    $categoryPresenter: Category 顯示處理
--}}
<div class="col-sm-4 col-md-3">
    <a href="{{ route('category.show-category', $relation) }}">
        <div class="panel panel-default" style="border-top: 1px solid rgb(231, 231, 231);">
            <div class="panel-body">
                <h3 class="text-center" style="margin: 15px 0">{{ $categoryPresenter->getTitleName($relation->category) }}</h3>
            </div>
        </div>
    </a>
</div>
