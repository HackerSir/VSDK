@extends('app')

@php($isEditMode = isset($category))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '分組')

@section('css')
    @include('common.fileinput.css')
@endsection

@inject('departmentPresenter', 'Hackersir\Presenters\DepartmentPresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('category.index', '選舉分組管理') }}</li>
            <li class="active">{{ $methodText }}分組</li>
        </ol>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('category.update', $category))->put() !!}
                        {!! BootForm::bind($category) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('category.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}分組</legend>
                        {{-- 對應科系 --}}
                        @if($isEditMode)
                            {!! BootForm::select('對應科系', 'department')->options([($category->department) ? $departmentPresenter->getFullName($category->department) : '-- 下拉選擇科系 --'])->helpBlock('分組建立後不可修改')->readonly()->disable() !!}
                        @else
                            {!! BootForm::select('對應科系', 'department')->options($departmentPresenter->getSelectList($departments))->helpBlock('分組建立後不可修改') !!}
                        @endif
                        {{-- 名稱（無科系時使用） --}}
                        @if($isEditMode)
                            {!! BootForm::text('分組名稱', 'name')->placeholder('若無對應科系，將以此作為名稱')->helpBlock('分組建立後不可修改')->readonly() !!}
                        @else
                            {!! BootForm::text('分組名稱', 'name')->placeholder('若無對應科系，將以此作為名稱')->helpBlock('分組建立後不可修改') !!}
                        @endif
                        {{-- 圖片 --}}
                        {!! BootForm::text('圖片網址', 'image')->placeholder('填入Imgur圖片網址，或使用下方上傳功能') !!}
                        {{-- FIXME: 右上角清除按鍵看不見 --}}
                        {!! BootForm::file('圖片上傳', 'image_upload') !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('category.show', $category) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('category.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @if($isEditMode && $category->image)
        @include('common.fileinput.js', ['name' => 'image', 'image' => $category->image])
    @else
        @include('common.fileinput.js', ['name' => 'image'])
    @endif
@endsection
