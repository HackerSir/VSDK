@extends('app')

@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li class="active">科系列表</li>
        </ol>
        @permission('department.create')
        <a href="{{ route('department.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增科系</a>
        @endpermission
        <div style="display: inline-block">
            <select name="college" id="college" class="form-control" required="required">
                <option value="">-- 顯示：所有學院 --</option>
                @foreach($colleges as $college)
                    <option value="{{ $college->id }}"{{ ($college->id == Request::get('college')) ? ' selected' : '' }}>{{ $college->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="page-header">
            <h1>科系列表</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>院所</th>
                <th>圖示</th>
                <th>科系(簡稱)</th>
            </tr>
            </thead>
            <tbody>
            @foreach($departments as $department)
                <tr>
                    <td style="vertical-align: middle">{{ $department->college->name }}</td>
                    <td class="text-center" style="width: 60px">
                        <img src="{{ $imgurImagePresenter->getSafeURL($department->image, '60x60') }}" style="max-width: 60px; max-height: 60px">
                    </td>
                    <td style="vertical-align: middle">
                        <a href="{{ route('department.show', $department) }}">[{{ $department->abbreviation }}] {{ $department->name }}</a>
                        @permission('department.edit')
                        <a href="{{ route('department.edit', $department) }}" class="pull-right"><i class="fa fa-pencil"></i></a>
                        @endpermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $departments->appends(Request::except(['page']))->render() !!}
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#college').change(function () {
            var $collegeId = $("#college").find("option:selected").val();
            var $url = '{{ route('department.index') }}';
            if ($collegeId) {
                $url += '?college=' + $collegeId;
            }
            window.location.href = $url;
        });
    </script>
@endsection
