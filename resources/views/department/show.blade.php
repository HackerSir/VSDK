@extends('app')

@inject('imgurImagePresenter', 'Hackersir\Presenters\ImgurImagePresenter')

@section('title', $department->name)

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('department.index', '科系列表') }}</li>
            <li class="active">{{ $department->name }}</li>
        </ol>
        <div class="btn-margin-bottom">
            <a href="{{ route('department.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回科系列表</a>
            <a href="{{ route('department.edit', $department) }}" class="btn btn-primary"><i class="fa fa-cogs"></i> 編輯科系</a>
        </div>
        <div class="page-header">
            <h1>{{ $department->name }}</h1>
        </div>
        <blockquote>
            學院：{{ $department->college->name }}<br/>
            名稱：{{ $department->name }}<br/>
            簡稱：{{ $department->abbreviation }}<br/>
        </blockquote>
        <div class="vertical-container" style="height: 320px">
            <img src="{{ $imgurImagePresenter->getSafeURL($department->image) }}"
                 class="img-responsive"
                 style="padding: 0 5% 0 5%; max-height: 300px; max-width: 100%; margin-top: 20px"
            >
        </div>
    </div>
@endsection
