@extends('app')

@php($isEditMode = isset($department))
@php($methodText = $isEditMode ? '編輯' : '新增')

@section('title', $methodText . '科系')

@section('css')
    @include('common.fileinput.css')
@endsection

@section('content')
    <div class="container">
        {{-- Breadcrumb --}}
        <ol class="breadcrumb">
            現在位置：
            <li>{{ link_to_route('department.index', '科系列表') }}</li>
            <li class="active">{{ $methodText }}科系</li>
        </ol>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if($isEditMode)
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('department.update', $department))->put()->encodingType('multipart/form-data') !!}
                        {!! BootForm::bind($department) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('department.store'))->encodingType('multipart/form-data') !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}科系</legend>
                        {{-- 所屬學院 --}}
                        @if($isEditMode)
                            {!! BootForm::select('所屬學院', 'college')->options([$department->college->name])->helpBlock('科系建立後不可修改')->readonly()->disable() !!}
                        @else
                            {!! BootForm::select('所屬學院', 'college')->options($colleges)->helpBlock('科系建立後不可修改')->required() !!}
                        @endif
                        {{-- 科系名稱 --}}
                        @if($isEditMode)
                            {!! BootForm::text('科系名稱', 'name')->placeholder('如：資訊工程學系')->helpBlock('科系建立後不可修改')->readonly() !!}
                        @else
                            {!! BootForm::text('科系名稱', 'name')->placeholder('如：資訊工程學系')->helpBlock('科系建立後不可修改')->required() !!}
                        @endif
                        {{-- 科系簡稱 --}}
                        {!! BootForm::text('科系簡稱', 'abbreviation')->placeholder('如：資工')->required() !!}
                        {{-- 圖片 --}}
                        {!! BootForm::text('圖片網址', 'image')->placeholder('填入Imgur圖片網址，或使用下方上傳功能') !!}
                        {{-- FIXME: 右上角清除按鍵看不見 --}}
                        {!! BootForm::file('圖片上傳', 'image_upload') !!}
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if($isEditMode)
                                    <a href="{{ route('department.show', $department) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('department.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @if($isEditMode && $department->image)
        @include('common.fileinput.js', ['name' => 'image', 'image' => $department->image])
    @else
        @include('common.fileinput.js', ['name' => 'image'])
    @endif
@endsection
