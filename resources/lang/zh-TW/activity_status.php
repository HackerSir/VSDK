<?php
return [
    'prepare'       => '準備中',
    'notice'        => '公告中',
    'vote'          => '投票中',
    'wait_counting' => '等待開票',
    'counting'      => '開票中',
    'finish'        => '已結束',
    'error'         => '未知的境界',
];
