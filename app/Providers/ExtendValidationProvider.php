<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ExtendValidationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Imgur 單一圖片網址
        //排除gallery與頁面網址，只允許圖片網址
        Validator::extend('imgur', function ($attribute, $value, $parameters, $validator) {
            $pattern = '/^(?:(?:https?:)?\/\/)?[iw].imgur\.[^\/]*\/([^\?\s\.\/]*)[\w\d]+\.[\w\d]{2,5}$/im';
            preg_match($pattern, $value, $matches);
            if (empty($matches) || count($matches) < 2) {
                return false;
            }

            return true;
        });
        //Youtube 網址
        Validator::extend('youtube', function ($attribute, $value, $parameters, $validator) {
            $pattern = '/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/im';
            preg_match($pattern, $value, $matches);
            if (empty($matches) || count($matches) < 2) {
                return false;
            }

            return true;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
