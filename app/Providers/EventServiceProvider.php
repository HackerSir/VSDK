<?php

namespace App\Providers;

use Auth;
use Hackersir\Activity;
use Hackersir\Ballot;
use Hackersir\Candidate;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Group;
use Hackersir\Helper\JsonHelper;
use Hackersir\WebsiteLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Request;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        /* AuthListener */
        'Illuminate\Auth\Events\Login'  => [
            'App\Listeners\AuthListener@onLogin',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\AuthListener@onLogout',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->addCreateEvent();
        $this->addUpdatedEvent();
        $this->addDeletedEvent();
    }

    protected function addCreateEvent()
    {
        $modelCreatedLog = function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '新增' . $model->modelName,
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        };

        Activity::created($modelCreatedLog);
        Election::created($modelCreatedLog);
        CategoryElectionRelation::created(function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '新增分組到指定選舉',
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        });
        Group::created($modelCreatedLog);
        Candidate::created($modelCreatedLog);
        Ballot::created($modelCreatedLog);
    }

    protected function addUpdatedEvent()
    {
        $modelUpdateLog = function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '編輯' . $model->modelName,
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        };

        Activity::updated($modelUpdateLog);
        Election::updated($modelUpdateLog);
        CategoryElectionRelation::updated(function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '編輯分組資訊',
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        });
        Group::updated($modelUpdateLog);
        Candidate::updated($modelUpdateLog);
        Ballot::updated($modelUpdateLog);
    }

    protected function addDeletedEvent()
    {
        $modelDeleteLog = function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '刪除' . $model->modelName,
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        };

        Activity::deleted($modelDeleteLog);
        Election::deleted($modelDeleteLog);
        CategoryElectionRelation::deleted(function (Model $model) {
            WebsiteLog::create([
                'tag'     => '異動',
                'user_id' => Auth::check() ? Auth::user()->id : null,
                'message' => '刪除選舉底下的分組',
                'data'    => JsonHelper::encode([
                    'ip'   => Request::ip(),
                    'data' => $model,
                ]),
            ]);
        });
        Group::deleted($modelDeleteLog);
        Candidate::deleted($modelDeleteLog);
        Ballot::deleted($modelDeleteLog);
    }
}
