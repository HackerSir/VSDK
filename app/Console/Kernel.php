<?php

namespace App\Console;

use App\Console\Commands\BackupRun;
use App\Jobs\CountingBallot;
use Carbon\Carbon;
use Hackersir\Activity;
use Hackersir\Services\ActivityService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        BackupRun::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            /** @var ActivityService $activityService */
            $activityService = $this->app->make(ActivityService::class);
            $activities = Activity::where('billing_start', '<=', Carbon::now())->get();
            foreach ($activities as $activity) {
                if (!$activityService->isCountingFinish($activity)) {
                    dispatch(new CountingBallot($activity));
                }
            }
        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
