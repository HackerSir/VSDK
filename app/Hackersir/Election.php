<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 選舉
 *
 * @property int $id
 * @property string $name
 * @property string|null $introduction
 * @property string|null $image
 * @property int $activity_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $order
 * @property-read \Hackersir\Activity $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\CategoryElectionRelation[] $categoryElectionRelations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\ElectionVotingStationRelation[] $electionVotingStationRelations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\User[] $staffs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\VotingStation[] $votingStations
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Election onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Election whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Election withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Election withoutTrashed()
 * @mixin \Eloquent
 */
class Election extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'elections';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'introduction',
        'image',
        'activity_id',
        'order',
    ];

    public $modelName = '選舉';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * 分組
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * 選務人員
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staffs()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function votingStations()
    {
        return $this->belongsToMany(VotingStation::class)->withPivot('counting_over', 'video')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function electionVotingStationRelations()
    {
        return $this->hasMany(ElectionVotingStationRelation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryElectionRelations()
    {
        return $this->hasMany(CategoryElectionRelation::class);
    }
}
