<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 活動公告、警告訊息
 *
 * @property int $id
 * @property string $type 類型：success、info、warning、danger
 * @property string $text 內文
 * @property int|null $user_id 發布者ID
 * @property int $activity_id 所屬活動ID
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Hackersir\Activity $activity
 * @property-read string $fa_class
 * @property-read string $name
 * @property-read \Hackersir\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Notification onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Notification whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Notification withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Notification withoutTrashed()
 * @mixin \Eloquent
 */
class Notification extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'notifications';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'type',
        'text',
        'user_id',
        'activity_id',
    ];

    /** @var integer 分頁時的每頁數量 */
    protected $perPage = 50;

    /** @var array 可用類型（配合alert） */
    public static $validTypes = ['success', 'info', 'warning', 'danger'];

    /**
     * 發布者
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 所屬活動
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /**
     * 名稱（公告摘要）
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return str_limit($this->text, 50);
    }

    /**
     * 類型：success、info、warning、danger
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        if (!in_array($this->getOriginal('type'), static::$validTypes)) {
            return 'info';
        }

        return $this->getOriginal('type');
    }

    /**
     * 對應的FontAwesome的Class
     *
     * @return string
     */
    public function getFaClassAttribute()
    {
        switch ($this->type) {
            case 'warning':
                return 'fa-exclamation-triangle';
            case 'danger':
                return 'fa-times-circle';
            case 'success':
                return 'fa-check-circle';
            case 'info':
            default:
                return 'fa-info-circle';
        }
    }
}
