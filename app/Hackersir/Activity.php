<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 活動
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $vote_start
 * @property \Illuminate\Support\Carbon|null $vote_end
 * @property \Illuminate\Support\Carbon|null $billing_start
 * @property string|null $process
 * @property string|null $reporting_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $results_report_link
 * @property bool $publicity
 * @property string|null $count_help_text 開票說明
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Election[] $elections
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Notification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Activity onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereBillingStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereCountHelpText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereProcess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity wherePublicity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereReportingLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereResultsReportLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereVoteEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Activity whereVoteStart($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Activity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Activity withoutTrashed()
 * @mixin \Eloquent
 */
class Activity extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'activities';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'vote_start',
        'vote_end',
        'billing_start',
        'process',
        'reporting_link',
        'results_report_link',
        'publicity',
        'count_help_text',
    ];

    /** @var array $dates 自動轉換為Carbon的屬性 */
    protected $dates = ['vote_start', 'vote_end', 'billing_start'];

    protected $casts = [
        'publicity' => 'boolean',
    ];

    public $modelName = '活動';

    public function elections()
    {
        return $this->hasMany(Election::class)->orderBy('order');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)->orderBy('updated_at');
    }
}
