<?php

namespace Hackersir\Presenters;

use Hackersir\Department;
use Illuminate\Database\Eloquent\Collection;

class DepartmentPresenter
{
    public static function getShortName(Department $department)
    {
        return $department->abbreviation . '系';
    }

    /**
     * 取得簡稱+全名，如：[資工]資訊工程學系
     *
     * @param Department $department
     * @return string
     */
    public function getFullName(Department $department)
    {
        return '[' . $department->abbreviation . ']' . $department->name;
    }

    public function getSelectList(Collection $departments)
    {
        $result = [null => '-- 下拉選擇科系 --'];
        $departments->each(function (Department $item, $key) use (& $result) {
            $result[$item->id] = $this->getFullName($item);
        });
        return $result;
    }
}
