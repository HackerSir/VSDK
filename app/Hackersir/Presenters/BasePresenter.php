<?php namespace Hackersir\Presenters;

use Config;

class BasePresenter
{
    /**
     * 取得完整頁面標題
     *
     * @param string $title 當前頁面標題
     * @return string 完整頁面標題
     */
    public function getPageTitle($title)
    {
        //網站名稱
        $fullTitle = Config::get('site.name');
        //若有頁面標題，在前面追加頁面標題
        $title = trim($title);
        if ($title) {
            $fullTitle = $title . ' - ' . $fullTitle;
        }

        return $fullTitle;
    }
}
