<?php

namespace Hackersir\Presenters;

use Entrust;
use Hackersir\Activity;
use Hackersir\CategoryElectionRelation;
use Hackersir\Notification;

class ActivityPresenter
{
    /** @var CategoryPresenter */
    protected $categoryPresenter;

    /**
     * ActivityPresenter constructor.
     * @param $categoryPresenter
     */
    public function __construct(CategoryPresenter $categoryPresenter)
    {
        $this->categoryPresenter = $categoryPresenter;
    }

    /**
     * 取得投票日的顯示字串，只顯示到年月日+時分
     * 像是 2016年03月22日 08:00 ~ 2016年03月23日 16:00
     * 如果頭尾是同一天，那只顯示 2016年03月22日 08:00 ~ 16:00
     * 如果很不幸，開票時間和結束時間都是同一個時間點，只有差幾秒(怎麼會這樣 O.o)
     * 那只會顯示 2016年03月22日 08:00
     *
     * @param Activity $activity 活動
     * @return string
     */
    public function getVoteDay(Activity $activity)
    {
        $dateFormat = 'Y年m月d日';
        $timeFormat = 'H:i';
        $format = $dateFormat . ' ' . $timeFormat;
        $voteStart = $activity->vote_start;
        $voteEnd = $activity->vote_end;
        $timeRangeText = $voteStart->format($format);
        if (!$voteStart->isSameDay($voteEnd)) {
            $timeRangeText .= ' ~ ' . $voteEnd->format($format);
        } elseif ($voteStart->diffInMinutes($voteEnd) != 0) {
            $timeRangeText .= ' ~ ' . $voteEnd->format($timeFormat);
        }

        return $timeRangeText;
    }

    /**
     * 取得開票時間的顯示字串
     * 如果投票頭尾是同一天，且與開票日同一天
     * 顯示 "當天 18:00"
     * 否則顯示 2016年4月26日 18:00
     *
     * @param Activity $activity 活動
     * @return string
     */
    public function getBillingTime(Activity $activity)
    {
        $format = 'Y年m月d日 H:i';
        if ($activity->vote_start->isSameDay($activity->vote_end)) {
            if ($activity->vote_start->isSameDay($activity->billing_start)) {
                return '當天 ' . $activity->billing_start->format('H:i');
            }
        }

        return $activity->billing_start->format($format);
    }

    public function openItemGroup(Activity $activity)
    {
        if ($activity->publicity) {
            return '<a href="' . route("activity.show", $activity) . '" class="list-group-item">';
        } else {
            if (Entrust::can('activity.view')) {
                return '<a href="'
                    . route("activity.show", $activity) . '" class="list-group-item list-group-item-danger">';
            } else {
                return '<a href="javascript:void(0)" class="list-group-item disabled">';
            }
        }
    }

    public function closeItemGroup()
    {
        return '</a>';
    }

    /**
     * 取得給 Google Chart 用的資訊，票數預設是0
     * @param Activity $activity
     * @param $ballots
     * @return array
     */
    public function getDataTableInfo(Activity $activity, $ballots)
    {
        $info = [];
        $categoryElectionRelation = CategoryElectionRelation
            ::with('category.department', 'election.categories', 'groups.candidates')
            ->whereIn('id', array_keys($ballots['data']))->get();
        foreach ($ballots['data'] as $rid => $ballot) {
            /** @var CategoryElectionRelation $relation */
            $relation = $categoryElectionRelation->find($rid);
            if ($relation != null) {
                $info[$rid] = $this->getRelationDataTable($relation, $ballot);
            }
        }

        uasort($info, function ($a, $b) {
            return strcmp($a['title'], $b['title']);
        });

        return $info;
    }

    /**
     * 取得給 Google Chart DataTable 使用的資料
     * Ex:
     * [
     *  'title' => '',
     *  'gid' => [7, 8],
     *  'data' => [
     *    ['候選人', '同意', "{type: 'number', role: 'annotation'}"}, '反對', "{type: 'number', role: 'annotation'}"],
     *    ['伊莉雅\n美游', 0, 0, 0, 0],
     *    ['王小明\n陳大明', 0, 0, 0, 0],
     *   ],
     *  'invalid' => 0
     * ]
     * OR:
     * [
     *  'title' => '',
     *  'gid' => [9, 10],
     *  'data' => [
     *    ['候選人', '票數', "{type: 'number', role: 'annotation'}"],
     *    ['依莉雅\n美游', 0, 0],
     *    ['王小明\n陳大明', 0, 0],
     *   ],
     *  'invalid' => 0
     * ]
     *
     *
     * @param CategoryElectionRelation $relation
     * @param $ballot
     * @return array
     */
    protected function getRelationDataTable(CategoryElectionRelation $relation, $ballot)
    {
        $annotation = ['type' => 'number', 'role' => 'annotation'];

        $result = [];
        //------------------------------------
        $result['title'] = $this->getRelationTitle($relation);
        //------------------------------------
        // 假設每一組的資料型態都一樣
        $ballotType = is_array($ballot['data'][current(array_keys($ballot['data']))]) ? 'agree_and_disagree' : 'simple';

        if ($ballotType == 'agree_and_disagree') {
            $data[] = ['候選人', '同意', $annotation, '反對', $annotation];
        } else {
            $data[] = ['候選人', '得票', $annotation];
        }

        $groups = $relation->groups;
        $result['gid'] = $groups->pluck('id')->toArray();

        foreach ($groups as $group) {
            // Name
            $name = implode("\n", $group->candidates->pluck('name')->toArray());

            $ballotData = $ballot['data'][$group->id];
            if ($ballotType == 'agree_and_disagree') {
                $data[] = [
                    $name,
                    $ballotData['agree'],
                    $ballotData['agree'],
                    $ballotData['disagree'],
                    $ballotData['disagree'],
                ];
            } else {
                $data[] = [$name, $ballotData, $ballotData];
            }
        }
        $result['data'] = $data;
        //------------------------------------
        $result['invalid'] = $ballot['invalid'];

        //------------------------------------
        return $result;
    }

    protected function getRelationTitle(CategoryElectionRelation $relation)
    {
        $result = $relation->election->name;
        if ($relation->election->categories->count() > 1) {
            $result .= ' - ' . $this->categoryPresenter->getTitleName($relation->category);
        }

        if ($relation->elected_group_amount > 1) {
            $result .= '(應選' . $relation->elected_group_amount . '人)';
        }

        return $result;
    }

    public function getSelectList()
    {
        $activities = Activity::all();
        $result = [null => '-- 下拉選擇活動 --'];
        $activities->each(function (Activity $item, $key) use (& $result) {
            $result[$item->id] = $item->name;
        });

        return $result;
    }

    public function getTypeSelectList()
    {
        $result = [null => '-- 下拉選擇類型 --'];
        foreach (Notification::$validTypes as $type) {
            $result[$type] = $type;
        }

        return $result;
    }
}
