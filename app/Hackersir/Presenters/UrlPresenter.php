<?php

namespace Hackersir\Presenters;

/**
 * 處理候選人的前端顯示
 * @package Hackersir\Presenters\
 */
class UrlPresenter
{
    public function addUTM($url, $options = [])
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            return $url;
        }
        //相關參數
        $options = array_only($options, ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content']);
        $utmQuery = http_build_query($options);
        if (empty($utmQuery)) {
            return $url;
        }

        //在網址後追加UTM
        if (!str_contains($url, '?')) {
            $url .= '?' . $utmQuery;
        } else {
            $url .= '&' . $utmQuery;
        }

        return $url;
    }
}
