<?php

namespace Hackersir\Presenters;

use Hackersir\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryPresenter
{
    public function getTitleName(Category $category)
    {
        if ($category->department == null) {
            return $category->name;
        }

        return DepartmentPresenter::getShortName($category->department);
    }

    public function getFullName(Category $category)
    {
        if ($category->department == null) {
            return $category->name;
        }

        return $category->department->name;
    }

    /**
     * 提供給開票管理用
     * 顯示全稱，如果沒有分組沒有科系，則回傳空字串
     * @param Category $category
     * @return string
     */
    public function getRemarkFullName(Category $category)
    {
        if ($category->department == null) {
            return "";
        }

        return $category->department->name;
    }

    public function getSelectList(Collection $categories)
    {
        $result = [null => '-- 下拉選擇學院 --'];
        $categories->each(function (Category $item, $key) use (& $result) {
            $result[$item->id] = $this->getFullName($item);
        });

        return $result;
    }
}
