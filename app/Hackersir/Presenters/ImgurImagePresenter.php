<?php

namespace Hackersir\Presenters;

use Hackersir\Helper\ImgurHelper;

class ImgurImagePresenter
{
    /**
     * @param string $url
     * @param string $size
     * @return string
     */
    public function getSafeURL($url, $size = '500x500')
    {
        if (empty($url)) {
            return 'https://placehold.it/' . $size;
        }

        return ImgurHelper::thumbnail($url, 'l');
    }
}
