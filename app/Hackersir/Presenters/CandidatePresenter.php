<?php

namespace Hackersir\Presenters;

use Exception;
use Hackersir\Department;

/**
 * 處理候選人的前端顯示
 * @package Hackersir\Presenters\
 */
class CandidatePresenter
{
    const CHINESE_NUMBER = [1 => '一', '二', '三', '四', '五', '六', '七'];
    const CLASS_NAME = [1 => '甲', '乙', '丙', '丁'];

    /**
     * 取得系級，例如：資訊二乙
     * 如果沒有指定班級就不會顯示，如：中文一
     * @param Department $department
     * @param int $grade
     * @param int $class_no
     * @return string
     * @throws Exception
     */
    public function getDepartmentGradeClass(Department $department, $grade, $class_no)
    {
        if (!(1 <= $grade && $grade <= 7)) {
            throw new Exception("grade must be in 1 ~ 7, grade = $grade.");
        }
        if (!(0 <= $class_no && $class_no <= 4)) {
            throw new Exception("class_no must be in 0 ~ 4, class_no = $class_no.");
        }
        $result = $department->abbreviation;
        $result .= self::CHINESE_NUMBER[$grade];

        if ($class_no > 0) {
            $result .= self::CLASS_NAME[$class_no];
        }

        return $result;
    }

    public function getGradeSelectList()
    {
        $grades = [null => '-- 下拉選擇年級 --'];
        foreach (static::CHINESE_NUMBER as $key => $chineseNumber) {
            $grades[$key] = '大' . $chineseNumber;
        }

        return $grades;
    }

    public function getClassSelectList()
    {
        $class = [null => '-- 下拉選擇班級 --'];
        $class[0] = '無';
        foreach (static::CLASS_NAME as $key => $className) {
            $class[$key] = $className . '班';
        }

        return $class;
    }
}
