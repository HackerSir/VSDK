<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;

/**
 * 投票所
 *
 * @property int $id
 * @property string $name 名稱
 * @property string $location 地點
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Election[] $elections
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\VotingStation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VotingStation extends Model
{
    /** @var string $table 資料表 */
    protected $table = 'voting_stations';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'location',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function elections()
    {
        return $this->belongsToMany(Election::class)->withPivot('counting_over');
    }
}
