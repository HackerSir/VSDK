<?php

namespace Hackersir\Helper;

class JsonHelper
{
    /**
     * 檢查字串是否為JSON
     *
     * @param $string
     * @return bool
     */
    public static function isJson($string)
    {
        if (!starts_with($string, '[') && !starts_with($string, '{')) {
            return false;
        }
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * 將物件編碼為JSON
     *
     * @param $value
     * @param int $options
     * @param int $depth
     * @return string
     */
    public static function encode($value, $options = 0, $depth = 512)
    {
        $options |= JSON_UNESCAPED_UNICODE;     //不跳脫Unicode字元
        $options |= JSON_UNESCAPED_SLASHES;     //不跳脫斜線
        return json_encode($value, $options, $depth);
    }

    /**
     * 將JSON解碼為物件
     *
     * @param string $json
     * @param bool $assoc
     * @param int $depth
     * @param int $options
     * @return mixed
     */
    public static function decode($json, $assoc = false, $depth = 512, $options = 0)
    {
        return json_decode($json, $assoc, $depth, $options);
    }
}
