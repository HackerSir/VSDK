<?php

namespace Hackersir\Helper;

class UrlHelper
{
    /**
     * 將網址轉為https
     *
     * @param string $url
     * @return null
     */
    public static function secure($url)
    {
        //不處理空字串
        if (empty(trim($url))) {
            return $url;
        }
        //FIXME: 指定http與https以外的協定會出錯，需要更好的正規替換方式
        $pattern = '/^(?:(?:https?:)?\/\/)?(.+)$/im';
        $url = preg_replace($pattern, 'https://$1', $url);
        return $url;
    }
}
