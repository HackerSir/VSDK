<?php

namespace Hackersir\Helper;

class YoutubeHelper
{
    /**
     * 取得Imgur ID
     *
     * @param string $url
     * @return null
     */
    public static function getYoutubeID($url)
    {
        $pattern = '/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/im';
        preg_match($pattern, $url, $matches);
        if (empty($matches) || count($matches) < 2) {
            return null;
        }
        return $matches[1];
    }

    /**
     * 將Youtube網址轉為一般型態
     *
     * @param $url
     * @return mixed
     */
    public static function transformUrl($url)
    {
        if (!self::getYoutubeID($url)) {
            return $url;
        }
        return 'https://www.youtube.com/watch?v=' . self::getYoutubeID($url);
    }

    /**
     * 將Youtube網址轉為簡短型態
     *
     * @param $url
     * @return mixed
     */
    public static function transformShortUrl($url)
    {
        if (!self::getYoutubeID($url)) {
            return $url;
        }
        return 'https://youtu.be/' . self::getYoutubeID($url);
    }

    /**
     * 將Youtube網址轉為嵌入型態
     *
     * @param $url
     * @return mixed
     */
    public static function transformEmbedUrl($url)
    {
        if (!self::getYoutubeID($url)) {
            return $url;
        }
        return 'https://www.youtube.com/embed/' . self::getYoutubeID($url);
    }
}
