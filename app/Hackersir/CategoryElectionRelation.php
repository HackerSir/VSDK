<?php

namespace Hackersir;

use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;

/**
 * 選舉(Election)與分組(Category)的關係
 *
 * @property int $id
 * @property int $category_id
 * @property int $election_id
 * @property int $voting_eligible_population
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $elected_group_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Ballot[] $ballots
 * @property-read \Hackersir\Category $category
 * @property-read \Hackersir\Election $election
 * @property-read bool $can_vote_objection
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Group[] $groups
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation findRelation(\Hackersir\Category $category, \Hackersir\Election $election)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereElectedGroupAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereElectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\CategoryElectionRelation whereVotingEligiblePopulation($value)
 * @mixin \Eloquent
 */
class CategoryElectionRelation extends Model
{
    /** @var string $table 資料表 */
    protected $table = 'category_election';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'category_id',
        'election_id',
        'voting_eligible_population',
        'elected_group_amount',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function election()
    {
        return $this->belongsTo(Election::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(Group::class, 'category_election_id')->orderBy('number');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ballots()
    {
        return $this->hasMany(Ballot::class, 'category_election_id');
    }

    /**
     * 能否投反對票？（僅有應選組數少於參選組數時不能）
     *
     * @return bool
     */
    public function getCanVoteObjectionAttribute()
    {
        return $this->elected_group_amount >= $this->groups->count();
    }

    /**
     * query出分組與選舉的關係
     *
     * @param QueryBuilder $query
     * @param Category $category
     * @param Election $election
     * @return mixed
     */
    public function scopeFindRelation($query, Category $category, Election $election)
    {
        return $query->where('category_id', '=', $category->id)
            ->where('election_id', '=', $election->id);
    }
}
