<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 候選人
 *
 * @property int $id
 * @property string|null $photo_url
 * @property string $name
 * @property string $gender
 * @property int $age
 * @property int $grade
 * @property int $class_no
 * @property string|null $experience
 * @property int $group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $department_id
 * @property int $order
 * @property string|null $title
 * @property-read \Hackersir\Department $department
 * @property-read \Hackersir\Group $group
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Candidate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereClassNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate wherePhotoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Candidate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Candidate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Candidate withoutTrashed()
 * @mixin \Eloquent
 */
class Candidate extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'candidates';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'photo_url',
        'name',
        'gender',
        'age',
        'grade',
        'class_no',
        'experience',
        'group_id',
        'department_id',
        'order',
        'title',
    ];

    public $modelName = '候選人';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
