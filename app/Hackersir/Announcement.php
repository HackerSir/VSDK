<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 公告
 *
 * @property int $id
 * @property string $title
 * @property string $message
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Hackersir\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Announcement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Announcement whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Announcement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Announcement withoutTrashed()
 * @mixin \Eloquent
 */
class Announcement extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'announcements';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'title',
        'message',
        'user_id',
    ];

    /** @var integer 分頁時的每頁數量 */
    protected $perPage = 20;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
