<?php

namespace Hackersir;

use Hackersir\Helper\MarkdownHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * 設定
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $desc
 * @property string $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'id',
        'name',
        'type',
        'desc',
        'data',
    ];
    /** @static array $types 有效型態與對應簡介 */
    public static $types = [
        'text'      => '單行文字',
        'multiline' => '多行文字',
        'markdown'  => 'Markdown多行文字',
        'boolean'   => '布林值',
    ];

    /**
     * 取得設定值
     *
     * @param $name
     * @param $default
     * @return null
     */
    public static function getItem($name, $default = null)
    {
        /** @var Setting $setting */
        $setting = self::where('name', $name)->first();
        if (!$setting) {
            //找不到設定時，回傳預設值，無預設值則回傳空值
            return ($default) ?: null;
        }

        return $setting->getData();
    }

    /**
     * 取得設定值（原始內容）
     *
     * @param $name
     * @return null
     */
    public static function getRaw($name)
    {
        /** @var Setting $setting */
        $setting = self::where('name', $name)->first();
        if (!$setting) {
            return null;
        }

        return $setting->data;
    }

    /**
     * 更新設定
     *
     * @param $name
     * @param $data
     * @return null
     */
    public static function set($name, $data)
    {
        /** @var Setting $setting */
        $setting = self::where('name', $name)->first();
        if (!$setting) {
            return null;
        }
        $setting->data = $data;
        $setting->save();

        return null;
    }

    /**
     * 取得型態
     *
     * @return string
     */
    public function getType()
    {
        //檢查是否為有效型態
        if (!in_array($this->type, array_keys(static::$types))) {
            //若不是，則自動選擇第一個型態
            return head(array_keys(static::$types));
        }

        return $this->type;
    }

    /**
     * 取得型態簡介文字
     *
     * @return string
     */
    public function getTypeDesc()
    {
        return static::$types[$this->getType()];
    }

    /**
     * 取得資料
     *
     * @return string
     */
    public function getData()
    {
        //依照型態進行不同處理
        if ($this->getType() == 'text') {
            return htmlspecialchars($this->data);
        }
        if ($this->getType() == 'multiline') {
            return nl2br(htmlspecialchars($this->data));
        }
        if ($this->getType() == 'markdown') {
            return MarkdownHelper::translate($this->data);
        }
        if ($this->getType() == 'boolean') {
            $bool = filter_var($this->data, FILTER_VALIDATE_BOOLEAN);

            return $bool;
        }

        //無效型態
        return 'Invalid Setting Type';
    }

    /**
     * 取得欄位型態（讓X-editable識別）
     *
     * @return string
     */
    public function getHtmlFieldType()
    {
        //依照型態進行不同處理
        if ($this->getType() == 'text') {
            return 'text';
        }
        if ($this->getType() == 'multiline') {
            return 'textarea';
        }
        if ($this->getType() == 'markdown') {
            //FIXME 須使用特殊形態
            return 'textarea';
        }
        if ($this->getType() == 'boolean') {
            return 'select';
        }

        //無效型態
        return 'Invalid Setting Type';
    }
}
