<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 學院
 *
 * @property int $id
 * @property string $name 名稱
 * @property string $abbreviation 簡稱
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Department[] $departments
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\College onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\College whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\College withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\College withoutTrashed()
 * @mixin \Eloquent
 */
class College extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'colleges';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'abbreviation',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function departments()
    {
        return $this->hasMany(Department::class);
    }
}
