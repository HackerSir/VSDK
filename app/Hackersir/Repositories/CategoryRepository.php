<?php

namespace Hackersir\Repositories;

use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;

class CategoryRepository
{
    protected $category;

    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getGroupsByElection(Category $category, Election $election)
    {
        $relation = CategoryElectionRelation::findRelation($category, $election)->first();

        if ($relation == null) {
            return null;
        }

        return $relation->groups;
    }
}
