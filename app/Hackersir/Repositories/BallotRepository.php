<?php

namespace Hackersir\Repositories;

use DB;
use Hackersir\Ballot;
use Hackersir\CategoryElectionRelation;
use Hackersir\Group;
use Hackersir\VotingStation;
use Illuminate\Http\Request;

/**
 * 處理 Ballot 的資料庫邏輯
 * @package Hackersir\Repositories
 */
class BallotRepository
{
    /**
     * 儲存有效票
     * @param Request $request
     * @param int $type 有效票 或 有效票(反對)
     */
    public function storeValid(Request $request, $type)
    {
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        $group = Group::find($request->input('group'));
        Ballot::create([
            'type'                 => $type,
            'voting_station_id'    => $votingStation->id,
            'group_id'             => $group->id,
            'category_election_id' => $categoryElectionRelation->id,
        ]);
    }

    /**
     * 儲存廢票
     * @param Request $request
     */
    public function storeInvalid(Request $request)
    {
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        $type = Ballot::INVALID;
        Ballot::create([
            'type'                 => $type,
            'voting_station_id'    => $votingStation->id,
            'category_election_id' => $categoryElectionRelation->id,
        ]);
    }

    /**
     * 刪除有效票
     * @param Request $request
     * @param int $type 有效票 或 有效票(反對)
     * @throws \Exception
     */
    public function destroyValid(Request $request, $type)
    {
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        $group = Group::find($request->input('group'));
        $ballot = Ballot::where('type', $type)
            ->where('voting_station_id', $votingStation->id)
            ->where('group_id', $group->id)
            ->where('category_election_id', $categoryElectionRelation->id)
            ->orderBy('id', 'desc')->first();
        if ($ballot) {
            $ballot->delete();
        }
    }

    /**
     * 刪除廢票
     * @param Request $request
     * @throws \Exception
     */
    public function destroyInvalid(Request $request)
    {
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        $type = Ballot::INVALID;
        $ballot = Ballot::where('type', $type)
            ->where('voting_station_id', $votingStation->id)
            ->where('category_election_id', $categoryElectionRelation->id)
            ->orderBy('id', 'desc')->first();
        if ($ballot) {
            $ballot->delete();
        }
    }

    /**
     * 回傳指定(選舉+分組)的選票
     * 可指定投票所，不指定則為全部
     *
     * 單組：同意、反對、廢票
     * 多組：第一組同意、第二組同意、...、廢票
     * 多組：第一組同意/反對、第二組同意/反對、...、廢票
     *
     * @param CategoryElectionRelation $relation
     * @param VotingStation $votingStation
     * @return array
     */
    public function countBallot(CategoryElectionRelation $relation, VotingStation $votingStation = null)
    {
        $groups = $relation->groups;
        if (!$votingStation) {
            //全部投票所
            $ballotQuery = $relation->ballots();
        } else {
            //指定投票所
            $ballotQuery = $relation->ballots()->where('voting_station_id', '=', $votingStation->id);
        }

        //票數計算
        //有效票
        $countCollection = (clone $ballotQuery)->groupBy('group_id', 'type')
            ->select('group_id', 'type', DB::raw('count(*) as count'))->get()->toArray();
        $countArray = [];
        foreach ($countCollection as $groupCount) {
            $groupId = $groupCount['group_id'];
            $type = $groupCount['type'];
            $count = $groupCount['count'];
            array_set($countArray, $groupId . '.' . $type, $count);
        }
        $result = [];

        $data = [];
        foreach ($groups as $group) {
            $agreeCount = array_get($countArray, $group->id . '.' . Ballot::VALID, 0);
            if ($relation->can_vote_objection) {
                $disagreeCount = array_get($countArray, $group->id . '.' . Ballot::VALID_OBJECTION, 0);
                $tempBallot = [
                    'agree'    => $agreeCount,
                    'disagree' => $disagreeCount,
                ];
            } else {
                $tempBallot = $agreeCount;
            }
            $data[$group->id] = $tempBallot;
        }
        $result['data'] = $data;

        //無效票
        $invalidBallotCount = (clone $ballotQuery)->where('type', Ballot::INVALID)->count();
        $result['invalid'] = $invalidBallotCount;

        return $result;
    }

    public function getEmptyBallot(CategoryElectionRelation $relation)
    {
        $groups = $relation->groups;

        $data = [];
        foreach ($groups as $group) {
            if ($relation->can_vote_objection) {
                $tempBallot = [
                    'agree'    => 0,
                    'disagree' => 0,
                ];
            } else {
                $tempBallot = 0;
            }
            $data[$group->id] = $tempBallot;
        }

        $result = [];
        $result['data'] = $data;
        $result['invalid'] = 0;

        return $result;
    }
}
