<?php

namespace Hackersir\Repositories;

use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;

class CategoryElectionRelationRepository
{
    public function create(Category $category, Election $election, $electedGroupAmount)
    {
        // 檢查選舉跟分組是否有建立過
        $relation = CategoryElectionRelation::findRelation($category, $election)->get();
        if (!$relation->isEmpty()) {
            return ['relation repeat'];
        }

        CategoryElectionRelation::create([
            'category_id'          => $category->id,
            'election_id'          => $election->id,
            'elected_group_amount' => $electedGroupAmount,
        ]);

        return [];
    }
}
