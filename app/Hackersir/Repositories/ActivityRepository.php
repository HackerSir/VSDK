<?php

namespace Hackersir\Repositories;

use Hackersir\Activity;
use Hackersir\Utils\ActivityStatus;
use Illuminate\Http\Request;

class ActivityRepository
{
    /**
     * @param Request $request
     * @param Activity $activity
     * @param string $status
     */
    public function update(Request $request, Activity $activity, $status)
    {
        $updateData = [
            'count_help_text' => $request->input('count_help_text'),
        ];

        if (!ActivityStatus::isVoteBegin($status)) {
            $updateData = array_merge($updateData, [
                'process'        => $request->input('process'),
                'reporting_link' => $request->input('reporting_link'),
            ]);
        }

        if (ActivityStatus::isFinish($status)) {
            $updateData = array_merge($updateData, [
                'results_report_link' => $request->input('results_report_link'),
            ]);
        }
        $activity->update($updateData);
    }

    /**
     * @param Request $request
     * @return Activity
     */
    public function store(Request $request)
    {
        $activity = Activity::create([
            'name'           => $request->input('name'),
            'vote_start'     => $request->input('vote_start'),
            'vote_end'       => $request->input('vote_end'),
            'billing_start'  => $request->input('billing_start'),
            'process'        => $request->input('process'),
            'reporting_link' => $request->input('reporting_link'),
        ]);

        return $activity;
    }

    /**
     * 取得要顯示在首頁的 Activity
     * @return Activity
     */
    public function getHomeActivity()
    {
        $activity = Activity::where('publicity', '=', true)
            ->orderBy('vote_start', 'desc')
            ->first();

        return $activity;
    }
}
