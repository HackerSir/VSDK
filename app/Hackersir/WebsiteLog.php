<?php

namespace Hackersir;

use Hackersir\Helper\JsonHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * WebsiteLog
 *
 * @property int $id
 * @property string $tag
 * @property int|null $user_id
 * @property string $message
 * @property string $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read mixed $json
 * @property-read \Hackersir\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\WebsiteLog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\WebsiteLog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\WebsiteLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\WebsiteLog withoutTrashed()
 * @mixin \Eloquent
 */
class WebsiteLog extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'website_logs';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'tag',
        'user_id',
        'message',
        'data',
    ];

    /** @var integer 分頁時的每頁數量 */
    protected $perPage = 50;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 取得data的json物件
     *
     * @return mixed
     */
    public function getJsonAttribute()
    {
        return JsonHelper::decode($this->data);
    }
}
