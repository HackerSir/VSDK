<?php

namespace Hackersir;

use Laratrust\Models\LaratrustPermission;

/**
 * 權限
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends LaratrustPermission
{
    /**
     * @param $roleName
     *
     * @return bool
     */
    public function hasRole($roleName)
    {
        foreach ($this->roles as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }

        return false;
    }
}
