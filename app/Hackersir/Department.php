<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 系所
 *
 * @property int $id
 * @property string $name 名稱
 * @property string $abbreviation 簡稱
 * @property int $college_id
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Candidate[] $candidates
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Category[] $categories
 * @property-read \Hackersir\College $college
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Department onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereCollegeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Department whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Department withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Department withoutTrashed()
 * @mixin \Eloquent
 */
class Department extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'departments';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'abbreviation',
        'college_id',
        'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function college()
    {
        return $this->belongsTo(College::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }
}
