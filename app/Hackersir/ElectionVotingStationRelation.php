<?php

namespace Hackersir;

use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 選舉 與 投票所 的關係
 *
 * @property int $id
 * @property int $election_id
 * @property int $voting_station_id
 * @property int $counting_over
 * @property string|null $video
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Hackersir\Election $election
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\User[] $users
 * @property-read \Hackersir\VotingStation $votingStation
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation findRelation(\Hackersir\Election $election, \Hackersir\VotingStation $votingStation)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ElectionVotingStationRelation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereCountingOver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereElectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ElectionVotingStationRelation whereVotingStationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ElectionVotingStationRelation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ElectionVotingStationRelation withoutTrashed()
 * @mixin \Eloquent
 */
class ElectionVotingStationRelation extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'election_voting_station';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'election_id',
        'voting_station_id',
        'counting_over',
        'video',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function election()
    {
        return $this->belongsTo(Election::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function votingStation()
    {
        return $this->belongsTo(VotingStation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }


    /**
     * 檢查是否為該選舉在該開票所之開票員
     *
     * @param User $user
     * @return boolean
     */
    public function isCountingStaff(User $user)
    {
        //無效成員
        if (!$user) {
            return false;
        }

        return $this->users->contains($user);
    }

    /**
     * query出選舉與投票所的關係
     *
     * @param QueryBuilder $query
     * @param Election $election
     * @param VotingStation $votingStation
     * @return mixed
     */
    public function scopeFindRelation($query, Election $election, VotingStation $votingStation)
    {
        return $query->where('election_id', '=', $election->id)
            ->where('voting_station_id', '=', $votingStation->id);
    }
}
