<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 候選人組分組
 *
 * @property int $id
 * @property string|null $name 組別名稱
 * @property int|null $department_id
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\CategoryElectionRelation[] $categoryElectionRelations
 * @property-read \Hackersir\Department|null $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Election[] $elections
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Category withoutTrashed()
 * @mixin \Eloquent
 */
class Category extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'categories';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'department_id',
        'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function elections()
    {
        return $this->belongsToMany(Election::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryElectionRelations()
    {
        return $this->hasMany(CategoryElectionRelation::class);
    }
}
