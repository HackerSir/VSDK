<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imgur;

/**
 * ImgurImage
 *
 * @property int $id
 * @property string $imgur_id
 * @property string $file_name 完整原始檔名
 * @property string $extension 副檔名
 * @property string $delete_hash
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read string $url
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ImgurImage onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereDeleteHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereImgurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\ImgurImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ImgurImage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\ImgurImage withoutTrashed()
 * @mixin \Eloquent
 */
class ImgurImage extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'imgur_images';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'imgur_id',
        'file_name',
        'extension',
        'delete_hash',
    ];

    /**
     * 取得圖片網址
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return 'https://i.imgur.com/' . $this->imgur_id . '.' . $this->extension;
    }

    public function delete()
    {
        //透過Delete Hash實際刪除在imgur的圖片
//        $client = App::make('Imgur\Client');
//        $client->api('image')->deleteImage($this->delete_hash);
        Imgur::delete($this->delete_hash);

        //刪除
        return parent::delete();
    }
}
