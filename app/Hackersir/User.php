<?php

namespace Hackersir;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

/**
 * 使用者
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $confirm_code
 * @property \Illuminate\Support\Carbon|null $confirm_at
 * @property \Illuminate\Support\Carbon|null $register_at 註冊時間
 * @property string|null $register_ip 註冊IP
 * @property \Illuminate\Support\Carbon|null $last_login_at 最後登入時間
 * @property string|null $last_login_ip 最後登入IP
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\ElectionVotingStationRelation[] $electionVotingStationRelations
 * @property-read bool $is_confirmed
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereConfirmAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereConfirmCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User wherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereRegisterAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereRegisterIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereRoleIs($role = '')
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use LaratrustUserTrait, Notifiable;

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'name',
        'email',
        'password',
        'confirm_code',
        'confirm_at',
        'register_at',
        'register_ip',
        'last_login_at',
        'last_login_ip',
    ];

    /** @var array $hidden 於JSON隱藏的屬性 */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /** @var array $dates 自動轉換為Carbon的屬性 */
    protected $dates = [
        'confirm_at',
        'register_at',
        'last_login_at',
    ];

    /** @var integer 分頁時的每頁數量 */
    protected $perPage = 50;

    /**
     * 帳號是否完成驗證
     *
     * @return boolean
     */
    public function getIsConfirmedAttribute()
    {
        return !empty($this->confirm_at);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function electionVotingStationRelations()
    {
        return $this->belongsToMany(ElectionVotingStationRelation::class);
    }
}
