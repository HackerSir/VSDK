<?php

namespace Hackersir;

use Laratrust\Models\LaratrustRole;

/**
 * 角色
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Permission[] $permissions
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends LaratrustRole
{
}
