<?php

namespace Hackersir\Services;

use Cache;
use Carbon\Carbon;
use Entrust;
use Hackersir\Activity;
use Hackersir\Helper\YoutubeHelper;
use Hackersir\Repositories\BallotRepository;
use Hackersir\Utils\ActivityStatus;
use Hackersir\VotingStation;
use Illuminate\Database\Eloquent\Collection;

class ActivityService
{
    /** @var BallotRepository */
    protected $ballotRepository;
    /** @var ElectionService */
    protected $electionService;

    public function __construct(ElectionService $electionService, BallotRepository $ballotRepository)
    {
        $this->electionService = $electionService;
        $this->ballotRepository = $ballotRepository;
    }

    /**
     * @param Activity $activity
     * @return string
     */
    public function getStatus(Activity $activity)
    {
        if (!$activity->publicity) {
            return ActivityStatus::PREPARE;
        }

        $now = Carbon::now();
        if ($now->lt($activity->vote_start)) {
            return ActivityStatus::NOTICE;
        } elseif ($now->lte($activity->vote_end)) {
            return ActivityStatus::VOTE;
        }

        if ($now->lt($activity->billing_start)) {
            return ActivityStatus::WAIT_COUNTING;
        }

        if ($this->isCountingFinish($activity)) {
            return ActivityStatus::FINISH;
        }

        return ActivityStatus::COUNTING;
    }

    public function isCountingFinish(Activity $activity)
    {
        //檢查底下的選舉
        foreach ($activity->elections as $election) {
            if (!($this->electionService->isCountingFinish($election))) {
                return false;
            }
        }

        return true;
    }

    /**
     * 檢查使用者是否可以瀏覽活動
     * @param Activity $activity
     * @return bool
     */
    public function canView(Activity $activity)
    {
        return $activity->publicity || Entrust::can('activity.view');
    }

    /**
     * 取得活動的投票所集合
     *
     * @param Activity $activity
     * @return Collection
     */
    public function getVotingStations(Activity $activity)
    {
        //FIXME: $votingStations或許有更好的取得方式
        $votingStations = new Collection();
        foreach ($activity->elections as $election) {
            foreach ($election->votingStations as $votingStation) {
                if ($votingStations->contains($votingStation)) {
                    continue;
                }
                $votingStations->add($votingStation);
            }
        }
        //依照ID排序
        $votingStations = $votingStations->sortBy('id');

        return $votingStations;
    }

    /**
     * @param Activity $activity
     * @param bool $doCalc
     * @return array
     */
    public function getBallotCount(Activity $activity, $doCalc = false)
    {
        $status = $this->getStatus($activity);
        if ($status == ActivityStatus::FINISH) {
            //緩存名稱
            $cacheName = 'BallotCount_' . $activity->id;
            //緩存時間
            $cacheMinutes = 60;
            $info = Cache::remember($cacheName, $cacheMinutes, function () use ($activity) {
                $newInfo['data'] = $this->getBallotCountImpl($activity, true);
                $newInfo['updated_at'] = Carbon::now()->timestamp;

                return $newInfo;
            });

            return $info;
        } else {
            $info['data'] = $this->getBallotCountImpl($activity, $doCalc);
            $info['updated_at'] = Carbon::now()->timestamp;

            return $info;
        }
    }

    protected function getBallotCountImpl(Activity $activity, $doCalc = false)
    {
        // TODO: 紀錄計算時間 ?
        $info = [];
        $elections = $activity->elections;
        //暫時不對 categoryElectionRelations.ballots 進行 eager loading
        //以免過大量選票資料導致 PHP Fatal error:  Allowed memory size of 134217728 bytes exhausted
        $elections->load('categoryElectionRelations.groups');

        foreach ($elections as $election) {
            $result = [];
            foreach ($election->categoryElectionRelations as $relation) {
                if ($doCalc) {
                    $result[$relation->id] = $this->ballotRepository->countBallot($relation);
                } else {
                    $result[$relation->id] = $this->ballotRepository->getEmptyBallot($relation);
                }
            }
            $info += $result;
        }

        return $info;
    }

    /**
     * 取得各投票所之直播影片清單
     * [
     *      ID => [
     *                  'name' => 投票所名稱,
     *                  'link' => Youtube影片連結（https://youtu.be/影片編號）,
     *                  'embed' => Youtube影片嵌入網址（https://www.youtube.com/embed/影片編號）,
     *            ],
     *      ...
     * ]
     *
     * @param Activity $activity
     * @return array
     */
    public function getVideoList(Activity $activity)
    {
        /** @var VotingStation[] $votingStations */
        $votingStations = $this->getVotingStations($activity);
        $videoList = [];
        foreach ($votingStations as $votingStation) {
            //忽略無直播影片之投票所
            if (empty($votingStation->pivot->video)) {
                continue;
            }
            $temp = [];
            $temp['name'] = $votingStation->name;
            $temp['link'] = $votingStation->pivot->video;
            $temp['embed'] = YoutubeHelper::transformEmbedUrl($votingStation->pivot->video);
            $videoList[$votingStation->id] = $temp;
        }

        return $videoList;
    }
}
