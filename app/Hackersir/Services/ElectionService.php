<?php

namespace Hackersir\Services;

use Auth;
use Hackersir\Election;
use Hackersir\User;

class ElectionService
{
    public function isCountingFinish(Election $election)
    {
        foreach ($election->votingStations as $votingStation) {
            //如果有一個投票所還沒開完票，代表沒開完票。
            if (!((bool) $votingStation->pivot->counting_over)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 檢查是否為該選舉之選務人員
     *
     * @param Election $election
     * @param User $user
     * @return bool
     */
    public function isStaff(Election $election, User $user = null)
    {
        //未指定使用者，檢查當前使用者
        if (!$user) {
            $user = Auth::user();
        }

        return $election->staffs->contains($user);
    }
}
