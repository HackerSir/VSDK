<?php

namespace Hackersir\Services;

use App\Mail\ConfirmMail;
use App\Mail\PlainTextMail;
use Exception;
use Hackersir\User;
use Log;
use Mail;

class MailService
{
    /**
     * 發送純文字信件
     *
     * @param string $mailTo
     * @param string $mailToName
     * @param string $text
     * @param string $subject
     * @return bool
     */
    public function sendPlainMail($mailTo, $mailToName, $text, $subject = '')
    {
        try {
            Mail::send(new PlainTextMail([
                'to'      => [
                    'address' => $mailTo,
                    'name'    => $mailToName,
                ],
                'text'    => $text,
                'subject' => $subject,
            ]));
        } catch (Exception $e) {
            Log::error('[MailSendFailed] 無法發送純文字信到 ' . $mailTo);

            return false;
        }

        return true;
    }

    /**
     * @param string $mailTo
     * @param string $mailToName
     * @param string $text
     * @param string $subject
     * @return bool
     */
    public function queuePlainMail($mailTo, $mailToName, $text, $subject = '')
    {
        Mail::queue(new PlainTextMail([
            'to'      => [
                'address' => $mailTo,
                'name'    => $mailToName,
            ],
            'text'    => $text,
            'subject' => $subject,
        ]));

        return true;
    }

    /**
     * 發送信箱驗證信件
     *
     * @param User $user
     */
    public function queueEmailConfirmation(User $user)
    {
        Mail::queue(new ConfirmMail($user));
    }
}
