<?php

namespace Hackersir\Utils;

/**
 * 表示活動的狀態
 * @package Hackersir\lib
 */
class ActivityStatus
{
    /** @const 準備中 */
    const PREPARE = 'prepare';
    /** @const 公告中 */
    const NOTICE = 'notice';
    /** @const 投票中 */
    const VOTE = 'vote';
    /** @const 等待開票 */
    const WAIT_COUNTING = 'wait_counting';
    /** @const 開票中 */
    const COUNTING = 'counting';
    /** @const 已結束 */
    const FINISH = 'finish';

    private static $codeTable = [
        ActivityStatus::PREPARE       => 0,
        ActivityStatus::NOTICE        => 1,
        ActivityStatus::VOTE          => 2,
        ActivityStatus::WAIT_COUNTING => 3,
        ActivityStatus::COUNTING      => 4,
        ActivityStatus::FINISH        => 5,
    ];

    /**
     * @param string $status
     * @return bool
     */
    public static function isVoteBegin($status)
    {
        return self::$codeTable[$status] >= 2;
    }

    /**
     * @param string $status
     * @return bool
     */
    public static function isFinish($status)
    {
        return $status == ActivityStatus::FINISH;
    }
}
