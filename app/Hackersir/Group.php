<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 候選人組
 *
 * @property int $id
 * @property int $number 號次
 * @property string $politics 政見
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $category_election_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Hackersir\Candidate[] $candidates
 * @property-read \Hackersir\CategoryElectionRelation $categoryElection
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Group onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereCategoryElectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group wherePolitics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Group withoutTrashed()
 * @mixin \Eloquent
 */
class Group extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'groups';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'number',
        'politics',
        'category_election_id',
    ];

    public $modelName = '候選人組';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoryElection()
    {
        return $this->belongsTo(CategoryElectionRelation::class);
    }

    public function candidates()
    {
        return $this->hasMany(Candidate::class)->orderBy('order');
    }
}
