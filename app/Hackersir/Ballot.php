<?php

namespace Hackersir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 選票
 *
 * @property int $id
 * @property int $type
 * @property int $voting_station_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $category_election_id
 * @property-read \Hackersir\CategoryElectionRelation $categoryElection
 * @property-read \Hackersir\Group|null $group
 * @property-read \Hackersir\VotingStation $votingStation
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Ballot onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereCategoryElectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Hackersir\Ballot whereVotingStationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Ballot withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Hackersir\Ballot withoutTrashed()
 * @mixin \Eloquent
 */
class Ballot extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @const 有效票 */
    const VALID = 0;
    /** @const 有效票(是反對票) */
    const VALID_OBJECTION = 1;
    /** @const 廢票 */
    const INVALID = 2;

    /** @var string $table 資料表 */
    protected $table = 'ballots';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'type',
        'voting_station_id',
        'group_id',
        'category_election_id',
    ];

    public $modelName = '選票';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoryElection()
    {
        return $this->belongsTo(CategoryElectionRelation::class, 'category_election_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function votingStation()
    {
        return $this->belongsTo(VotingStation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
