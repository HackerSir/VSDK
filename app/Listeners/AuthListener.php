<?php

namespace App\Listeners;

use Carbon\Carbon;
use Hackersir\Helper\JsonHelper;
use Hackersir\User;
use Hackersir\WebsiteLog;

class AuthListener
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    public function onLogin()
    {
        /** @var User $user */
        $user = auth()->user();
        $ip = request()->getClientIp();
        //紀錄時間與IP
        $user->update([
            'last_login_at' => Carbon::now(),
            'last_login_ip' => $ip,
        ]);
        WebsiteLog::create([
            'tag'     => 'Auth',
            'user_id' => $user->id,
            'message' => '登入了',
            'data'    => JsonHelper::encode([
                'ip' => $ip,
            ]),
        ]);
    }

    public function onLogout()
    {
        WebsiteLog::create([
            'tag'     => 'Auth',
            'user_id' => auth()->user()->id,
            'message' => '登出了',
            'data'    => JsonHelper::encode([
                'ip' => request()->getClientIp(),
            ]),
        ]);
    }
}
