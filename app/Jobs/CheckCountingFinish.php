<?php

namespace App\Jobs;

use App\Events\CountingFinishEvent;
use Hackersir\Activity;
use Hackersir\Services\ActivityService;
use Hackersir\Utils\ActivityStatus;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * 檢查開票是否完成
 * 如果是則：發送活動狀態改變的事件，並傳送最後一次票數結果
 * @package App\Jobs
 */
class CheckCountingFinish extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $activity;

    /**
     * Create a new job instance.
     *
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @param ActivityService $activityService
     */
    public function handle(ActivityService $activityService)
    {
        $status = $activityService->getStatus($this->activity);
        if ($status == ActivityStatus::FINISH) {
            dispatch(new CountingBallot($this->activity));
            event(new CountingFinishEvent($this->activity->id, $status));
        }
    }
}
