<?php

namespace App\Jobs;

use App\Events\BallotsUpdateEvent;
use Carbon\Carbon;
use Hackersir\Activity;
use Hackersir\Services\ActivityService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class CountingBallot extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Activity */
    protected $activity;

    protected $job_created_at;

    /**
     * Create a new job instance.
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
        $this->job_created_at = Carbon::now();
    }

    /**
     * Execute the job.
     *
     * @param ActivityService $activityService
     */
    public function handle(ActivityService $activityService)
    {
        if (Carbon::now()->diffInSeconds($this->job_created_at) <= 60) {
            $data = $activityService->getBallotCount($this->activity, true);
            event(new BallotsUpdateEvent($this->activity->id, $data));
        } else {
            Log::warning('CountingBallot is skip.');
        }
    }
}
