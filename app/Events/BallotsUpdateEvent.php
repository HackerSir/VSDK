<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class BallotsUpdateEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /** @var int */
    protected $activity_id;
    /** @var array */
    protected $data;

    /**
     * Create a new event instance.
     * @param int $activity_id
     * @param array $data
     */
    public function __construct($activity_id, array $data)
    {
        $this->activity_id = $activity_id;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['activity_' . $this->activity_id];
    }

    public function broadcastWith()
    {
        return $this->data;
    }
}
