<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class CountingFinishEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    protected $id;
    protected $status;

    public function __construct($id, $status)
    {
        $this->id = $id;
        $this->status = $status;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['status_activity_' . $this->id];
    }

    public function broadcastWith()
    {
        return [
            'id'     => $this->id,
            'status' => trans('activity_status' . '.' . $this->status),
        ];
    }
}
