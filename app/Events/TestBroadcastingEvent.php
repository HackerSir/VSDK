<?php

namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class TestBroadcastingEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['test-broadcasting'];
    }

    public function broadcastWith()
    {
        return [
            'message' => 'This is a message come from server.',
        ];
    }
}
