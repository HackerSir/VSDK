<?php

namespace App\Http\Controllers;

use Hackersir\Repositories\ActivityRepository;

class HomeController extends Controller
{
    /** @var ActivityRepository */
    protected $activityRepository;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity = $this->activityRepository->getHomeActivity();
        return view('home', compact('activity'));
    }

    public function about()
    {
        return view('misc.policies')->with('tab', 'about');
    }

    public function term()
    {
        return view('misc.policies')->with('tab', 'term');
    }

    public function disclaimer()
    {
        return view('misc.policies')->with('tab', 'disclaimer');
    }

    public function privacy()
    {
        return view('misc.policies')->with('tab', 'privacy');
    }

    public function faq()
    {
        return view('misc.policies')->with('tab', 'faq');
    }
}
