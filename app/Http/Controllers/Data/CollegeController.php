<?php

namespace App\Http\Controllers\Data;

use App\Http\Controllers\Controller;
use Hackersir\College;
use Illuminate\Http\Request;

class CollegeController extends Controller
{
    /**
     * CollegeController constructor.
     */
    public function __construct()
    {
        // 新增權限 (管理員)
        $this->middleware('permission:college.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        // 編輯權限 (管理員)
        $this->middleware('permission:college.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
    }

    /**
     * 顯示網站的學院清單，10筆一頁
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colleges = College::paginate(10);

        return view('college.index', compact('colleges'));
    }

    /**
     * 建立新的學院，欄位：名稱、簡稱
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('college.create-or-edit');
    }

    /**
     * 存儲新的學院，欄位：名稱、簡稱(都必須)
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'abbreviation' => 'required',
        ]);

        $college = College::create([
            'name'         => $request->input('name'),
            'abbreviation' => $request->input('abbreviation'),
        ]);

        return redirect()->route('college.show', $college)->with('global', '學院已建立');
    }

    /**
     * Display the specified resource.
     *
     * @param College $college
     * @return \Illuminate\Http\Response
     */
    public function show(College $college)
    {
        //顯示單一筆的學院資訊
        return view('college.show', compact('college'));
    }

    /**
     * 編輯學院，欄位：名稱(不可變更)、簡稱(可改)
     *
     * @param College $college
     * @return \Illuminate\Http\Response
     */
    public function edit(College $college)
    {
        return view('college.create-or-edit', compact('college'));
    }

    /**
     * 更新學院資料，欄位：名稱(不可變更)、簡稱(可改)
     *
     * @param  \Illuminate\Http\Request $request
     * @param College $college
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, College $college)
    {
        $this->validate($request, [
            'abbreviation' => 'required',
        ]);

        $college->update([
            'abbreviation' => $request->input('abbreviation'),
        ]);

        return redirect()->route('college.show', $college)->with('global', '學院已更新');
    }
}
