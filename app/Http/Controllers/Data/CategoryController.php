<?php

namespace App\Http\Controllers\Data;

use App\Http\Controllers\Controller;
use Hackersir\Category;
use Hackersir\Department;
use Hackersir\Helper\UrlHelper;
use Hackersir\Repositories\CategoryRepository;
use Hackersir\Services\ActivityService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /** @var CategoryRepository 注入的 CategoryRepository */
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository, ActivityService $activityService)
    {
        $this->categoryRepository = $categoryRepository;

        $this->middleware('permission:category.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);

        $this->middleware('permission:category.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
    }

    /**
     * 顯示網站有哪些選舉分組
     */
    public function index()
    {
        $categories = Category::with('department')->paginate(10);

        return view('category.index', compact('categories'));
    }

    /**
     * 顯示單一選舉分組的資訊
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('category.show', compact('category'));
    }

    /**
     * 顯示建立選舉分組的頁面
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view('category.create-or-edit', compact('departments'));
    }

    /**
     * 儲存處理建立選舉分組的要求
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'department' => 'required_without:name|exists:departments,id',
            'name'       => 'required_without:department',
            'image'      => 'nullable|url|imgur',
        ]);

        $category = Category::create([
            'name'          => $request->input('name'),
            'department_id' => $request->input('department'),
            'image'         => UrlHelper::secure($request->input('image')),
        ]);

        return redirect()->route('category.show', $category)->with('global', '分組已建立');
    }

    /**
     * 編輯頁面
     *
     * @param Category $category
     * @return mixed
     */
    public function edit(Category $category)
    {
        return view('category.create-or-edit', compact('category'));
    }

    /**
     * 更新資料
     * 僅能更新圖片
     *
     * @param Request $request
     * @param Category $category
     * @return mixed
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'image' => 'nullable|url|imgur',
        ]);

        $category->update([
            'image' => UrlHelper::secure($request->input('image')),
        ]);

        return redirect()->route('category.show', $category)->with('global', '分組已更新');
    }
}
