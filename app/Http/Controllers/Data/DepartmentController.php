<?php

namespace App\Http\Controllers\Data;

use App\Http\Controllers\Controller;
use Hackersir\College;
use Hackersir\Department;
use Hackersir\Helper\UrlHelper;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    public function __construct()
    {
        // 新增權限 (管理員)
        $this->middleware('permission:department.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        // 編輯權限 (管理員)
        $this->middleware('permission:department.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
    }

    /**
     * 顯示科系清單
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $colleges = College::all();
        $college = null;
        if ($request->has('college')) {
            $college = College::find($request->input('college'));
        }
        if ($college) {
            $departments = Department::where('college_id', $college->id)->paginate(10);
        } else {
            // 先依院排序
            $departments = Department::with('college')->orderBy('college_id')->paginate(10);
        }

        return view('department.index', compact('departments', 'colleges'));
    }

    /**
     * 回傳新增科系頁面
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colleges = [null => '-- 下拉選擇學院 --'] + College::pluck('name', 'id')->toArray();

        return view('department.create-or-edit', compact('colleges'));
    }

    /**
     * 處理科系新增請求
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'college'      => 'required|exists:colleges,id',
            'name'         => 'required',
            'abbreviation' => 'required',
            'image'        => 'nullable|url|imgur',
        ]);

        $department = Department::create([
            'college_id'   => $request->input('college'),
            'name'         => $request->input('name'),
            'abbreviation' => $request->input('abbreviation'),
            'image'        => UrlHelper::secure($request->input('image')),
        ]);

        return redirect()->route('department.show', $department)->with('global', '科系已建立');
    }

    /**
     * 顯示單一個科系資料
     *
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return view('department.show', compact('department'));
    }

    /**
     * 回傳科系編輯頁面
     *
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('department.create-or-edit', compact('department'));
    }

    /**
     * 處理科系編輯請求
     * 為了保持選舉紀錄的完整性，只予許修改簡稱或變更Icon，如果要變更科系名稱，請新增一個。
     *
     * @param  \Illuminate\Http\Request $request
     * @param Department $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $this->validate($request, [
            'abbreviation' => 'required',
            'image'        => 'nullable|url|imgur',
        ]);

        $department->abbreviation = $request->input('abbreviation');
        $department->image = UrlHelper::secure($request->input('image'));
        $department->save();

        return redirect()->route('department.show', $department)->with('global', '科系已更新');
    }
}
