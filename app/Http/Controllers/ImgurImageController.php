<?php

namespace App\Http\Controllers;

use Hackersir\ImgurImage;

class ImgurImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgurImages = ImgurImage::paginate();

        return view('imgur-image.index', compact('imgurImages'));
    }

    /**
     * Display the specified resource.
     *
     * @param ImgurImage $imgurImage
     * @return \Illuminate\Http\Response
     */
    public function show(ImgurImage $imgurImage)
    {
        return view('imgur-image.show', compact('imgurImage'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ImgurImage $imgurImage
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(ImgurImage $imgurImage)
    {
        $imgurImage->delete();

        return redirect()->route('imgurImage.index')->with('global', '圖片已刪除');
    }
}
