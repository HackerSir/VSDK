<?php

namespace App\Http\Controllers;

use App\Jobs\TestBroadcasting;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Redis;

class BroadcastingController extends Controller
{
    public function __construct()
    {
        // 先限定只有管理員能檢視
        $this->middleware('role:admin', [
            'only' => [
                'test',
                'findBroadcasting',
            ],
        ]);
        $this->middleware('ajax', [
            'only' => [
                'testRedis',
                'testBroadcastingServer',
                'findBroadcasting',
            ],
        ]);
    }

    /**
     * 測試 Broadcasting 是否正常
     * 並回結果給前端
     */
    public function test()
    {
        return view('misc.broadcasting-test');
    }

    /**
     * 提供前端要求後台測試 Redis 是否可用
     * @return string
     */
    public function testRedis()
    {
        try {
            $key_name = 'testRedis';
            $value = str_random(60);

            Redis::set($key_name, $value);
            $result = Redis::get($key_name);
            if ($value != $result) {
                return "error: get value != set value";
            }
        } catch (Exception $e) {
            return "error: " . $e->getMessage();
        }

        return "success";
    }

    /**
     * 提供前端要求後台測試 可否連線到 Broadcasting Server
     * @return string
     */
    public function testBroadcastingServer()
    {
        $serverHost = env('SOCKET_SERVER_URL', null);

        if (is_null($serverHost)) {
            return "error: SOCKET_SERVER_URL is null";
        }

        try {
            $client = new Client();
            $res = $client->request('GET', $serverHost);

            if ($res->getStatusCode() != 200) {
                return "error: " . $res->getStatusCode();
            }
        } catch (Exception|GuzzleException $e) {
            return "error: " . $e->getMessage();
        }

        return "success";
    }

    /**
     * 提供前端要求後台發送測試用廣播
     * @return string
     */
    public function findBroadcasting()
    {
        // fire 測試用 job
        dispatch(new TestBroadcasting());

        return "success fire. Wait message";
    }
}
