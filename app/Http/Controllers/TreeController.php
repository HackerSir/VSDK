<?php

namespace App\Http\Controllers;

use Hackersir\Activity;
use Hackersir\Election;
use Illuminate\Http\Request;

class TreeController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'activity' => 'exists:activities,id',
            'election' => 'exists:elections,id',
        ]);
        //篩選條件
        //活動
        if ($request->has('activity')) {
            $activity = Activity::with(
                'elections.staffs',
                'elections.categoryElectionRelations.category.department',
                'elections.categoryElectionRelations.groups.candidates',
                'elections.electionVotingStationRelations.votingStation',
                'elections.electionVotingStationRelations.users'
            )->find($request->input('activity'));

            return view('tree.index', compact('activity'));
        }
        //選舉
        if ($request->has('election')) {
            $election = Election::with(
                'staffs',
                'categoryElectionRelations.category.department',
                'categoryElectionRelations.groups.candidates',
                'electionVotingStationRelations.votingStation',
                'electionVotingStationRelations.users'
            )->find($request->input('election'));

            return view('tree.index', compact('election'));
        }
        //全部顯示
        $activities = Activity::with(
            'elections.staffs',
            'elections.categoryElectionRelations.category.department',
            'elections.categoryElectionRelations.groups.candidates',
            'elections.electionVotingStationRelations.votingStation',
            'elections.electionVotingStationRelations.users'
        )->orderBy('vote_start', 'desc')->get();

        return view('tree.index', compact('activities'));
    }
}
