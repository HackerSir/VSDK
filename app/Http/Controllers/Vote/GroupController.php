<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Group;
use Hackersir\Services\ElectionService;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected $electionService;

    public function __construct(ElectionService $electionService)
    {
        $this->electionService = $electionService;
        $this->middleware('permission:group.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        $this->middleware('permission:group.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
        $this->middleware('permission:group.delete', [
            'only' => [
                'destroy',
            ],
        ]);
    }

    /**
     * 回傳建立 group 的建立頁面
     * 需要接收是要建立在哪一個 CategoryElectionRelation 之下
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'eid' => 'required|exists:elections,id',
            'cid' => 'required|exists:categories,id',
        ]);

        /** @var Election $election */
        $election = Election::find($request->input('eid'));
        $category = Category::find($request->input('cid'));
        // 活動尚未公開才能新增
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        // 檢查關係是否存在
        $relation = CategoryElectionRelation::findRelation($category, $election)->first();
        if ($relation == null) {
            return back()->with('warning', '參數錯誤');
        }

        return view('group.create-or-edit', compact('relation'));
    }

    /**
     * 處理 group 的建立請求
     * 需要接收是要建立在哪一個 CategoryElectionRelation 之下
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'election_id' => 'required|exists:elections,id',
            'category_id' => 'required|exists:categories,id',
        ]);

        /** @var Election $election */
        $election = Election::find($request->input('election_id'));
        /** @var Category $category */
        $category = Category::find($request->input('category_id'));

        /** @var CategoryElectionRelation $relation */
        $relation = CategoryElectionRelation::findRelation($category, $election)->first();
        // 活動尚未公開才能新增
        if ($election->activity->publicity) {
            if ($election->categories->count() == 1) {
                return redirect()->route('election.show', $election)->with('warning', '活動已公開，屬於唯讀狀態');
            } else {
                return redirect()->route('category.show-category', $relation)
                    ->with('warning', '活動已公開，屬於唯讀狀態');
            }
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        // 檢查關係是否存在
        if ($relation == null) {
            return back()->with('warning', '參數錯誤');
        }

        // 建立新的
        Group::create([
            'election_id'          => $request->input('election_id'),
            'category_id'          => $request->input('category_id'),
            'number'               => $relation->groups->count() + 1,
            'politics'             => $request->input('politics'),
            'category_election_id' => $relation->id,
        ]);

        if ($election->categories->count() == 1) {
            return redirect()->route('election.show', $election);
        } else {
            return redirect()->route('category.show-category', $relation);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        $relation = $group->categoryElection;
        $election = $relation->election;

        // 活動尚未公開才能編輯
        if ($election->activity->publicity) {
            if ($election->categories->count() == 1) {
                return redirect()->route('election.show', $election)->with('warning', '活動已公開，屬於唯讀狀態');
            } else {
                return redirect()->route('category.show-category', $relation)
                    ->with('warning', '活動已公開，屬於唯讀狀態');
            }
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        return view('group.create-or-edit', compact('relation', 'group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $relation = $group->categoryElection;
        $election = $relation->election;

        // 活動尚未公開才能編輯
        if ($election->activity->publicity) {
            if ($election->categories->count() == 1) {
                return redirect()->route('election.show', $election)->with('warning', '活動已公開，屬於唯讀狀態');
            } else {
                return redirect()->route('category.show-category', $relation)
                    ->with('warning', '活動已公開，屬於唯讀狀態');
            }
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        $group->update([
            'politics' => $request->input('politics'),
        ]);

        if ($election->categories->count() == 1) {
            return redirect()->route('election.show', $election);
        } else {
            return redirect()->route('category.show-category', $relation);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $relation = $group->categoryElection;
        $election = $relation->election;

        // 活動尚未公開才能編輯
        if ($election->activity->publicity) {
            if ($election->categories->count() == 1) {
                return redirect()->route('election.show', $election)->with('warning', '活動已公開，屬於唯讀狀態');
            } else {
                return redirect()->route('category.show-category', $relation)
                    ->with('warning', '活動已公開，屬於唯讀狀態');
            }
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        $group->delete();

        // 重新編號
        $number = 1;
        foreach ($relation->groups as $group) {
            /** @var Group $group */
            $group->number = $number++;
            $group->save();
        }

        if ($election->categories->count() == 1) {
            return redirect()->route('election.show', $election);
        } else {
            return redirect()->route('category.show-category', $relation);
        }
    }
}
