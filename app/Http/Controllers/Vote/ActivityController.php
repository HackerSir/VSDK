<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Activity;
use Hackersir\Repositories\ActivityRepository;
use Hackersir\Services\ActivityService;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /** @var ActivityRepository */
    protected $activityRepository;
    /** @var ActivityService */
    protected $activityService;

    public function __construct(ActivityRepository $activityRepository, ActivityService $activityService)
    {
        $this->activityRepository = $activityRepository;
        $this->activityService = $activityService;

        $this->middleware('permission:activity.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        $this->middleware('permission:activity.edit', [
            'only' => [
                'edit',
                'update',
                'publish',
            ],
        ]);
        $this->middleware('password', [
            'only' => [
                'create',
            ],
        ]);
    }

    /**
     * 顯示選舉活動清單
     * 照投票開始時間排序，最新在前面
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::orderBy('vote_start', 'desc')->paginate(5);

        $status = [];
        $activities->each(function ($item, $key) use (& $status) {
            $status[] = $this->activityService->getStatus($item);
        });

        return view('activity.index', compact('activities', 'status'));
    }

    /**
     * @param Activity $activity
     * @return mixed
     */
    public function show(Activity $activity)
    {
        if (!($this->activityService->canView($activity))) {
            return redirect()->route('activity.index')->with('warning', '活動準備中');
        }

        return view('activity.show', compact('activity'));
    }

    /**
     *
     */
    public function create()
    {
        return view('activity.create-or-edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required|string|max:255',
            'vote_start'      => 'required|date|after:today',
            'vote_end'        => 'required|date|after:vote_start',
            'billing_start'   => 'required|date|after:vote_end',
            'process'         => 'nullable|string',
            'count_help_text' => 'nullable|string',
            'reporting_link'  => 'nullable|url',
        ]);

        $activity = $this->activityRepository->store($request);

        return redirect()->route('activity.show', $activity)->with('info', '活動已建立');
    }

    /**
     * @param Activity $activity
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Activity $activity)
    {
        $status = $this->activityService->getStatus($activity);

        return view('activity.create-or-edit', compact('activity', 'status'));
    }

    /**
     * @param Request $request
     * @param Activity $activity
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Activity $activity)
    {
        $this->validate($request, [
            'process'             => 'nullable|string',
            'count_help_text'     => 'nullable|string',
            'reporting_link'      => 'nullable|url',
            'results_report_link' => 'nullable|url',
        ]);

        $this->activityRepository->update($request, $activity, $this->activityService->getStatus($activity));

        return redirect()->route('activity.show', $activity)->with('info', '活動已更新');
    }

    public function publish(Activity $activity)
    {
        $activity->publicity = true;
        $activity->save();

        return redirect()->route('activity.show', compact('activity'))->with('info', '活動已公開');
    }
}
