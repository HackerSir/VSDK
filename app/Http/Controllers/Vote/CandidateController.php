<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Candidate;
use Hackersir\CategoryElectionRelation;
use Hackersir\Department;
use Hackersir\Election;
use Hackersir\Group;
use Hackersir\Helper\UrlHelper;
use Hackersir\Services\ElectionService;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    protected $electionService;

    public function __construct(ElectionService $electionService)
    {
        $this->electionService = $electionService;
        $this->middleware('permission:candidate.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        $this->middleware('permission:candidate.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
        $this->middleware('permission:candidate.delete', [
            'only' => [
                'destroy',
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'gid' => 'required|exists:groups,id',
        ]);

        /** @var Group $group */
        $group = Group::find($request->input('gid'));
        /** @var CategoryElectionRelation $relation */
        $relation = $group->categoryElection;
        /** @var Election $election */
        $election = $relation->election;

        // 活動尚未公開才能新增
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }

        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        $departments = Department::all();

        return view('candidate.create-or-edit', compact('group', 'departments', 'relation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // FIXME: 把photo_url的必須加上
        $this->validate($request, [
            'group'         => 'required|exists:groups,id',
            'photo_url'     => 'nullable|url|imgur',
            'name'          => 'required',
            'gender'        => 'required',
            'department_id' => 'required|exists:departments,id',
            'grade'         => 'required|between:1,7',
            'class_no'      => 'required|between:0,4',
            'age'           => 'required|between:0,150',
        ]);

        /** @var Group $group */
        $group = Group::find($request->input('group'));
        /** @var CategoryElectionRelation $relation */
        $relation = $group->categoryElection;
        /** @var Election $election */
        $election = $relation->election;

        // 活動尚未公開才能新增
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }
        //建立候選人
        Candidate::create([
            'group_id'      => $request->input('group'),
            'department_id' => $request->input('department_id'),
            'photo_url'     => UrlHelper::secure($request->input('photo_url')),
            'title'         => $request->input('title'),
            'name'          => $request->input('name'),
            'gender'        => $request->input('gender'),
            'grade'         => $request->input('grade'),
            'class_no'      => $request->input('class_no'),
            'age'           => $request->input('age'),
            'experience'    => $request->input('experience'),
        ]);
        //跳轉
        if ($election->categories->count() == 1) {
            return redirect()->route('election.show', $election)->with('global', '候選人已建立');
        } else {
            return redirect()->route('category.show-category', $relation)->with('global', '候選人已建立');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidate $candidate)
    {
        /** @var Group $group */
        $group = $candidate->group;
        /** @var CategoryElectionRelation $relation */
        $relation = $group->categoryElection;
        /** @var Election $election */
        $election = $relation->election;
        // 活動尚未公開才能編輯
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }
        $departments = Department::all();

        return view('candidate.create-or-edit', compact('candidate', 'group', 'departments', 'relation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        // FIXME: 把photo_url的必須加上
        $this->validate($request, [
            'photo_url'     => 'nullable|url|imgur',
            'name'          => 'required',
            'gender'        => 'required',
            'department_id' => 'required|exists:departments,id',
            'grade'         => 'required|between:1,7',
            'class_no'      => 'required|between:0,4',
            'age'           => 'required|between:0,150',
        ]);
        /** @var Group $group */
        $group = $candidate->group;
        /** @var CategoryElectionRelation $relation */
        $relation = $group->categoryElection;
        /** @var Election $election */
        $election = $relation->election;
        // 活動尚未公開才能編輯
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }

        $candidate->update([
            'department_id' => $request->input('department_id'),
            'photo_url'     => UrlHelper::secure($request->input('photo_url')),
            'title'         => $request->input('title'),
            'name'          => $request->input('name'),
            'gender'        => $request->input('gender'),
            'grade'         => $request->input('grade'),
            'class_no'      => $request->input('class_no'),
            'age'           => $request->input('age'),
            'experience'    => $request->input('experience'),
        ]);

        //跳轉
        if ($election->categories->count() == 1) {
            return redirect()->route('election.show', $election)->with('global', '候選人已更新');
        } else {
            return redirect()->route('category.show-category', $relation)->with('global', '候選人已更新');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Candidate $candidate
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Candidate $candidate)
    {
        /** @var Group $group */
        $group = $candidate->group;
        /** @var CategoryElectionRelation $relation */
        $relation = $group->categoryElection;
        /** @var Election $election */
        $election = $relation->election;
        // 活動尚未公開才能刪除
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($election)) {
            abort(403);
        }
        $candidate->delete();

        return back()->with('global', '候選人已刪除');
    }
}
