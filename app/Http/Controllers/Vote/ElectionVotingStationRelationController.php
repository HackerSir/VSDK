<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\ElectionVotingStationRelation;
use Hackersir\Services\ElectionService;
use Hackersir\User;
use Illuminate\Http\Request;

class ElectionVotingStationRelationController extends Controller
{
    protected $electionService;

    public function __construct(ElectionService $electionService)
    {
        $this->electionService = $electionService;
    }

    public function getEditCountingStaff(ElectionVotingStationRelation $relation)
    {
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($relation->election)) {
            abort(403);
        }

        return view('election.edit-counting-staff', compact('relation'));
    }

    public function postEditCountingStaff(Request $request, ElectionVotingStationRelation $relation)
    {
        // 檢查選務人員是否有此選舉的權限
        if (!$this->electionService->isStaff($relation->election)) {
            abort(403);
        }
        $countingStaffList = $request->input('counting_staff');
        if (empty($countingStaffList)) {
            $relation->users()->sync([]);

            return back()->with('global', '開票員已清空');
        }
        $countingStaffList = array_unique($countingStaffList);
        /** @var User[] $userList */
        $userList = User::whereIn('id', $countingStaffList)->get();
        $userIDArray = [];
        foreach ($userList as $user) {
            //排除未驗證使用者
            if (!$user->is_confirmed) {
                continue;
            }
            //排除非開票員
            if (!$user->hasRole('counting_staff')) {
                continue;
            }
            $userIDArray[] = $user->id;
        }
        $relation->users()->sync($userIDArray);
        //建立回應
        $response = back()->with('global', '開票員已更新');

        return $response;
    }
}
