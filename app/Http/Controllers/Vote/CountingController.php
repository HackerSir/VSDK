<?php


namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use App\Jobs\CheckCountingFinish;
use Auth;
use Hackersir\Activity;
use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\ElectionVotingStationRelation;
use Hackersir\Repositories\BallotRepository;
use Hackersir\Services\ActivityService;
use Hackersir\Utils\ActivityStatus;

/**
 * 處理跟開票頁面相關的請求
 * @package App\Http\Controllers\Vote
 */
class CountingController extends Controller
{
    /** @var BallotRepository */
    protected $ballotRepository;
    /** @var ActivityService */
    protected $activityService;

    public function __construct(BallotRepository $ballotRepository, ActivityService $activityService)
    {
        $this->ballotRepository = $ballotRepository;
        $this->activityService = $activityService;

        $this->middleware('role:counting_staff', [
            'only' => [
                'manageIndex',
                'manageShow',
                'manageClose',
            ],
        ]);
    }

    /**
     * 回傳開票頁面
     * @param Activity $activity
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Activity $activity)
    {
        $status = $this->activityService->getStatus($activity);
        if (!$activity->publicity) {
            return view('counting.prepare', compact('activity', 'status'));
        }
        $ballots = $this->activityService->getBallotCount($activity);
        $videoList = $this->activityService->getVideoList($activity);

        return view('counting.show', compact('activity', 'ballots', 'videoList', 'status'));
    }

    /**
     * 各站開票狀態
     *
     * @param Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function status(Activity $activity)
    {
        $activity->load('elections.votingStations');
        $votingStations = $this->activityService->getVotingStations($activity);
        /** @var array $status 索引：投票所ID、選舉ID */
        $status = [];
        //各站開票狀態
        foreach ($activity->elections as $election) {
            foreach ($election->votingStations as $votingStation) {
                $status[$votingStation->id][$election->id] = (bool) ($votingStation->pivot->counting_over);
            }
        }

        return view('counting.status', compact('activity', 'votingStations', 'status'));
    }

    public function manageIndex()
    {
        // 取得目前使用者可以開票的(選舉+開票所)清單
        $user = Auth::user();
        $relations = $user->electionVotingStationRelations;
        $relations->load('election.activity', 'votingStation');

        $groupedRelation = $relations->groupBy(function ($item, $key) {
            return $item->election->activity->name;
        });

        // TODO: 根據各種狀況，決定按鈕是否 disabled

        // 回傳給使用者
        return view('counting.manage.index', compact('groupedRelation'));
    }

    public function manageShow(ElectionVotingStationRelation $evsRelation)
    {
        // 檢查使用者是否有開票所的開票權限
        $user = Auth::user();
        if (!$evsRelation->isCountingStaff($user)) {
            return redirect()->route('counting.manage.index')->with('warning', '權限不足');
        }

        // 檢查活動是否在在開票期間
        if (!($this->activityService->getStatus($evsRelation->election->activity) == ActivityStatus::COUNTING)) {
            return redirect()->route('counting.manage.index')->with('warning', '不是開票期間');
        }

        // 檢查是否已開完
        if ($evsRelation->counting_over) {
            return redirect()->route('counting.manage.index')->with('warning', '已開票結束');
        }

        $evsRelation->load('election.categoryElectionRelations.category.department.college');
        $groupedRelations = $evsRelation->election->categoryElectionRelations->groupBy(function ($item, $key) {
            /** @var CategoryElectionRelation $item */
            /** @var Category $category */
            $category = $item->category;
            if (is_null($category->department)) {
                return 'all';
            } else {
                return $category->department->college->name;
            }
        });

        return view('counting.manage.show', compact('evsRelation', 'groupedRelations'));
    }

    public function manageClose(ElectionVotingStationRelation $evsRelation)
    {
        // 檢查使用者是否有開票所的開票權限
        $user = Auth::user();
        if (!$evsRelation->isCountingStaff($user)) {
            return redirect()->route('counting.manage.index')->with('warning', '權限不足');
        }

        // 檢查活動是否在在開票期間
        if (!($this->activityService->getStatus($evsRelation->election->activity) == ActivityStatus::COUNTING)) {
            return redirect()->route('counting.manage.index')->with('warning', '不是開票期間');
        }

        // 檢查是否已開完
        if ($evsRelation->counting_over) {
            return redirect()->route('counting.manage.index')->with('warning', '開票早已結束');
        }

        $evsRelation->counting_over = true;
        $evsRelation->save();

        // 檢查開票是否全結束
        $activity = $evsRelation->election->activity;
        dispatch(new CheckCountingFinish($activity));


        return redirect()->route('counting.manage.index')->with('global', '已結束開票');
    }
}
