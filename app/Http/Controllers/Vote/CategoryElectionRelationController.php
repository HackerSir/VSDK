<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Category;
use Hackersir\CategoryElectionRelation;
use Hackersir\Election;
use Hackersir\Repositories\BallotRepository;
use Hackersir\Repositories\CategoryElectionRelationRepository;
use Hackersir\Repositories\CategoryRepository;
use Hackersir\Services\ActivityService;
use Hackersir\VotingStation;
use Illuminate\Http\Request;

class CategoryElectionRelationController extends Controller
{
    /** @var CategoryRepository 注入的 CategoryRepository */
    protected $categoryRepository;
    /** @var ActivityService */
    protected $activityService;
    /** @var CategoryElectionRelationRepository */
    protected $categoryElectionRelationRepository;
    /** @var BallotRepository */
    protected $ballotRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        ActivityService $activityService,
        CategoryElectionRelationRepository $categoryElectionRelationRepository,
        BallotRepository $ballotRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->activityService = $activityService;
        $this->categoryElectionRelationRepository = $categoryElectionRelationRepository;
        $this->ballotRepository = $ballotRepository;

        $this->middleware('permission:category.relation.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        $this->middleware('ajax', [
            'only' => [
                'apiDataTable',
            ],
        ]);
    }

    public function show(CategoryElectionRelation $categoryElectionRelation)
    {
        $categoryElectionRelation->load('category', 'election.activity', 'groups.candidates.department');
        $election = $categoryElectionRelation->election;

        if (!($this->activityService->canView($election->activity))) {
            return redirect()->route('activity.index')->with('warning', '活動準備中');
        }
        $category = $categoryElectionRelation->category;
        $groups = $categoryElectionRelation->groups;
        $relation = $categoryElectionRelation;

        // 如果 $groups 為 null，代表沒有這個分組
        if (!$groups) {
            abort(404);
        }

        return view('category.show-one', compact('election', 'category', 'groups', 'relation'));
    }


    public function create(Request $request)
    {
        $this->validate($request, [
            'eid' => 'required|exists:elections,id',
        ]);
        // 檢查是否有 id，有的話代表是建立選舉和分組的關係
        $election = Election::find($request->input('eid'));

        // 檢查 $election 現在是否能新增分組
        if ($election->activity->publicity) {
            return redirect()->route('election.show', $election)->with('warning', '活動已公開，屬於唯讀狀態');
        }

        // 取得 $election 可以新增的組別清單
        $existCategories = CategoryElectionRelation::where('election_id', '=', $election->id)
            ->pluck('category_id')->toArray();
        $categories = Category::whereNotIn('id', $existCategories)->get();

        return view('category.create-under-election', compact('election', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'election_id'          => 'required|exists:elections,id',
            'category'             => 'required|exists:categories,id',
            'elected_group_amount' => 'required|integer|min:1',
        ]);

        // 檢查是否有 id，有的話代表是建立選舉和分組的關係
        $election = Election::find($request->input('election_id'));
        $category = Category::find($request->input('category'));

        $electedGroupAmount = $request->input('elected_group_amount');

        // 建立分組
        $results = $this->categoryElectionRelationRepository->create($category, $election, $electedGroupAmount);
        if ($results) {
            if (in_array('relation repeat', $results)) {
                return redirect()->route('election.show', $election)->with('warning', '已經有這個分組了');
            }
            //TODO: LOG
            abort(500);
        }

        // 返回 election 頁
        return redirect()->route('election.show', $election)->with('info', '組別建立完成');
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Category $category)
    {
        $this->validate($request, [
            'eid' => 'required|exists:elections,id',
        ]);

        /** @var Election $election */
        $election = Election::find($request->input('eid'));

        // 檢查 $election 是否可編輯
        if ($election->activity->publicity) {
            return back()->with('warning', '活動已公開，屬於唯讀狀態');
        }

        // 檢查 $election 與 $category 的關係是否存在
        /** @var CategoryElectionRelation $relation */
        $relation = CategoryElectionRelation::findRelation($category, $election)->first();
        if (!$relation) {
            abort(404);
        }

        //刪除關係
        $relation->delete();

        return redirect()->route('election.show', $election)->with('info', '分組已刪除');
    }

    public function apiDataTable(Request $request)
    {
        $this->validate($request, [
            'rid'  => 'required|exists:category_election,id',
            'vsid' => 'required|exists:voting_stations,id',
        ]);

        /** @var CategoryElectionRelation $relation */
        $relation = CategoryElectionRelation::with('groups.candidates')->find($request->input('rid'));
        /** @var VotingStation $votingStation */
        $votingStation = VotingStation::find($request->input('vsid'));

        $data = [];
        $ballots = $this->ballotRepository->countBallot($relation, $votingStation);
        foreach ($relation->groups as $index => $group) {
            $data[] = [
                'gid'    => $group->id,
                'number' => $group->number,
                'name'   => implode(" ", $group->candidates->pluck('name')->toArray()),
                'ballot' => $ballots['data'][$group->id],
            ];
        }
        $data[] = [
            'name'   => '廢票',
            'ballot' => $ballots['invalid'],
        ];
        $json = [
            'success' => true,
            'data'    => $data,
        ];

        return \Response::json($json);
    }
}
