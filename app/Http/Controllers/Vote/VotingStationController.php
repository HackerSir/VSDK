<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\VotingStation;
use Illuminate\Http\Request;

class VotingStationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $votingStations = VotingStation::all();

        return view('voting-station.index', compact('votingStations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('voting-station.create-or-edit')->with('methodText', '新增');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'location' => 'required',
        ]);

        $votingStation = VotingStation::create([
            'name'     => $request->input('name'),
            'location' => $request->input('location'),
        ]);

        return redirect()->route('votingStation.show', $votingStation)->with('global', '投票所已建立');
    }

    /**
     * Display the specified resource.
     *
     * @param VotingStation $votingStation
     * @return \Illuminate\Http\Response
     */
    public function show(VotingStation $votingStation)
    {
        return view('voting-station.show', compact('votingStation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param VotingStation $votingStation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(VotingStation $votingStation)
    {
        return view('voting-station.create-or-edit', compact('votingStation'))->with('methodText', '修改');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param VotingStation $votingStation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VotingStation $votingStation)
    {
        $this->validate($request, [
            'name'     => 'required',
            'location' => 'required',
        ]);

        $votingStation->update([
            'name'     => $request->input('name'),
            'location' => $request->input('location'),
        ]);

        return redirect()->route('votingStation.show', $votingStation)->with('global', '投票所已更新');
    }
}
