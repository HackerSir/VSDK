<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Activity;
use Hackersir\ElectionVotingStationRelation;
use Hackersir\Helper\YoutubeHelper;
use Hackersir\Services\ActivityService;
use Illuminate\Http\Request;

class ActivityVideoController extends Controller
{
    protected $activityService;

    /**
     * ActivityVideoController constructor.
     *
     * @param ActivityService $activityService
     */
    public function __construct(ActivityService $activityService)
    {
        $this->activityService = $activityService;
    }

    /**
     * 顯示投票所影片網址清單
     *
     * @param Activity $activity
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Activity $activity)
    {
        $votingStations = $this->activityService->getVotingStations($activity);

        return view('activity.video.show', compact('activity', 'votingStations'));
    }

    /**
     * 編輯投票所影片網址清單
     *
     * @param Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        $votingStations = $this->activityService->getVotingStations($activity);
        if (count($votingStations) == 0) {
            return back()->with('warning', '無投票所');
        }

        return view('activity.video.edit', compact('activity', 'votingStations'));
    }

    /**
     * 更新投票所影片網址清單
     *
     * @param Request $request
     * @param Activity $activity
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Activity $activity)
    {
        //限定Youtube影片
        $this->validate($request, [
            'video.*' => 'nullable|url|youtube',
        ]);
        $video = $request->input('video');
        //該活動所有選舉ID
        $electionIds = $activity->elections->pluck('id')->toArray();
        //該活動所有投票所ID
        $votingStationIds = array_keys($video);
        //取得所有關聯
        /** @var ElectionVotingStationRelation[] $relations */
        $relations = ElectionVotingStationRelation::whereIn('election_id', $electionIds)
            ->whereIn('voting_station_id', $votingStationIds)->get();
        //處理影片資料
        foreach ($video as $key => $value) {
            //轉換Youtube網址為簡短型態
            if (YoutubeHelper::getYoutubeID($value)) {
                $video[$key] = YoutubeHelper::transformShortUrl($value);
            }
        }
        //更新資料
        foreach ($relations as $relation) {
            $relation->video = $video[$relation->voting_station_id];
            $relation->save();
        }

        return redirect()->route('activity.video.show', $activity)->with('global', '影片清單已更新');
    }
}
