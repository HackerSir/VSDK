<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Hackersir\Activity;
use Hackersir\Election;
use Hackersir\Helper\UrlHelper;
use Hackersir\Services\ActivityService;
use Hackersir\User;
use Hackersir\VotingStation;
use Illuminate\Http\Request;

class ElectionController extends Controller
{
    /** @var ActivityService */
    protected $activityService;

    public function __construct(ActivityService $activityService)
    {
        $this->activityService = $activityService;

        $this->middleware('permission:election.create', [
            'only' => [
                'create',
                'store',
            ],
        ]);
        $this->middleware('permission:election.edit', [
            'only' => [
                'edit',
                'update',
            ],
        ]);
        $this->middleware('permission:election.edit-staff', [
            'only' => [
                'getEditStaff',
                'postEditStaff',
            ],
        ]);
        $this->middleware('permission:election.edit-voting-station', [
            'only' => [
                'getEditVotingStation',
                'postEditVotingStation',
            ],
        ]);
    }

    /**
     * 顯示單一選舉頁面
     *
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function show(Election $election)
    {
        if (!($this->activityService->canView($election->activity))) {
            return redirect()->route('activity.index')->with('warning', '活動準備中');
        }

        $categories = $election->categories;
        $categories->load('department');
        $categoriesCount = count($categories);
        //FIXME 修正取得關聯的方式
        $relations = $election->categoryElectionRelations->whereIn('category_id', $categories->pluck('id')->toArray());
        $relations->load('election', 'groups.candidates.department', 'category.department');
        $groups = $categoriesCount == 1 ? $relations->first()->groups : collect();

        return view('election.show', compact('election', 'groups', 'categories', 'categoriesCount', 'relations'));
    }

    /**
     * 回傳建立選舉的頁面
     * 從使用者接收要建立在哪一個活動以下的參數
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
        ]);

        /** @var Activity $activity */
        $activity = Activity::findOrFail($request->input('id'));
        if ($activity->publicity) {
            return redirect()->route('activity.show', $activity)->with('warning', '已公開活動不能新增選舉');
        }

        return view('election.create-or-edit', compact('activity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'activity_id' => 'required|integer',
            'name'        => 'required',
            'image'       => 'nullable|url|imgur',
        ]);
        /** @var Activity $activity */
        $activity = Activity::findOrFail($request->input('activity_id'));
        $election = Election::create([
            'name'         => $request->input('name'),
            'introduction' => $request->input('introduction'),
            'image'        => UrlHelper::secure($request->input('image')),
            'activity_id'  => $request->input('activity_id'),
            'order'        => $activity->elections->max('order') + 1,
        ]);

        return redirect()->route('election.show', $election)->with('global', '選舉已建立');
    }

    /**
     * 回傳選舉的編輯頁面
     *
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function edit(Election $election)
    {
        return view('election.create-or-edit', compact('election'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Election $election)
    {
        $this->validate($request, [
            'name'  => 'required',
            'image' => 'nullable|url|imgur',
        ]);

        if (!$election->activity->publicity) {
            $election->name = $request->input('name');
            $election->introduction = $request->input('introduction');
        }
        $election->image = UrlHelper::secure($request->input('image'));
        $election->save();

        return redirect()->route('election.show', $election)->with('global', '選舉已更新');
    }

    /**
     * 編輯選務人員（頁面）
     *
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function getEditStaff(Election $election)
    {
        return view('election.edit-staff', compact('election'));
    }

    /**
     * 編輯選務人員（處理）
     *
     * @param Request $request
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function postEditStaff(Request $request, Election $election)
    {
        $staffList = $request->input('staff');
        if (empty($staffList)) {
            $election->staffs()->sync([]);

            return back()->with('global', '選務人員已清空');
        }
        $staffList = array_unique($staffList);
        /** @var User[] $userList */
        $userList = User::whereIn('id', $staffList)->get();
        $userIDArray = [];
        foreach ($userList as $user) {
            //排除未驗證使用者
            if (!$user->is_confirmed) {
                continue;
            }
            //排除非選務人員
            if (!$user->hasRole('staff')) {
                continue;
            }
            $userIDArray[] = $user->id;
        }
        $election->staffs()->sync($userIDArray);
        //建立回應
        $response = back()->with('global', '選務人員已更新');

        return $response;
    }

    /**
     * 編輯該選舉之投票所（頁面）
     *
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function getEditVotingStation(Election $election)
    {
        $election->load('electionVotingStationRelations.votingStation', 'electionVotingStationRelations.users');
        $votingStations = VotingStation::all();

        return view('election.edit-voting-station', compact('election', 'votingStations'));
    }

    /**
     * 編輯該選舉之投票所（處理）
     *
     * @param Request $request
     * @param Election $election
     * @return \Illuminate\Http\Response
     */
    public function postEditVotingStation(Request $request, Election $election)
    {

        $stationList = $request->input('station');
        if (empty($stationList)) {
            $election->votingStations()->sync([]);

            return back()->with('global', '投票所已清空');
        }
        $stationList = array_unique($stationList);
        $stationArray = VotingStation::whereIn('id', $stationList)->pluck('id')->toArray();
        $election->votingStations()->sync($stationArray);

        return back()->with('global', '投票所已更新');
    }
}
