<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use Auth;
use Hackersir\CategoryElectionRelation;
use Hackersir\ElectionVotingStationRelation;
use Hackersir\Group;
use Hackersir\Repositories\BallotRepository;
use Hackersir\Services\ActivityService;
use Hackersir\Utils\ActivityStatus;
use Hackersir\VotingStation;
use Illuminate\Http\Request;
use Log;

class BallotController extends Controller
{
    /** @var BallotRepository $ballotRepository */
    protected $ballotRepository;
    /** @var ActivityService $activityService */
    protected $activityService;

    /**
     * BallotController constructor.
     * @param BallotRepository $ballotRepository
     * @param ActivityService $activityService
     */
    public function __construct(BallotRepository $ballotRepository, ActivityService $activityService)
    {
        $this->ballotRepository = $ballotRepository;
        $this->activityService = $activityService;
        $this->middleware('ajax', [
            'only' => [
                'checkRequest',
            ],
        ]);
    }

    /**
     * 新增或刪除選票，只接受 post，資料格式是 Json
     * 成功：['success' => true, 'data' => [票數, 無效票, 廢票]]
     * 失敗：['success' => false, 'message' => '失敗訊息']
     * FIXME: 檢查開票是否結束
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //判斷儲存或刪除
        if ($request->input('method') == 'destroy') {
            $method = 'destroy';
        } else {
            $method = 'store';
        }
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        //檢查請求
        $checkResponse = $this->checkRequest($request);
        if ($checkResponse) {
            return $checkResponse;
        }
        $group = Group::find($request->input('group'));
        // 根據 type 檢查資料是否符合格式
        $type = $request->input('type');
        if (in_array($type, [0, 1])) {
            // 有效票(0, 1)
            // 合法的group ID
            if ($categoryElectionRelation->groups->contains($group)) {
                // (group存在，且在選舉底下)
                // 是反對票嗎?
                // 這個組可以投反對票嗎?
                if ($type == 1 && !$categoryElectionRelation->can_vote_objection) {
                    return response()->json(['success' => false, 'message' => '此組不能投反對票'], 500);
                }

                //儲存
                $this->ballotRepository->{$method . 'Valid'}($request, $type);

                //回傳指定開票所的開票數
                return response()->json([
                    'success' => true,
                    'data'    => $this->ballotRepository->countBallot($categoryElectionRelation, $votingStation),
                ]);
            } else {
                return response()->json(['success' => false, 'message' => '這分組不包含此候選人'], 500);
            }
        } elseif (in_array($type, [2])) {
            // 廢票 (2)
            // 檢查 ?
            if (!$group) {
                $this->ballotRepository->{$method . 'Invalid'}($request);

                //回傳指定開票所的開票數
                return response()->json([
                    'success' => true,
                    'data'    => $this->ballotRepository->countBallot($categoryElectionRelation, $votingStation),
                ]);
            } else {
                return response()->json(['success' => false, 'message' => '廢票不應該有指定候選人'], 500);
            }
        }
        // 不合法的票
        //Log紀錄(Laravel log->error)
        Log::error('不合法的票', $request->toArray());

        return response()->json(['success' => false, 'message' => '不合法的票'], 500);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|null
     */
    public function checkRequest($request)
    {
        // 檢查資料
        // 是否有合法的選舉+分組關聯ID
        // 是否有合法的開票所ID
        $this->validate($request, [
            'categoryElectionRelation' => 'required|exists:category_election,id',
            'votingStation'            => 'required|exists:voting_stations,id',
            'type'                     => 'required|integer|in:0,1,2',
            'group'                    => 'nullable|exists:groups,id',
        ]);
        $categoryElectionRelation = CategoryElectionRelation::find($request->input('categoryElectionRelation'));
        $votingStation = VotingStation::find($request->input('votingStation'));
        $election = $categoryElectionRelation->election;
        /** @var ElectionVotingStationRelation $electionVotingStationRelation */
        $electionVotingStationRelation = ElectionVotingStationRelation::findRelation(
            $election,
            $votingStation
        )->first();
        // 選舉跟開票所是否有關連
        if (!$electionVotingStationRelation) {
            //TODO: LOG
            return response()->json(['success' => false, 'message' => '選舉與開票所的關係不存在'], 500);
        }
        // 檢查使用者是否有開票所的開票權限
        $user = Auth::user();
        if (!$electionVotingStationRelation->isCountingStaff($user)) {
            return response()->json(['success' => false, 'message' => '權限不足'], 403);
        }

        // 檢查活動是否在在開票期間
        if (!($this->activityService->getStatus($electionVotingStationRelation->election->activity)
            == ActivityStatus::COUNTING)
        ) {
            return redirect()->route('counting.manage.index')->with('warning', '不是開票期間');
        }

        // 檢查是否已開完
        if ($electionVotingStationRelation->counting_over) {
            return redirect()->route('counting.manage.index')->with('warning', '已開票結束');
        }

        return null;
    }
}
