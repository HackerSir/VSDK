<?php

namespace App\Http\Controllers;

use Hackersir\Helper\ImgurHelper;
use Hackersir\ImgurImage;
use Illuminate\Http\Request;
use Imgur;

class ImageApiController extends Controller
{
    /**
     * 上傳圖片
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        if (!$request->has('file_id')) {
            return response()->json(['error' => '無圖片可上傳']);
        }
        //圖片欄位名稱
        $inputName = $request->input('input_name');
        //取得檔案
        $file = $request->file($inputName);
        $image = Imgur::upload($file);
//        $fileContent = File::get($file->getPathname());
//        //將圖片上傳至Imgur
//        /** @var \Imgur\Client $client */
//        $client = App::make('Imgur\Client');
//        $imageData = [
//            'image' => base64_encode($fileContent),
//            'type'  => 'base64',
//        ];
//        /** @var \Imgur\Api\Image $imageClient */
//        $imageClient = $client->api('image');
//        $basic = $imageClient->upload($imageData);
//        $data = $basic->getData();
        //圖檔原始名稱
        $fileOriginalName = $file->getClientOriginalName();
        //額外記錄圖片資料（Imgur ID與Delete Hash）
        $imgurImage = ImgurImage::create([
            'imgur_id'    => $image->id,
            'file_name'   => $fileOriginalName,
            'extension'   => mb_strtolower($file->getClientOriginalExtension()),
            'delete_hash' => $image->deletehash,
        ]);
        $url = $imgurImage->url;

        $json = [
            'success'              => 'success',
            'url'                  => $url,
            'initialPreview'       => [
                "<img src='$url' class='file-preview-image' title="
                . $fileOriginalName
                . " style='max-width:200px;max-height:200px;width:auto;height:auto;'>",
            ],
            'initialPreviewConfig' => [
                [
                    'caption' => $fileOriginalName,
                    'url'     => route('image.delete'), // server delete action
                    'key'     => $url,
                ],
            ],
            'append'               => false,
        ];

        return response()->json($json);
    }

    /**
     * 刪除圖片
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $url = $request->input('key');
        $imgurId = ImgurHelper::getImgurID($url);
        /** @var ImgurImage $imgurImage */
        $imgurImage = ImgurImage::where('imgur_id', $imgurId)->first();
        if ($imgurImage) {
            //刪除ImgurImage
            $imgurImage->delete();
        }
        $json = [
            'success' => 'success',
        ];

        return response()->json($json);
    }
}
