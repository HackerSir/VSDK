<?php

namespace App\Http\Controllers;

use Auth;
use Hackersir\Activity;
use Hackersir\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class NotificationController extends Controller
{
    /**
     * NotificationController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:notification.manage', [
            'except' => [
                'index',
                'show',
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activityId = Input::get('activity');
        $activity = Activity::find($activityId);
        $notificationsQuery = Notification::with('user', 'activity')->orderBy('updated_at', 'desc');
        if ($activity) {
            $notificationsQuery->where('activity_id', $activity->id);
        }
        $notifications = $notificationsQuery->paginate();

        return view('notification.index', compact('notifications', 'activity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activityId = Input::get('activity');
        $activity = Activity::find($activityId);

        return view('notification.create-or-edit', compact('activity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type'        => 'required|in:' . implode(',', Notification::$validTypes),
            'text'        => 'required',
            'activity_id' => 'required|exists:activities,id',
        ]);
        $user = Auth::user();
        $notification = Notification::create([
            'type'        => $request->input('type'),
            'text'        => $request->input('text'),
            'activity_id' => $request->input('activity_id'),
            'user_id'     => $user->id,
        ]);

        return redirect()->route('notification.show', $notification)->with('global', '活動公告已建立');
    }

    /**
     * Display the specified resource.
     *
     * @param Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        return view('notification.show', compact('notification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        return view('notification.create-or-edit', compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        $this->validate($request, [
            'type'        => 'required|in:' . implode(',', Notification::$validTypes),
            'text'        => 'required',
            'activity_id' => 'required|exists:activities,id',
        ]);
        $notification->update([
            'type'        => $request->input('type'),
            'text'        => $request->input('text'),
            'activity_id' => $request->input('activity_id'),
        ]);

        return redirect()->route('notification.show', $notification)->with('global', '活動公告已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Notification $notification
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();

        return redirect()->route('notification.index')->with('global', '活動公告已刪除');
    }
}
