<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ReEnterPassword;
use Auth;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProtectController extends Controller
{
    public function postReEnterPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
        ]);
        $user = Auth::user();
        $password = $request->input('password');
        //驗證密碼
        if (!Auth::once(['email' => $user->email, 'password' => $password])) {
            return back()->with('warning', '密碼錯誤');
        }
        //驗證後設定初始期限
        Cache::put('ReEnterPassword_' . $user->email, Carbon::now(), ReEnterPassword::$limitMinutes);

        //刷新頁面
        return back();
    }
}
