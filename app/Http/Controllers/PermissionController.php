<?php

namespace App\Http\Controllers;

use Hackersir\Permission;
use Hackersir\Role;

class PermissionController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $permissions = Permission::with('roles')->orderBy('name')->get();

        return view('permission.index', compact('roles', 'permissions'));
    }
}
