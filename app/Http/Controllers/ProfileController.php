<?php

namespace App\Http\Controllers;

use Hackersir\User;
use Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * 查看會員資料
     *
     * @return \Illuminate\View\View
     */
    public function getProfile()
    {
        /** @var User $user */
        $user = auth()->user();

        return view('profile.index', compact('user'));
    }

    /**
     * 個人資料編輯頁面
     *
     * @return \Illuminate\View\View
     */
    public function getEditProfile()
    {
        /** @var User $user */
        $user = auth()->user();

        return view('profile.edit', compact('user'));
    }

    /**
     * 編輯個人資料
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        /** @var User $user */
        $user = auth()->user();

        $user->update([
            'name' => $request->input('name'),
        ]);

        return redirect()->route('profile')->with('global', '資料修改完成。');
    }

    /**
     * 密碼修改頁面
     *
     * @return \Illuminate\View\View
     */
    public function getChangePassword()
    {
        /** @var User $user */
        $user = auth()->user();

        return view('profile.change-password', compact('user'));
    }

    /**
     * 修改密碼
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
        /* @var User $user */
        $user = auth()->user();
        //檢查原密碼
        if (!Hash::check($request->input('password'), $user->getAuthPassword())) {
            return redirect()->route('profile.change-password')->withErrors(['password' => '輸入有誤，請重新輸入。']);
        }
        //檢查新密碼
        $this->validate($request, [
            'new_password' => 'required|confirmed|min:6',
        ]);

        //更新密碼
        $user->password = bcrypt($request->input('new_password'));
        $user->save();

        return redirect()->route('profile')->with('global', '密碼修改完成。');
    }
}
