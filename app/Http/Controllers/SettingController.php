<?php

namespace App\Http\Controllers;

use Hackersir\Services\MailService;
use Hackersir\Setting;
use Illuminate\Http\Request;
use Response;

class SettingController extends Controller
{
    protected $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
        $this->middleware('ajax', [
            'only' => [
                'sendTestMail',
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::all();

        return view('setting.index', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Setting $setting
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $this->validate($request, [
            'data' => 'max:65536',
        ]);

        $setting->update([
            'data' => $request->input('value'),
        ]);

        $json = [
            'success' => true,
        ];

        return Response::json($json);
    }

    public function sendTestMail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'type'  => 'required|in:normal,queue',
        ]);

        $email = $request->input('email');
        $type = $request->input('type');

        $result = false;
        if ($type == 'normal') {
            $result = $this->mailService->sendPlainMail($email, '', '這是測試信。', '測試信');
        } elseif ($type == 'queue') {
            $result = $this->mailService->queuePlainMail($email, '', '這是測試信。', '測試信');
        }

        return $result ? 'success' : 'error';
    }
}
