<?php namespace App\Http\Controllers;

use Hackersir\Helper\MarkdownHelper;
use Illuminate\Http\Request;
use Response;

class MarkdownApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax');
    }

    public function markdownPreview(Request $request)
    {
        $data = $request->getContent();
        //檢查是否有內容
        if (empty($data)) {
            return Response::make(" ");
        }

        return Response::make(MarkdownHelper::translate($data));
    }
}
