<?php

namespace App\Http\Controllers;

use Gravatar;
use Hackersir\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ApiController extends Controller
{
    public function userList(Request $request)
    {
        /** @var Builder|User $usersQuery */
        $usersQuery = User::query();
        //搜尋關鍵字
        if ($request->has('q')) {
            $searchPattern = '%' . $request->input('q') . '%';
            //搜尋使用者名稱或信箱
            $usersQuery->where(function ($query) use ($searchPattern) {
                /** @var Builder|User $query */
                $query->where('name', 'like', $searchPattern)
                    ->orWhere('email', 'like', $searchPattern);
            });
        }
        //使用者必須通過信箱驗證
        if ($request->has('confirm')) {
            $usersQuery->whereNotNull('confirm_at');
        }
        //角色
        if ($request->has('role')) {
            $usersQuery->whereHas('roles', function ($query) use ($request) {
                $query->where('name', $request->input('role'));
            });
        }
        //取得資料
        /** @var Collection|User[] $users */
        $users = $usersQuery->get();
        //轉換陣列內容
        $items = [];
        foreach ($users as $user) {
            $items[] = [
                'id'       => $user->id,
                'name'     => $user->name,
                'email'    => $user->email,
                'gravatar' => Gravatar::src($user->email, 40),
            ];
        }
        //建立JSON
        $json = [
            'total_count' => $users->count(),
            'items'       => $items,
        ];

        return response()->json($json);
    }
}
