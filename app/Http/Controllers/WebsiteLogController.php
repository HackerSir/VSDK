<?php

namespace App\Http\Controllers;

use Hackersir\WebsiteLog;

class WebsiteLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $websiteLogs = WebsiteLog::with('user')->orderBy('id', 'desc')->paginate();

        return view('website-log.index', compact('websiteLogs'));
    }

    /**
     * @param WebsiteLog $websiteLog
     * @return mixed
     */
    public function show(WebsiteLog $websiteLog)
    {
        return view('website-log.show', compact('websiteLog'));
    }
}
