<?php

namespace App\Http\Controllers;

use Auth;
use Hackersir\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    /**
     * AnnouncementController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:announcement.manage', [
            'except' => [
                'index',
                'show',
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = Announcement::orderBy('created_at', 'desc')->paginate();

        return view('announcement.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        return view('announcement.create-or-edit')->with('methodText', '新增');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'   => 'required|max:255',
            'message' => 'required|max:65535',
        ]);

        $announcement = Announcement::create([
            'title'   => $request->input('title'),
            'message' => $request->input('message'),
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->route('announcement.show', $announcement)->with('global', '公告已建立');
    }

    /**
     * Display the specified resource.
     *
     * @param Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return view('announcement.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Announcement $announcement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit(Announcement $announcement)
    {
        return view('announcement.create-or-edit', compact('announcement'))->with('methodText', '修改');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        $this->validate($request, [
            'title'   => 'required|max:255',
            'message' => 'required|max:65535',
        ]);

        $announcement->update([
            'title'   => $request->input('title'),
            'message' => $request->input('message'),
        ]);

        return redirect()->route('announcement.show', $announcement)->with('global', '公告已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Announcement $announcement
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Announcement $announcement)
    {
        $announcement->delete();

        return redirect()->route('announcement.index')->with('global', '公告已刪除');
    }
}
