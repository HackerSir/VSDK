<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Log\Logger;
use Log;

//http://stackoverflow.com/questions/31619350/correct-way-to-get-server-response-time-in-laravel
class ResponseTimeRecord
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param $response
     */
    public function terminate($request, $response)
    {
        try {
            $logger = new Logger(new \Monolog\Logger('ResponseTime'));
            // FIXME: No method called useDailyFiles
//            $logger->useDailyFiles(storage_path('response-time/response.log'), 365);

            $responseTime = microtime(true) - LARAVEL_START;
            $logger->info($responseTime . ' "' . $request->getMethod() . ' ' . $request->getPathInfo() . '"');
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
