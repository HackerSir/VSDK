<?php

namespace App\Http\Middleware;

use Auth;
use Cache;
use Carbon;
use Closure;
use Hackersir\User;

/**
 * Class ReEnterPassword
 * - 套用至想保護的function（只須套用到回傳view的get路由）
 * - 進入頁面須輸入密碼
 * - 密碼輸入後，可進入所有受保護頁面
 * - 密碼輸入後，N分鐘內無須再次輸入
 * - 每次訪問受保護頁面時，時限自動重置為N分鐘
 * - 在受保護頁面，停留時間沒有限制
 *
 * @package App\Http\Middleware
 */
class ReEnterPassword
{
    /**
     * 輸入密碼後，可訪問受保護頁面的有效時間（分鐘）
     * 在受保護頁面活動時，該時間會重新計算
     *
     * @static int
     */
    public static $limitMinutes = 5;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = Auth::user();
        //檢查是否通過驗證
        $confirm = Cache::get('ReEnterPassword_' . $user->email);
        if (!$confirm) {
            //顯示密碼驗證頁面（直接回傳view，不跳轉）
            return response(view('protect.re-enter-password'));
        }
        //延長期限
        Cache::put('ReEnterPassword_' . $user->email, Carbon::now(), static::$limitMinutes);

        return $next($request);
    }
}
