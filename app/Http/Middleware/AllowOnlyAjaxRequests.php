<?php

namespace App\Http\Middleware;

use Closure;

class AllowOnlyAjaxRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax() && env('AJAX_PROTECT', true) !== false) {
            return response()->json(['error' => 'AJAX only.'], 405);
        }

        return $next($request);
    }
}
