<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Entrust;
use Gravatar;
use Hackersir\Repositories\ActivityRepository;
use Menu;

class LaravelMenu
{
    /**
     * @var ActivityRepository
     */
    private $activityRepository;

    /**
     * LaravelMenu constructor.
     * @param ActivityRepository $activityRepository
     */
    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $activity = $this->activityRepository->getHomeActivity();

        Menu::make('navBar', function ($menu) use ($activity) {
            /* @var \Lavary\Menu\Builder $menu */
            //基本巡覽列
            if ($activity) {
                /* @var \Lavary\Menu\Builder $candidatePolitics */
                $candidatePolitics = $menu->add('候選人政見');
                foreach ($activity->elections as $election) {
                    $candidatePolitics->add($election->name, ['route' => ['election.show', $election]]);
                }
                $menu->add('開票直播', ['route' => ['counting.show', $activity]]);
            }
            $menu->add('選舉活動', ['route' => 'activity.index']);
            $menu->add('網站公告', ['route' => 'announcement.index']);
            //會員
            if (Auth::check()) {
                //信箱驗證
                if (!Auth::user()->is_confirmed) {
                    $confirmAlert = $menu->add(
                        '<i class="fa fa-exclamation-triangle"></i> 尚未完成信箱驗證',
                        ['route' => 'confirm-mail.resend']
                    );
                    $confirmAlert->attr('class', 'alert-danger');
                }
                if (Entrust::hasRole('admin')) {
                    /* @var \Lavary\Menu\Builder $adminMenu */
                    $adminMenu = $menu->add('管理員', 'javascript:void(0)');
                    $adminMenu->add('會員管理', ['route' => 'user.index']);
                    $adminMenu->add('權限清單', ['route' => 'permission.index'])->divide();
                    $adminMenu->add('網站設定', ['route' => 'setting.index']);
                    $adminMenu->add('科系管理', ['route' => 'department.index']);
                    $adminMenu->add('學院管理', ['route' => 'college.index']);
                    $adminMenu->add('選舉分組管理', ['route' => 'category.index']);
                    $adminMenu->add('投票所管理', ['route' => 'votingStation.index']);
                    $adminMenu->add('活動公告', ['route' => 'notification.index']);
                    $adminMenu->add('資源樹狀圖', ['route' => 'tree.index']);
                    $adminMenu->add('Imgur圖片管理', ['route' => 'imgurImage.index'])->divide();
                    $adminMenu->add('CSS測試', ['route' => 'css-test']);
                    $adminMenu->add('Queue狀態', ['route' => 'queue-status']);
                    $adminMenu->add('廣播測試', ['route' => 'broadcasting-test'])->divide();
                    $adminMenu->add('網站記錄', ['route' => 'websiteLog.index']);
                    $adminMenu->add(
                        '記錄檢視器 <i class="fa fa-external-link"></i>',
                        ['route' => 'log-viewer::dashboard']
                    )->link->attr('target', '_blank');
                }
                if (Entrust::hasRole('counting_staff')) {
                    /* @var \Lavary\Menu\Builder $countingMenu */
                    $countingMenu = $menu->add('開票員', 'javascript:void(0)');
                    $countingMenu->add('開票管理', ['route' => 'counting.manage.index']);
                }
                //會員
                /* @var \Lavary\Menu\Builder $userMenu */
                $userMenu = $menu->add(
                    '<img src="' . Gravatar::src(Auth::user()->email) . '" class="img-circle" height=40 width=40> '
                    . Auth::user()->name,
                    'javascript:void(0)'
                );
                $userMenu->link->attr('style', 'padding: 5px 15px 5px 5px');
                $userMenu->add('個人資料', ['route' => 'profile']);
                $userMenu->add('登出', ['route' => 'logout']);
            } else {
                //遊客
            }
        });

        return $next($request);
    }
}
