<?php

namespace App\Http\Middleware;

use Closure;
use Hackersir\Setting;

class RegisterSwitch
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowRegister = Setting::getItem('AllowRegister', true);
        if (!$allowRegister) {
            return back()->with('warning', '目前未開放註冊');
        }
        return $next($request);
    }
}
