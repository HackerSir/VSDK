<?php

namespace App\Mail;

use Config;
use Hackersir\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->user->email, $this->user->name)
            ->subject('[' . Config::get('site.name') . '] 信箱驗證')
            ->view('emails.email_confirmation')
            ->with([
                'name' => $this->user->name,
                'link' => route('confirm', $this->user->confirm_code),
            ]);
    }
}
