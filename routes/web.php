<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//會員（須完成信箱驗證）
Route::group(['middleware' => ['email', 'auth']], function () {
    //權限清單
    //權限：permission.index.access
    Route::get('permission', [
        'as'         => 'permission.index',
        'uses'       => 'PermissionController@index',
        'middleware' => 'permission:permission.index.access',
    ]);
    //會員管理
    //權限：user.manage、user.view
    Route::resource('user', 'UserController', [
        'except' => [
            'create',
            'store',
        ],
    ]);
    //網站設定
    //權限：setting.edit
    Route::group(['middleware' => 'permission:setting.edit'], function () {
        //寄送測試信
        Route::post('send-test-mail', [
            'as'   => 'send-test-mail',
            'uses' => 'SettingController@sendTestMail',
        ]);
        Route::resource('setting', 'SettingController', [
            'only' => [
                'index',
                'update',
            ],
        ]);
    });
    //投票所管理
    //權限：voting-station.manage
    Route::group(['middleware' => 'permission:voting-station.manage'], function () {
        Route::resource('votingStation', 'Vote\VotingStationController', [
            'except' => 'destroy',
        ]);
    });

    //編輯選務人員
    //權限：election.edit-staff
    Route::get('election/{election}/edit-staff', [
        'as'   => 'election.edit-staff',
        'uses' => 'Vote\ElectionController@getEditStaff',
    ]);
    Route::post('election/{election}/edit-staff', [
        'as'   => 'election.edit-staff',
        'uses' => 'Vote\ElectionController@postEditStaff',
    ]);
    //編輯開票員
    //權限: election.edit-counting-staff 並且 使用者必須為該選舉之選務人員
    Route::group(['middleware' => 'permission:election.edit-counting-staff'], function () {
        Route::get('election/edit-counting-staff/{relation}', [
            'as'   => 'election.edit-counting-staff',
            'uses' => 'Vote\ElectionVotingStationRelationController@getEditCountingStaff',
        ]);
        Route::post('election/edit-counting-staff/{relation}', [
            'as'   => 'election.edit-counting-staff',
            'uses' => 'Vote\ElectionVotingStationRelationController@postEditCountingStaff',
        ]);
    });
    //編輯選舉之投票所
    //權限：election.edit-voting-station
    Route::get('election/{election}/edit-voting-station', [
        'as'   => 'election.edit-voting-station',
        'uses' => 'Vote\ElectionController@getEditVotingStation',
    ]);
    Route::post('election/{election}/edit-voting-station', [
        'as'   => 'election.edit-voting-station',
        'uses' => 'Vote\ElectionController@postEditVotingStation',
    ]);
    //開票
    //權限：election.count.ballot 且 須為該站開票員
    Route::group(['middleware' => 'permission:election.count.ballot'], function () {
        Route::post('ballot', [
            'as'   => 'ballot.store',
            'uses' => 'Vote\BallotController@store',
        ]);
    });
    //檢視網站記錄
    //權限：log-viewer.access
    Route::group(['middleware' => 'permission:log-viewer.access'], function () {
        Route::resource('websiteLog', 'WebsiteLogController', [
            'only' => [
                'index',
                'show',
            ],
        ]);
    });
    //網站資源樹狀圖
    //權限：tree.access
    Route::get('tree', [
        'as'         => 'tree.index',
        'uses'       => 'TreeController@index',
        'middleware' => 'permission:tree.access',
    ]);
    //Imgur圖片管理
    //權限：imgur-image.manage
    Route::group(['middleware' => 'permission:imgur-image.manage'], function () {
        Route::resource('imgurImage', 'ImgurImageController', [
            'only' => [
                'index',
                'show',
                'destroy',
            ],
        ]);
    });

    //會員資料
    Route::group(['prefix' => 'profile'], function () {
        //查看會員資料
        Route::get('/', [
            'as'   => 'profile',
            'uses' => 'ProfileController@getProfile',
        ]);
        //編輯會員資料
        Route::get('edit', [
            'as'   => 'profile.edit',
            'uses' => 'ProfileController@getEditProfile',
        ]);
        Route::put('update', [
            'as'   => 'profile.update',
            'uses' => 'ProfileController@updateProfile',
        ]);
        //修改密碼
        Route::get('change-password', [
            'as'   => 'profile.change-password',
            'uses' => 'ProfileController@getChangePassword',
        ]);
        Route::put('update-password', [
            'as'   => 'profile.update-password',
            'uses' => 'ProfileController@updatePassword',
        ]);
    });
    //保護機制
    Route::group(['prefix' => 'protect'], function () {
        //重新輸入密碼（保護頁面）
        Route::post('re-enter-password', [
            'as'   => 'protect.re-enter-password',
            'uses' => 'ProtectController@postReEnterPassword',
        ]);
    });
});

// 首頁
Route::get('/', 'HomeController@index')->name('home');
// 關於我們
Route::get('about', 'HomeController@about')->name('about');
// 服務條款
Route::get('term', 'HomeController@term')->name('term');
// 免責聲明
Route::get('disclaimer', 'HomeController@disclaimer')->name('disclaimer');
// 隱私權政策
Route::get('privacy', 'HomeController@privacy')->name('privacy');
// 常見問題
Route::get('faq', 'HomeController@faq')->name('faq');
// Queue狀態
Route::get('queue-status', 'QueueStatusController@index')->name('queue-status');
// 廣播測試
Route::get('broadcasting-test', 'BroadcastingController@test')->name('broadcasting-test');
Route::post('broadcasting-test/redis', 'BroadcastingController@testRedis')->name('broadcasting-test.redis');
Route::post('broadcasting-test/server', 'BroadcastingController@testBroadcastingServer')
    ->name('broadcasting-test.server');
Route::post('broadcasting-test/fire', 'BroadcastingController@findBroadcasting')->name('broadcasting-test.fire');

// 活動
Route::post('activity/{activity}/publish', 'Vote\ActivityController@publish')->name('activity.publish');
// 活動直播影片
//權限：activity.video.manage
Route::group(['middleware' => 'permission:activity.video.manage'], function () {
    Route::get('activity/{activity}/video', 'Vote\ActivityVideoController@show')->name('activity.video.show');
    Route::get('activity/{activity}/video/edit', 'Vote\ActivityVideoController@edit')->name('activity.video.edit');
    Route::post('activity/{activity}/video/edit', 'Vote\ActivityVideoController@update')
        ->name('activity.video.update');
});
Route::resource('activity', 'Vote\ActivityController', [
    'except' => [
        'destroy',
    ],
]);

// 選舉
Route::resource('election', 'Vote\ElectionController', [
    'except' => [
        'index',
        'destroy',
    ],
]);

// 選舉分組
// 顯示某一選舉的分組資料
Route::resource('category', 'Data\CategoryController');

Route::get('election/show-category/{categoryElectionRelation}', [
    'as'   => 'category.show-category',
    'uses' => 'Vote\CategoryElectionRelationController@show',
]);

Route::get('category-relation/create', 'Vote\CategoryElectionRelationController@create')
    ->name('category-relation.create');

Route::post('category-relation/store', 'Vote\CategoryElectionRelationController@store')
    ->name('category-relation.store');

Route::delete('category-relation/delete/{category}', 'Vote\CategoryElectionRelationController@destroy')
    ->name('category-relation.delete');

Route::get('category-relation/api', 'Vote\CategoryElectionRelationController@apiDataTable')
    ->name('category-relation.api.data');

Route::resource('group', 'Vote\GroupController', [
    'except' => [
        'index',
        'show',
    ],
]);

Route::resource('candidate', 'Vote\CandidateController', [
    'except' => [
        'index',
        'show',
    ],
]);

Route::resource('department', 'Data\DepartmentController', [
    'except' => [
        'destroy',
    ],
]);

Route::resource('college', 'Data\CollegeController', [
    'except' => [
        'destroy',
    ],
]);

Route::resource('notification', 'NotificationController');

// 開票頁面
Route::get('counting/manage', 'Vote\CountingController@manageIndex')->name('counting.manage.index');
Route::get('counting/manage/{evsRelation}', 'Vote\CountingController@manageShow')->name('counting.manage.show');
Route::post('counting/manage/{evsRelation}/close', 'Vote\CountingController@manageClose')
    ->name('counting.manage.close');
Route::get('counting/{activity}', 'Vote\CountingController@show')->name('counting.show');
Route::get('counting/{activity}/status', 'Vote\CountingController@status')->name('counting.status');

//公告
Route::resource('announcement', 'AnnouncementController');

Route::get('css-test', [
    'as'         => 'css-test',
    'middleware' => 'role:admin',
    function () {
        return view('bootstrap-test');
    },
]);

//API
Route::group(['prefix' => 'api'], function () {
    Route::post('user-list', [
        'as'   => 'api.user-list',
        'uses' => 'ApiController@userList',
    ]);
});
//Markdown API
Route::any('markdown', [
    'as'   => 'markdown.preview',
    'uses' => 'MarkdownApiController@markdownPreview',
]);
//Image API
Route::post('image/upload', [
    'as'   => 'image.upload',
    'uses' => 'ImageApiController@upload',
]);
Route::post('image/delete', [
    'as'   => 'image.delete',
    'uses' => 'ImageApiController@delete',
]);

Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::post('logout', 'LoginController@logout')->name('logout');
    // Registration Routes...
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register')->name('register');
    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
    //修改密碼
//    Route::get('password/change', 'PasswordController@getChangePassword')->name('password.change');
//    Route::put('password/change', 'PasswordController@putChangePassword')->name('password.change');
    //驗證信箱
    Route::get('resend', 'RegisterController@resendConfirmMailPage')->name('confirm-mail.resend');
    Route::post('resend', 'RegisterController@resendConfirmMail')->name('confirm-mail.resend');
    Route::get('confirm/{confirmCode}', 'RegisterController@emailConfirm')->name('confirm');
});
