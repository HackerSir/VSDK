let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/bootswatch.scss', 'public/build-css').version('public/build-css')
   .sass('resources/assets/sass/sticky-footer.scss', 'public/build-css').version('public/build-css')
   .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/build/fonts/bootstrap');

mix.copy('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css', 'public/build-css/fileinput.min.css')
   .copy('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js', 'public/build-js/fileinput.min.js')
   .copy('vendor/kartik-v/bootstrap-fileinput/js/locales/zh-TW.js', 'public/build-js/fileinput_locale_zh-TW.js')
   .copy('node_modules/js-cookie/src/js.cookie.js', 'public/build-js/js.cookie.js')
   .copy('node_modules/animate.css/animate.min.css', 'public/build-css/animate.min.css');
